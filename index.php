<?php
	require "db.php";
	require "simple_html_dom.php";
	
	$stmt = $conn->prepare("SELECT * FROM default_news");
	$stmt->execute();

	$crawl = $stmt->fetchAll();
?>
<html>
	<meta charset="utf-8">
	<body>
	<table>
		<tr>
			<td>ID</td>
			<td>Title</td>
			<td>Description</td>
			<td>Image</td>
		</tr>
		<?php 
			if(count($crawl) > 0){
				foreach($crawl as $value){
					?>
						<tr>
							<td><?php echo $value["id"];?></td>
							<td><?php echo $value["title"];?></a></td>
							<td><?php echo $value["description"];?></td>
							<td><img style="width:200px;height:200px;" src= "image/<?php echo $value["image"];?>"></td>
							
						</tr>
					<?php
				}
			}
		?>
		
	</table>
	</body>
</html>