<?php
date_default_timezone_set("Asia/Ho_Chi_Minh");
error_reporting(E_ALL);
ini_set("display_errors",1);
/*ini_set('session.cookie_httponly',1);
ini_set('session.use_only_cookies',1);
ini_set('session.cookie_secure',1);*/
$rootDir = dirname(dirname(__FILE__));

set_include_path(get_include_path() . PATH_SEPARATOR . $rootDir);
include $rootDir . '/Frontend/router.php';
include $rootDir .'/config.php';


include_once $rootDir . '/Soul/Loader.php';
$loader = new \Soul\Loader();


$loader->registerNamespaces(array(

    'Soul\Mysql' => $rootDir . '\Mysql',
    'Soul\Helpers' => $rootDir ,
    'Soul\Pattern' => $rootDir . '\Pattern',
    'Application\Frontend\Helpers' => $rootDir,
    'Application\Frontend\Models' => $rootDir,
    'Application\Frontend\Controllers' => $rootDir,
    'Application\Library' => $rootDir,
    'Soul' => $rootDir . '',
));
$loader->register();
use Soul\Application;


\Soul\Session::getInstance();

$session = \Soul\Session::getInstance();

#DOMdocument
/*require_once($rootDir . "/Application/Library/simple_html_dom.php");
$DOM = new \simple_html_dom();
\Soul\Registry::set('DOM', $DOM);*/

#Mobile check
require_once($rootDir . "/Application/Library/Mobile_Detect.php");
/*$mb = new \Mobile_Detect();
if ($mb->isMobile()) {
    header("Location: "._ROOT_MOBILE);
}*/


#Facebook config#

/*require $rootDir . '/Application/Library/facebook_full/autoload.php';

use Facebook\FacebookSession;

FacebookSession::setDefaultApplication(FACEBOOK_APP_ID,FACEBOOK_APP_SECRET);*/
/*$helper = new FacebookCanvasLoginHelper();
\Soul\Registry::set('facebook', $helper);*/



#MySQL
$mysql['host'] = DB_SERVER;
$mysql['dbname'] = DB_DATABASE;
$mysql['username'] = DB_USERNAME;
$mysql['password'] = DB_PASSWORD;
$mysql['options'] = null;

/*$mysql_second['host'] = DB_SERVER_SECOND;
$mysql_second['dbname'] = DB_DATABASE_SECOND;
$mysql_second['username'] = DB_USERNAME_SECOND;
$mysql_second['password'] = DB_PASSWORD_SECOND;
$mysql_second['options'] = null;*/

$mysqlConnection = \Soul\Mysql::factory('Main', $mysql);
\Soul\Registry::set('Mysql', $mysqlConnection);


/*$mysqlConnection = \Soul\Mysql::factory('member', $mysql_second);
\Soul\Registry::set('MysqlUser', $mysqlConnection);*/


#MongoDb
/*$mongo['host'] = MONGO_SERVER;
$mongo['dbname'] = MONGO_DATABASE;
$mongo['username'] = MONGO_USERNAME;
$mongo['password'] = MONGO_PASSWORD;
$mongo['port'] = MONGO_PORT;


$mongoConnection = \Soul\Mongo::factory('master', $mongo);
\Soul\Registry::set('Mongo', $mongoConnection);*/


#Memcache
define("ENABLE_MEMCACHED", false);
$memcache['host'] = MEMCACHE_SERVER;
$memcache['port'] = MEMCACHE_SERVER_PORT;

$memcacheConnection = \Soul\Memcache::getConnection('master',$memcache);
\Soul\Registry::set('Memcache', $memcacheConnection);




$router = new \Application\Library\Router($routerConfig);


$modules = array('Frontend');

$app = Application::getInstance()->setRouter($router)
    ->setApplicationDir($rootDir . DIRECTORY_SEPARATOR . 'Application')
    ->setModules($modules);
$app->setModuleName('Frontend');
try {
    Application::run();
} catch (\Soul\Exception $e) {
    print $e->getMessage();
}