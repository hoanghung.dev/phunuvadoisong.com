<?php
$routerConfig[] = array('GET', '/', array('controller' => 'index', 'action' => 'index', 'module' => 'Frontend'), 'Home');
$routerConfig[] = array('GET', '/gioi-thieu.html', array('controller' => 'index', 'action' => 'about', 'module' => 'Frontend'), 'About');
//$routerConfig[] = array('GET', '/x', array('controller' => 'index', 'action' => 'x', 'module' => 'Frontend'), 'x');
$routerConfig[] = array('GET|POST', '/lien-he.html', array('controller' => 'index', 'action' => 'contact', 'module' => 'Frontend'), 'Contact');
$routerConfig[] = array('GET', '/404.html', array('controller' => 'index', 'action' => 'notFound', 'module' => 'Frontend'), 'index notFound');
/**
 *
 * Test
 */

$routerConfig[] = array('GET|POST', '/test', array('controller' => 'test', 'action' => 'index', 'module' => 'Frontend'), 'test');


/**
 * Tag
 */
$routerConfig[] = array('GET', '/tags/[*:slug]/[i:page]?', array('controller' => 'tags', 'action' => 'index', 'module' => 'Frontend'), 'tag page');

/**
 * Search
 */
$routerConfig[] = array('GET', '/search/[*:q]/[i:page]?', array('controller' => 'article', 'action' => 'search', 'module' => 'Frontend'), 'search page');

/**
 * Search
 */
$routerConfig[] = array('GET', '/[*:slug]-[i:id]/[i:page]?', array('controller' => 'category', 'action' => 'index', 'module' => 'Frontend'), 'Category page');


/**
 * News
 */
$routerConfig[] = array('GET', '/[*:category]/[*:slug]-[i:id].html', array('controller' => 'Article', 'action' => 'detail', 'module' => 'Frontend'), 'News detail');


/**
 * Sitemap
 */
$routerConfig[] = array('GET', '/sitemap.xml', array('controller' => 'index', 'action' => 'sitemap', 'module' => 'frontend'), 'Site map');