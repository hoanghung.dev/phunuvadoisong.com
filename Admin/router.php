<?php
$routerConfig[] = array('GET', '/', array('controller' => 'index', 'action' => 'index', 'module' => 'admin'), 'Home');
$routerConfig[] = array('GET', '/404.html', array('controller' => 'index', 'action' => 'notFound', 'module' => 'admin'), 'index notFound');

$routerConfig[] = array('GET', '/import', array('controller' => 'importData', 'action' => 'index', 'module' => 'admin'), 'importData');

$routerConfig[] = array('GET|POST', '/setting', array('controller' => 'setting', 'action' => 'index', 'module' => 'admin'), 'setting dashboard');


$routerConfig[] = array('GET|POST', '/page', array('controller' => 'page', 'action' => 'index', 'module' => 'admin'), 'setting page');
$routerConfig[] = array('GET|POST', '/page/add', array('controller' => 'page', 'action' => 'add', 'module' => 'admin'), 'setting page add');
$routerConfig[] = array('GET|POST', '/page/edit', array('controller' => 'page', 'action' => 'edit', 'module' => 'admin'), 'setting page edit');


$routerConfig[] = array('GET|POST', '/setting/actInsert', array('controller' => 'setting', 'action' => 'actInsert', 'module' => 'admin'), 'setting actInsert');
$routerConfig[] = array('GET|POST', '/setting/actUpdate', array('controller' => 'setting', 'action' => 'actUpdate', 'module' => 'admin'), 'setting actUpdate');
$routerConfig[] = array('GET|POST', '/setting/actDelete', array('controller' => 'setting', 'action' => 'actDelete', 'module' => 'admin'), 'setting actDelete');


$routerConfig[] = array('GET|POST', '/slider', array('controller' => 'slider', 'action' => 'index', 'module' => 'admin'), 'slider slider');
$routerConfig[] = array('GET|POST', '/slider/add', array('controller' => 'slider', 'action' => 'add', 'module' => 'admin'), 'slider slider add');
$routerConfig[] = array('GET|POST', '/slider/edit/[i:id]', array('controller' => 'slider', 'action' => 'edit', 'module' => 'admin'), 'slider slider edit');



$routerConfig[] = array('GET|POST', '/login', array('controller' => 'user', 'action' => 'login', 'module' => 'admin'), 'Login');
$routerConfig[] = array('GET', '/logout', array('controller' => 'user', 'action' => 'logout', 'module' => 'admin'), 'Logout');


$routerConfig[] = array('GET', '/permission', array('controller' => 'permission', 'action' => 'index', 'module' => 'admin'), 'permission dashboard');
$routerConfig[] = array('GET|POST', '/permission/add', array('controller' => 'permission', 'action' => 'add', 'module' => 'admin'), 'Permission add');
$routerConfig[] = array('GET|POST', '/permission/edit/[i:id]', array('controller' => 'permission', 'action' => 'edit', 'module' => 'admin'), 'permission edit');
$routerConfig[] = array('GET|POST', '/permission/actUpdate', array('controller' => 'permission', 'action' => 'actUpdate', 'module' => 'admin'), 'Permission actUpdate');
$routerConfig[] = array('GET', '/permission/permission-group', array('controller' => 'permission', 'action' => 'permissionGroup', 'module' => 'admin'), 'permission group dashboard');



$routerConfig[] = array('GET', '/user', array('controller' => 'user', 'action' => 'index', 'module' => 'admin'), 'User dashboard');
$routerConfig[] = array('GET', '/user-group', array('controller' => 'user', 'action' => 'userGroup', 'module' => 'admin'), 'User Group');
$routerConfig[] = array('GET', '/user/profile', array('controller' => 'user', 'action' => 'profile', 'module' => 'admin'), 'profile');
$routerConfig[] = array('GET', '/user/setting', array('controller' => 'user', 'action' => 'setting', 'module' => 'admin'), 'setting');
$routerConfig[] = array('GET|POST', '/user/add', array('controller' => 'user', 'action' => 'add', 'module' => 'admin'), 'addUser');
$routerConfig[] = array('GET|POST', '/user/add-group', array('controller' => 'user', 'action' => 'addGroup', 'module' => 'admin'), 'addGroup');
$routerConfig[] = array('GET|POST', '/user/edit/[i:id]', array('controller' => 'user', 'action' => 'edit', 'module' => 'admin'), 'editUser');
$routerConfig[] = array('GET|POST', '/user/edit-group/[i:id]', array('controller' => 'user', 'action' => 'editGroup', 'module' => 'admin'), 'editGroup');
$routerConfig[] = array('GET|POST', '/user/actTrash', array('controller' => 'user', 'action' => 'actTrash', 'module' => 'admin'), 'actTrash');
$routerConfig[] = array('GET|POST', '/user/actUnTrash', array('controller' => 'user', 'action' => 'actUnTrash', 'module' => 'admin'), 'actUnTrash');
$routerConfig[] = array('GET|POST', '/user/actDelete', array('controller' => 'user', 'action' => 'actDelete', 'module' => 'admin'), 'actDelete');
$routerConfig[] = array('GET|POST', '/user/actTrashGroup', array('controller' => 'user', 'action' => 'actTrashGroup', 'module' => 'admin'), 'actTrashGroup');
$routerConfig[] = array('GET|POST', '/user/actUnTrashGroup', array('controller' => 'user', 'action' => 'actUnTrash', 'module' => 'admin'), 'actUnTrashGroup');
$routerConfig[] = array('GET|POST', '/user/actDeleteGroup', array('controller' => 'user', 'action' => 'actDeleteGroup', 'module' => 'admin'), 'actDeleteGroup');
$routerConfig[] = array('GET', '/user/permissionJson', array('controller' => 'user', 'action' => 'permissionJson', 'module' => 'admin'), 'permissionJson');


$routerConfig[] = array('GET', '/category', array('controller' => 'category', 'action' => 'index', 'module' => 'admin'), 'Category dashboard');
$routerConfig[] = array('GET|POST', '/category/add', array('controller' => 'category', 'action' => 'add', 'module' => 'admin'), 'add');
$routerConfig[] = array('GET|POST', '/category/edit/[i:id]', array('controller' => 'category', 'action' => 'edit', 'module' => 'admin'), 'edit');
$routerConfig[] = array('GET|POST', '/category/actDelete', array('controller' => 'category', 'action' => 'actDelete', 'module' => 'admin'), 'Category actDelete');


$routerConfig[] = array('GET', '/article', array('controller' => 'article', 'action' => 'index', 'module' => 'admin'), 'article dashboard');
$routerConfig[] = array('GET|POST', '/article/add', array('controller' => 'article', 'action' => 'add', 'module' => 'admin'), 'article add');
$routerConfig[] = array('GET|POST', '/article/edit/[i:id]', array('controller' => 'article', 'action' => 'edit', 'module' => 'admin'), 'article edit');
$routerConfig[] = array('GET|POST', '/article/actDelete', array('controller' => 'article', 'action' => 'actDelete', 'module' => 'admin'), 'article actDelete');



$routerConfig[] = array('GET', '/customer-care', array('controller' => 'customerCare', 'action' => 'index', 'module' => 'admin'), 'customer care');
$routerConfig[] = array('GET', '/customer-care/subscriber-detail', array('controller' => 'customerCare', 'action' => 'subscriberDetail', 'module' => 'admin'), 'customer care subscriber-detail');
$routerConfig[] = array('GET', '/customer-care/subscriber-history', array('controller' => 'customerCare', 'action' => 'subscriberHistory', 'module' => 'admin'), 'customer care subscriber-history');
$routerConfig[] = array('GET', '/customer-care/momt-history', array('controller' => 'customerCare', 'action' => 'MOMTHistory', 'module' => 'admin'), 'customer care momt-history');
$routerConfig[] = array('GET', '/customer-care/send-movies', array('controller' => 'customerCare', 'action' => 'sendMovies', 'module' => 'admin'), 'customer care send-movies');
$routerConfig[] = array('GET', '/customer-care/send-link-movies-hot', array('controller' => 'customerCare', 'action' => 'sendLinkHot', 'module' => 'admin'), 'customer care send-link-movies-hot');
$routerConfig[] = array('GET', '/customer-care/register-movies-hot', array('controller' => 'customerCare', 'action' => 'registerMoviesHot', 'module' => 'admin'), 'customer care register-movies-hot');



$routerConfig[] = array('GET', '/report/score', array('controller' => 'reports', 'action' => 'statisticsScore', 'module' => 'admin'), 'report Score');
$routerConfig[] = array('GET', '/report/movies', array('controller' => 'reports', 'action' => 'statisticsMovies', 'module' => 'admin'), 'report movies');
$routerConfig[] = array('GET', '/report/revenue', array('controller' => 'reports', 'action' => 'statisticsRevenue', 'module' => 'admin'), 'report revenue');
$routerConfig[] = array('GET', '/report/category-movies', array('controller' => 'reports', 'action' => 'statisticsCategoryMovies', 'module' => 'admin'), 'report category-movies');
$routerConfig[] = array('GET', '/report/change-package', array('controller' => 'reports', 'action' => 'statisticsChangePackage', 'module' => 'admin'), 'report change-package');
$routerConfig[] = array('GET', '/report/cancel', array('controller' => 'reports', 'action' => 'statisticsCancel', 'module' => 'admin'), 'report cancel');
$routerConfig[] = array('GET', '/report/viewed-not-registered', array('controller' => 'reports', 'action' => 'statisticsViewedNotRegistered', 'module' => 'admin'), 'report viewed-not-registered');
$routerConfig[] = array('GET', '/report/downloaded-movies', array('controller' => 'reports', 'action' => 'statisticsDownloadedMovies', 'module' => 'admin'), 'report downloaded-movies');
$routerConfig[] = array('GET', '/report/registered-package', array('controller' => 'reports', 'action' => 'statisticsRegisteredPackage', 'module' => 'admin'), 'report statisticsRegisteredPackage');
$routerConfig[] = array('GET', '/report/cp', array('controller' => 'reports', 'action' => 'statisticsCP', 'module' => 'admin'), 'report cp');
$routerConfig[] = array('GET', '/report/revenue-total', array('controller' => 'reports', 'action' => 'statisticsRevenueTotal', 'module' => 'admin'), 'report revenue total');


$routerConfig[] = array('GET', '/message', array('controller' => 'message', 'action' => 'index', 'module' => 'admin'), 'message dashboard');
$routerConfig[] = array('GET', '/message_approval', array('controller' => 'message', 'action' => 'approval', 'module' => 'admin'), 'message approval dashboard');
$routerConfig[] = array('GET|POST', '/message/add', array('controller' => 'message', 'action' => 'add', 'module' => 'admin'), 'message add');
$routerConfig[] = array('GET|POST', '/message/edit/[i:id]', array('controller' => 'message', 'action' => 'edit', 'module' => 'admin'), 'message edit');
$routerConfig[] = array('GET|POST', '/message/actTrash', array('controller' => 'message', 'action' => 'actTrash', 'module' => 'admin'), 'message actTrash');
$routerConfig[] = array('GET|POST', '/message/actApproval', array('controller' => 'message', 'action' => 'actApproval', 'module' => 'admin'), 'message actApproval');
$routerConfig[] = array('GET|POST', '/message/actUnTrash', array('controller' => 'message', 'action' => 'actUnTrash', 'module' => 'admin'), 'message actUnTrash');
$routerConfig[] = array('GET|POST', '/message/actDelete', array('controller' => 'message', 'action' => 'actDelete', 'module' => 'admin'), 'message actDelete');