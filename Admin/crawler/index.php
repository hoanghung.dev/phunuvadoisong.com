<?php
ini_set("memory_limit","512M");
set_time_limit(0);
date_default_timezone_set("Asia/Ho_Chi_Minh");
error_reporting(E_ALL);
ini_set("display_errors",1);
$rootDir = dirname(dirname(dirname(__FILE__)));
setlocale(LC_MONETARY,"vi_VN");

set_include_path(get_include_path() . PATH_SEPARATOR . $rootDir);
include $rootDir .'/config.php';
include $rootDir.'/Crawler/simple_html_dom.php';

include_once $rootDir . '/Soul/Loader.php';
$loader = new \Soul\Loader();

$loader->registerNamespaces(array(
    'Soul\Mysql' => $rootDir . '\Mysql',
    'Soul\Helpers' => $rootDir ,
    'Soul\Pattern' => $rootDir . '\Pattern',
    'Application\Admin\Helpers' => $rootDir,
    'Application\Admin\Models' => $rootDir,
    'Application\Admin\Controllers' => $rootDir,
    'Application\Library' => $rootDir,
    'Soul' => $rootDir . '',
));
$loader->register();


#MySQL
$mysql['host'] = DB_SERVER;
$mysql['dbname'] = DB_DATABASE;
$mysql['username'] = DB_USERNAME;
$mysql['password'] = DB_PASSWORD;
$mysql['options'] = null;

$mysqlConnection = \Soul\Mysql::factory('main', $mysql);
\Soul\Registry::set('Mysql', $mysqlConnection);

include $rootDir.'/Crawler/Base.php';
include $rootDir.'/Crawler/Dear.php';
$eva = new \Crawl\Dear();
