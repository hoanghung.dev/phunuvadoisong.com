<?php
date_default_timezone_set("Asia/Ho_Chi_Minh");
error_reporting(E_ALL);
ini_set("display_errors",1);
$rootDir = dirname(dirname(__FILE__));
setlocale(LC_MONETARY,"vi_VN");

set_include_path(get_include_path() . PATH_SEPARATOR . $rootDir);
include dirname(__FILE__) . '/router.php';
/*include dirname(__FILE__) . '/Application/Library/Permission.php';*/
include $rootDir .'/config.php';

include_once $rootDir . '/Soul/Loader.php';
$loader = new \Soul\Loader();

$loader->registerNamespaces(array(
    'Soul\Mysql' => $rootDir . '\Mysql',
    'Soul\Helpers' => $rootDir ,
    'Soul\Pattern' => $rootDir . '\Pattern',
    'Application\Admin\Helpers' => $rootDir,
    'Application\Admin\Models' => $rootDir,
    'Application\Admin\Controllers' => $rootDir,
    'Application\Library' => $rootDir,
    'Soul' => $rootDir . '',
));
$loader->register();
use Soul\Application;

\Soul\Session::getInstance();

$session = \Soul\Session::getInstance();


#MySQL
$mysql['host'] = DB_SERVER;
$mysql['dbname'] = DB_DATABASE;
$mysql['username'] = DB_USERNAME;
$mysql['password'] = DB_PASSWORD;
$mysql['options'] = null;

$mysqlConnection = \Soul\Mysql::factory('main', $mysql);
\Soul\Registry::set('Mysql', $mysqlConnection);

#Memcache
/*define("ENABLE_MEMCACHED", false);
$memcache['host'] = MEMCACHE_SERVER;
$memcache['port'] = MEMCACHE_SERVER_PORT;

$memcacheConnection = \Soul\Memcache::getConnection('master',$memcache);
\Soul\Registry::set('Memcache', $memcacheConnection);*/


$router = new \Application\Library\Router($routerConfig);


$modules = array('Admin');

$app = Application::getInstance()->setRouter($router)
    ->setApplicationDir($rootDir . DIRECTORY_SEPARATOR . 'Application')
    ->setModules($modules);
$app->setModuleName('Admin');

try {
    Application::run();
} catch (\Soul\Exception $e) {
    print $e->getMessage();
}