<?php
date_default_timezone_set("Asia/Ho_Chi_Minh");
error_reporting(E_ALL);
ini_set("display_errors",1);
$rootDir = dirname(dirname(__FILE__));

set_include_path(get_include_path() . PATH_SEPARATOR . $rootDir);
include $rootDir .'/config.php';

include_once $rootDir . '/Soul/Loader.php';
$loader = new \Soul\Loader();

$loader->registerNamespaces(array(
    'Soul\Mysql' => $rootDir . '\Mysql',
    'Soul\Helpers' => $rootDir ,
    'Soul\Pattern' => $rootDir . '\Pattern',
    'Application\Admin\Helpers' => $rootDir,
    'Application\Admin\Models' => $rootDir,
    'Application\Admin\Controllers' => $rootDir,
    'Application\Library' => $rootDir,
    'Soul' => $rootDir . '',
));
$loader->register();
use Soul\Application;

#MySQL
$mysql['host'] = DB_SERVER;
$mysql['dbname'] = DB_DATABASE;
$mysql['username'] = DB_USERNAME;
$mysql['password'] = DB_PASSWORD;
$mysql['options'] = null;

$mysqlConnection = \Soul\Mysql::factory('main', $mysql);
\Soul\Registry::set('Mysql', $mysqlConnection);


include $rootDir .'/Admin/crawler/Phunuvadoisong.php';
$PhuNu = new \Crawler\Phunuvadoisong();
$PhuNu->getListCategory();
