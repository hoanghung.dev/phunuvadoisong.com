<?php

/**
 * Created by PhpStorm.
 * User: STEVEN
 * Date: 18/08/2016
 * Time: 12:05 SA
 */
namespace Crawl;

class Vnexpress extends Base
{
    public function __construct(){
        print "Crawler Vnexpress ...... \n";
        for ($i = 1;$i < 2; $i++){
            $this->getCategory('http://vnexpress.net/tin-tuc/oto-xe-may/page/'.$i.'.html', 17);
            $this->getCategory('http://vnexpress.net/tin-tuc/phap-luat/page/'.$i.'.html', 10);
        }
    }
    function getCategory($url, $cateId){
        print "Crawl ".$url." \n";
        $html = $this->cUrl($url);
        $html = str_get_html($html);
        if(!empty($html)) {
            for($i = 0; $i <= 2 ; $i ++) {
                if(isset($html->find('#news_home.list_news li')[$i])) {
                    $article = $html->find('#news_home.list_news li')[$i];
                    $link = $article->find('a', 0)->href;
                    $this->getDetail($link,$cateId);
                } elseif(isset($html->find('.list_news li')[$i])) {
                    $article = $html->find('.list_news li')[$i];
                    $link = $article->find('a', 0)->href;
                    $this->getDetail($link,$cateId);
                }
            }
        } else print "Don't get html category \n";
//        if(!empty($html)) foreach($html->find('#news_home li') as $article) {
//            if($article->find('a', 0)){
//                $link = $article->find('a', 0)->href;
//                $this->getDetail($link,$cateId);
//                //print $link."\n";
//            }
//        }else print "Don't get html category \n";
        $html->clear();
    }
    function getDetail($url,$cateId){

        $newsModel = new \Application\Admin\Models\News();
        print "Crawl ".$url." \n";
        $dom = $this->cUrl($url);
        $html = str_get_html($dom);
//print $html->find('meta[property="og:image"]',0)->getAttribute('content');exit;
        if(!empty($html)){
            $data['category_id'] = $cateId;
            $data['user_id'] = 1;
            $data['status'] = 1;
            $data['source'] = $url;
            $data['title']  = str_replace(' - VnExpress', '', $html->find("meta[property=og:title]",0)->getAttribute('content'));
            $data['title_page']  = $data['title'];
            $data['slug'] = $this->toSlug($data['title']);

            if($this->checkExist($data['slug']) == true){
                print $data['slug']." exist !\n";
            }else{
                $data['intro']  = $html->find('meta[name="description"]',0)->getAttribute('content');
                //$data['intro'] = str_replace('Đọc Làm đẹp trên Eva.vn','',$data['intro']);
                $data['keywords']  = $html->find("meta[name=keywords]",0)->getAttribute('content');

                if(!empty($html->find('meta[property="og:image"]'))) { $image = $html->find('meta[property="og:image"]',0)->getAttribute('content');
                    $path = '/'.date('Ym').'/'; // Directory to upload files to.
                    $thumb = $this->uploadImageURL($image,$path,$data['slug']);
                    $data['image'] = $thumb;

                    foreach ($html->find('ul.detail_other') as $element) {
                        $element->outertext = '';
                    }
                    /*foreach ($html->find('span.shareImage') as $link) {
                        $link->outertext = '';
                    }*/
                    /*foreach ($html->find('a') as $link) {
                        $link->outertext = '';
                    }*/
                    foreach ($html->find('script') as $link) {
                        $link->outertext = '';
                    }
                    //$html = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $html);
                    $data['content']  = $html->find('#box_details_news .fck_detail',0)->innertext;
                    $data['content'] = preg_replace("/\<a([^>]*)\>([^<]*)\<\/a\>/i", "$2", $data['content']);
                    $data['content'] = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $data['content']);
                    $data['content'] = preg_replace('/<ins class="adsbygoogle".*?<\/ins>/s','',$data['content']);
                    $data['content'] = preg_replace('#<iframe(.*?)>(.*?)</iframe>#is', '', $data['content']);


                    /*if(!empty($data['content'])) foreach(str_get_html($data['content'])->find("img") as $i=>$img) {
                        $image = $img->src;
                        $path = '/'.date('Ym').'/content/'; // Directory to upload files to.
                        $directory = $this->uploadImageURL($image,$path,$data['slug'].'-'.$i);
                        $data['content'] = str_replace($image, _ROOT_UPLOAD.$directory, $data['content']);
                    }*/

                    if($newsModel->insert($data)) print "Insert '.$url.' done !\n";else print "Insert '.$url.' unsuccess !\n";
                    unset($data);$html->clear();
                }
            }

        }else print "Don't get html detail\n";
    }
}