<?php
ini_set("memory_limit","512M");
set_time_limit(0);
date_default_timezone_set("Asia/Ho_Chi_Minh");
error_reporting(E_ALL);
ini_set("display_errors",1);
$rootDir = dirname(dirname(__FILE__));
setlocale(LC_MONETARY,"vi_VN");

set_include_path(get_include_path() . PATH_SEPARATOR . $rootDir);
include dirname(dirname(__FILE__)) . '/Admin/router.php';
include $rootDir .'/config.php';
include 'simple_html_dom.php';

include_once $rootDir . '/Soul/Loader.php';
$loader = new \Soul\Loader();

$loader->registerNamespaces(array(
    'Soul\Mysql' => $rootDir . '\Mysql',
    'Soul\Helpers' => $rootDir ,
    'Soul\Pattern' => $rootDir . '\Pattern',
    'Application\Admin\Helpers' => $rootDir,
    'Application\Admin\Models' => $rootDir,
    'Application\Admin\Controllers' => $rootDir,
    'Application\Library' => $rootDir,
    'Soul' => $rootDir . '',
));
$loader->register();

\Soul\Session::getInstance();

$session = \Soul\Session::getInstance();


#MySQL
$mysql['host'] = DB_SERVER;
$mysql['dbname'] = DB_DATABASE;
$mysql['username'] = DB_USERNAME;
$mysql['password'] = DB_PASSWORD;
$mysql['options'] = null;

$mysqlConnection = \Soul\Mysql::factory('main', $mysql);
\Soul\Registry::set('Mysql', $mysqlConnection);

include 'Base.php';
//include 'Phunutoday.php';
//$eva = new \Crawl\Phunutoday();
//die('ok');
//$arr = [];
//for ($i = 11225; $i <= 11243;  $i ++) {
//    $arr[] = $i;
//}
$newModel = new \Application\Admin\Models\News();
//$arr = [ 10865, 10862, 10861, 10869,  10843,  10844,  10842, 10869
//];
//$arr = $newModel->getDataArr(['category_id'=>28]);
//var_dump($arr[0]); die;
//foreach ($arr as $item) {
//    $newModel->delete('news_id=:id', [':id'=>$item]);
//}
//$newModel->delete('category_id =:id', [':id' => 28]);
//die('ok');
