<?php

/**
 * Created by PhpStorm.
 * User: STEVEN
 * Date: 18/08/2016
 * Time: 12:05 SA
 */
namespace Crawl;
class Guu extends Base
{
    public function __construct(){
        print "Crawler Guu ...... \n";
        for ($i = 1;$i < 2; $i++){
            $this->getCategory(sprintf('https://guu.vn/cat/guu-cuoc-song/%d',$i),9);
            $this->getCategory(sprintf('https://guu.vn/cat/guu-lam-dep/%d',$i),15);
        }
        //$this->getCategory(sprintf('https://guu.vn/cat/guu-cuoc-song/%d',1),9);
        //$this->getDetail('https://guu.vn/khong-tim-thay-dien-thoai-iphone-6-hanh-khach-hang-thuong-gia-tat-vao-mat-nu-tiep-vien-a5G3VQl9nCho3.html', 9);
    }
    function getCategory($url,$cateId){
        print "Crawl ".$url." \n";
        $html = $this->cUrl($url);
        $html = str_get_html($html);
        if(!empty($html)) {
            for($i = 1; $i <= 2 ; $i ++) {
                if(isset($html->find('div#latest-news div.latest-block')[$i])) {
                    $article = $html->find('div#latest-news div.latest-block')[$i];
                    $link = 'https://guu.vn'.$article->find('a', 0)->href;
                    $this->getDetail($link,$cateId);
                }
            }
        } else print "Don't get html category \n";
        $html->clear();
    }
    function getDetail($url,$cateId){
        $newsModel = new \Application\Admin\Models\News();
        print "Crawl ".$url." \n";
        $dom = $this->cUrl($url);
        $html = str_get_html($dom);
//print $html->find('meta[property="og:image"]',0)->getAttribute('content');exit;
        if(!empty($html)){
            $data['category_id'] = $cateId;
            $data['user_id'] = 1;
            $data['status'] = 1;
            $data['source'] = $url;
            $data['title']  = $html->find("meta[property=og:title]",0)->getAttribute('content');
            $data['title'] = str_replace(' - GUU.vn','',$data['title']);
            $data['title_page']  = $data['title'];
            $data['slug'] = $this->toSlug($data['title']);

            if($this->checkExist($data['slug']) == true){
                print $data['slug']." exist !\n";
            }else{
                $data['intro']  = $html->find('meta[name="description"]',0)->getAttribute('content');
                //$data['intro'] = str_replace('Đọc Làm đẹp trên Eva.vn','',$data['intro']);
                $data['keywords']  = $html->find("meta[name=keywords]",0)->getAttribute('content');

                if(!empty($html->find('meta[property="og:image"]'))) { $image = $html->find('meta[property="og:image"]',0)->getAttribute('content');
                    $path = '/'.date('Ym').'/'; // Directory to upload files to.
                    $thumb = $this->uploadImageURL($image,$path,$data['slug']);
                    $data['image'] = $thumb;
                }
                /*foreach ($html->find('ul.detail_other') as $element) {
                    $element->outertext = '';
                }*/
                /*foreach ($html->find('span.shareImage') as $link) {
                    $link->outertext = '';
                }*/
                /*foreach ($html->find('a') as $link) {
                    $link->outertext = '';
                }*/
                foreach ($html->find('script') as $link) {
                    $link->outertext = '';
                }
                //$html = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $html);
                $data['content']  = $html->find('article#body',0)->innertext;
                $data['content'] = preg_replace("/\<a([^>]*)\>([^<]*)\<\/a\>/i", "$2", $data['content']);


                /*if(!empty($data['content'])) foreach(str_get_html($data['content'])->find("img") as $i=>$img) {
                    $image = $img->src;
                    $path = '/'.date('Ym').'/content/'; // Directory to upload files to.
                    $directory = $this->uploadImageURL($image,$path,$data['slug'].'-'.$i);
                    $data['content'] = str_replace($image, _ROOT_UPLOAD.$directory, $data['content']);
                }*/
                if($newsModel->insert($data)) print "Insert '.$url.' done !\n";else print "Insert '.$url.' unsuccess !\n";
                unset($data);$html->clear();

                //print_r($data);
            }

        }else print "Don't get html detail\n";
    }
}