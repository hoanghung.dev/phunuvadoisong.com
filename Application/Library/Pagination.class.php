<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 22/07/2015
 * Time: 01:53 CH
 */
class Pagination {
    private $_limit;
    private $_param;
    private $_link;
    private $_page;
    private $_total;
    private $_class;

    public function __construct($params) {
        $this->_total = isset($params['total'])?$params['total']:1000;
        $this->_limit = isset($params['limit'])?$params['limit']:30;
        $this->_param = isset($params['param'])?$params['param']:'?page=';
        $this->_link = isset($params['link'])?$params['link']:'';
        $this->_page = isset($params['page'])?$params['page']:1;
        $this->_class = isset($params['class'])?$params['class']:'pagination';

    }
    public function createLinks() {

        if ( $this->_limit == '' ) {
            return '';
        }
        $links= 5;
        $last       = ceil( $this->_total / $this->_limit );

        $start      = ( ( $this->_page - $links ) > 0 ) ? $this->_page - $links : 1;
        $end        = ( ( $this->_page + $links ) < $last ) ? $this->_page + $links : $last;

        $html       = '<ul class="' . $this->_class . '">';

        $class      = ( $this->_page == 1 ) ? "disabled" : "";
        $html       .= '<li class="' . $class . '"><a href="'.$this->_link.'">&laquo;</a></li>';

        if ( $start > 1 ) {
            $html   .= '<li><a href="'.$this->_link.'?page=1">1</a></li>';
            $html   .= '<li class="disabled"><span>...</span></li>';
        }

        for ( $i = $start ; $i <= $end; $i++ ) {
            $class  = ( $this->_page == $i ) ? "active" : "";
            $html   .= '<li class="' . $class . '"><a href="'.$this->_link.$this->_param . $i . '">' . $i . '</a></li>';
        }

        if ( $end < $last ) {
            $html   .= '<li class="disabled"><span>...</span></li>';
            $html   .= '<li><a href="'.$this->_link.$this->_param . $last . '">' . $last . '</a></li>';
        }

        $class      = ( $this->_page == $last ) ? "disabled" : "";
        $html       .= '<li class="' . $class . '"><a href="'.$this->_link.$this->_param . ( $this->_page + 1 ) . '">&raquo;</a></li>';

        $html       .= '</ul>';

        return $html;
    }
}