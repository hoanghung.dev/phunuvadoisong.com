<?php

/**
 * JPhpMailer class file.
 *
 * @version alpha 2 (2010-6-3 16:42)
 * @author jerry2801 <jerry2801@gmail.com>
 * @required PHPMailer v5.1
 *
 * A typical usage of JPhpMailer is as follows:
 * <pre>
 * Yii::import('ext.phpmailer.JPhpMailer');
 * $mail=new JPhpMailer;
 * $mail->IsSMTP();
 * $mail->Host='smpt.163.com';
 * $mail->SMTPAuth=true;
 * $mail->Username='yourname@yourdomain';
 * $mail->Password='yourpassword';
 * $mail->SetFrom('name@yourdomain.com','First Last');
 * $mail->Subject='PHPMailer Test Subject via smtp, basic with authentication';
 * $mail->AltBody='To view the message, please use an HTML compatible email viewer!';
 * $mail->MsgHTML('<h1>JUST A TEST!</h1>');
 * $mail->AddAddress('whoto@otherdomain.com','John Doe');
 * $mail->Send();
 * </pre>
 */
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'class.phpmailer.php';
//require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'class.phpmailer.php';
class JPhpMailer extends PHPMailer {

    public function sendMailSmtp($from, $pass , $to, $subject, $content) {
        $this->IsSMTP();
		$this->SMTPSecure = 'ssl'; 
        $this->Host = 'smtp.gmail.com';
        $this->Port = 465;
        $this->SMTPAuth = true;
        $this->Username = $from;
        $this->Password = $pass;
        $this->Subject = $subject;
        $this->AltBody = 'To view the message, please use an HTML compatible email viewer!';
        $this->MsgHTML($content);
        $toMail = getMail($to);
        foreach($toMail as $email => $name)
        {

            $this->AddAddress($email, $name);
        }
        if (!$this->Send()) {
            echo "Mailer Error: " . $this->ErrorInfo;
            writeLog('alert_'.date('Ymd'),"SEND_MAIL=>".date('Y-m-d H:i:s').": Mailer Error: ".$this->ErrorInfo);
        } else {
            echo "Message sent!";
            writeLog('alert_'.date('Ymd'),"SEND_MAIL=>".date('Y-m-d H:i:s').": Message sent !");
        }
    }

}