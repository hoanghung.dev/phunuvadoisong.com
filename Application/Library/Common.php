<?php
namespace Application\Library;

use Soul\Registry,
    Soul\Redis;

class Common
{
    static function createAppToken($id, $ip)
    {
        $appList = Registry::get('Apps');
        if (!isset($appList[$id])) return false;
        $secret = $appList[$id]['secret'];
        $tokenRaw = $id . '|' . $ip . '|' . $secret;
        return hash('sha256', $tokenRaw);
    }

    static function createUserToken($id, $user, $ip)
    {
        $appList = Registry::get('Apps');
        if (!isset($appList[$id])) return false;
        $secret = $appList[$id]['secret'];
        $tokenRaw = $id . '|' . $secret . '|' . $ip . '|' . $user;
        $token = $id . '|' . $user . '|' . hash('sha256', $tokenRaw);
        $redis = Redis::factory('master', Registry::get('redisConfig'));
        $redis->set('UserToken:' . $user, $token, 3600*24);
        return $token;
    }

    static function checkUserToken($token)
    {
        if (empty($token)) return false;
        $tokenPart = explode('|', $token);
        if (!isset($tokenPart[1])) return false;
        $user = $tokenPart[1];
        $redis = Redis::factory('master', Registry::get('redisConfig'));
        $userToken = $redis->get('UserToken:' . $user);
        if ($userToken == null) return false;
        return $user;
    }


    static function detectPhoneNumber()
    {
        $api = new Payment();
        $result = $api->detectNumber('TOIDEP');

        $result = json_decode($result);
        if ($result->errorCode != 0) {
            return false;
        }
        header("location: $result->data");
        exit;
    }


    static function videoApi($id, $ip)
    {
        $timestr = time();
        $str = "MOBI_PORTAL" . $id . $timestr . $ip . 'Mobi@Portal!@#';
        $hash = md5($str);
        $link = "http://vapi.kenh1.vn.vn/streaming/link?partner=MOBI_PORTAL&videoId=" . $id . "&timestamp=" . $timestr . "&userIp=" . $ip . "&hash=" . $hash;
        $link = file_get_contents($link);
        return $link = json_decode($link, true);
    }

    static function wapCharge()
    {
        #@todo warp charge
        return true;
    }


    static function postAsync($url, $params)
    {
        foreach ($params as $key => &$val) {
            if (is_array($val)) $val = implode(',', $val);
            $post_params[] = $key . '=' . urlencode($val);
        }
        $post_string = implode('&', $post_params);

        $parts = parse_url($url);

        $fp = fsockopen($parts['host'],
            isset($parts['port']) ? $parts['port'] : 80,
            $errno, $errstr, 30);

        $out = "POST " . $parts['path'] . " HTTP/1.1\r\n";
        $out .= "Host: " . $parts['host'] . "\r\n";
        $out .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $out .= "Content-Length: " . strlen($post_string) . "\r\n";
        $out .= "Connection: Close\r\n\r\n";
        if (isset($post_string)) $out .= $post_string;

        fwrite($fp, $out);
        fclose($fp);
    }


    static function clientIpAddress()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return explode(',', $ip)[0];
    }

    static function encrypt($string, $key)
    {
        $result = '';
        $string = json_encode($string);
        for ($i = 0; $i < strlen($string); $i++) {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % strlen($key)) - 1, 1);
            $char = chr(ord($char) + ord($keychar));
            $result .= $char;
        }

        return base64_encode($result);
    }

    static function decrypt($string, $key)
    {
        $result = '';
        $string = base64_decode($string);

        for ($i = 0; $i < strlen($string); $i++) {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % strlen($key)) - 1, 1);
            $char = chr(ord($char) - ord($keychar));
            $result .= $char;
        }
        $result = json_decode($result);
        return $result;
    }


}

