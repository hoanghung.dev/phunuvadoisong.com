<?php
namespace Application\Library;

class Payment
{
    public $user = 'kenh1';
    public $secret = '123';
    public $token;
    public $requestTime;
    public $requestId;
    public $mode = 'TEST';
    public $host = 'http://125.212.193.234:9003/api';

    /**\
     * mobile=mobile&model=TEST&password=password&requestId=34234234&requestTime=1414633801&serect=NBNJmaRAsEzgrMZ&user=toidep&username=username
     *
     * @param $username
     * @param $password
     * @return string
     *
     */
    public function __construct()
    {
        $time = time();
        # 053yyMMddHHmmss123
        $this->requestId = sprintf('053%s123', date('ymdHis'));
        $this->requestTime = $time;

    }

    public function getToken($params)
    {
        unset($params['token']);
        ksort($params);
        $raw = http_build_query($params);
        $token = hash_hmac('sha256', $raw, $this->secret);
        return $this->token = $token;
    }

    /**
     *   http://localhost:9001/api/register?user=kenh1&secret=NXhQrLkFkJaCp4d&token=xxxx&mode=TEST&requestTime=1414641250690&requestId=2&username=840982800008&password=12344
     */
    public function register($username, $password)
    {

        $params['user'] = $this->user;
        $params['secret'] = $this->secret;
        $params['mode'] = $this->mode;
        $params['requestTime'] = $this->requestTime;
        $params['requestId'] = $this->requestId;
        $params['username'] = $username;
        $params['password'] = $password;
        $params['token'] = $this->getToken($params);
        $requestUrl = $this->host . '/register?' . http_build_query($params);
        return $this->curl($requestUrl);
    }

    public function userInfo($username) {
        $params['user'] = $this->user;
        $params['secret'] = $this->secret;
        $params['mode'] = $this->mode;
        $params['requestTime'] = $this->requestTime;
        $params['requestId'] = $this->requestId;
        $params['username'] = $username;
        $params['token'] = $this->getToken($params);
        $requestUrl = $this->host . '/userinfo?' . http_build_query($params);
        return $this->curl($requestUrl);
    }

    public function changePassword($username, $password, $newpassword) {
        $params['user'] = $this->user;
        $params['secret'] = $this->secret;
        $params['mode'] = $this->mode;
        $params['requestTime'] = $this->requestTime;
        $params['requestId'] = $this->requestId;
        $params['username'] = $username;
        $params['password'] = $newpassword;
        $params['oldpassword'] = $password;
        $params['token'] = $this->getToken($params);
        $requestUrl = $this->host . '/changpass?' . http_build_query($params);
        return $this->curl($requestUrl);
    }


    public function detectNumber($sub) {
        $params['user'] = $this->user;
        $params['secret'] = $this->secret;
        $params['mode'] = $this->mode;
        $params['requestTime'] = $this->requestTime;
        $params['requestId'] = $this->requestId;
        $params['SUB'] = $sub;
        $params['SOURCE'] = 'WAP';
        $params['token'] = $this->getToken($params);
        $requestUrl = $this->host . '/mobile?' . http_build_query($params);
        return $this->curl($requestUrl);

    }


    public function card($username, $cardCode, $cardSeri) {
        $params['user'] = $this->user;
        $params['secret'] = $this->secret;
        $params['mode'] = $this->mode;
        $params['requestTime'] = $this->requestTime;
        $params['requestId'] = $this->requestId;
        $params['username'] = $username;
        $params['cardCode'] = $cardCode;
        $params['cardSeri'] = $cardSeri;
        $params['token'] = $this->getToken($params);
        $requestUrl = $this->host . '/charingCard?' . http_build_query($params);
        echo $requestUrl;
        return $this->curl($requestUrl);
    }

    /**
     * @param $sub
     * @param $cate
     * @param $item
     * @param $subCP
     * @param $content
     * @param $price
     * @param $mobile
     * @param $source
     * @param $cmd
     * @return array|mixed
     */
    public function sub($sub, $cate, $item, $subCP, $content, $price, $mobile, $source, $cmd ) {
        $params['user'] = $this->user;
        $params['secret'] = $this->secret;
        $params['mode'] = $this->mode;
        $params['requestTime'] = $this->requestTime;
        $params['requestId'] = $this->requestId;
        $params['SUB'] = $sub;
        $params['CATE'] = $cate;
        $params['ITEM'] = $item;
        $params['SUB_CP'] = $subCP;
        $params['CONT'] = $content;
        $params['PRICE'] = $price;
        $params['MOBIE'] = $mobile;
        $params['SOURCE'] = $source;
        $params['CMD'] = $cmd;
        $params['token'] = $this->getToken($params);
        $requestUrl = $this->host . '/charingSub?' . http_build_query($params);
        file_put_contents('tttt.txt',$requestUrl."\n");
        return $this->curl($requestUrl);

    }


    public function curl($url, $method = 'GET', $data = false, $headers = false, $returnInfo = false)
    {
        $ch = curl_init();

        if ($method == 'POST') {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            if ($data !== false) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            }
        } else {
            if ($data !== false) {
                if (is_array($data)) {
                    $dataTokens = array();
                    foreach ($data as $key => $value) {
                        array_push($dataTokens, urlencode($key) . '=' . urlencode($value));
                    }
                    $data = implode('&', $dataTokens);
                }
                curl_setopt($ch, CURLOPT_URL, $url . '?' . $data);
            } else {
                curl_setopt($ch, CURLOPT_URL, $url);
            }
        }
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        if ($headers !== false) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        $contents = curl_exec($ch);

        if ($returnInfo) {
            $info = curl_getinfo($ch);
        }

        curl_close($ch);

        if ($returnInfo) {
            return array('contents' => $contents, 'info' => $info);
        } else {
            return $contents;
        }
    }
}