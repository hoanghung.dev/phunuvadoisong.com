<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 05/05/2015
 * Time: 09:45 SA
 */
?>
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Quản lý nội dung
            <small>Danh sách nội dung</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Quản lý nội dung</a></li>
            <li class="active">Danh sách nội dung</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Danh sách nội dung (<?php echo $this->total; ?>)</h3>
                        <div class="box-tools">
                            <div class="input-group" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                                <div class="input-group-btn">
                                    <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <?php echo $this->flash->message();?>
                        <?php if (!empty($this->data)): ?>
                            <table class="table table-hover">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Image</th>
                                    <th>Category</th>
                                    <th>HOT</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                <?php foreach ($this->data as $item): $oneCate = $this->getCategory($item->category_id,'title'); ?>
                                <tr data-id="<?php echo $item->news_id; ?>">
                                    <td><?php echo $item->news_id; ?></td>
                                    <td><?php echo $item->title; ?></td>
                                    <td><img width="200" src="<?php echo $this->getImageNews($item->news_id,200,100); ?>" align="Image"></td>
                                    <td><?php echo isset($oneCate->title)?$oneCate->title:'Trang chủ'; ?></td>
                                    <td><?php if($item->is_hot == true) echo "<span class=\"badge bg-red\">HOT</span>"; ?></td>
                                    <td><?php if($item->status == 1): ?><span class="label label-success">Hiển thị</span><?php else: ?><span class="label label-danger">Không hiển thị</span><?php endif; ?></td>
                                    <td>
                                        <button type="button" class="btn btn-success" onclick="javascript: window.open('/<?php echo $this->params['router']['controller']; ?>/edit/<?php echo $item->news_id; ?>','_blank');"><i class="fa fa fa-edit"></i></button>
                                        <button type="button" class="btn btn-danger btnDelete"><i class="fa fa-times"></i></button>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        <?php endif; ?>
                    </div><!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <ul class="pagination pagination-sm no-margin center-block">
                            <?php echo isset($this->paging)?$this->paging:''; ?>
                        </ul>
                    </div>
                </div><!-- /.box -->
            </div>
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div>