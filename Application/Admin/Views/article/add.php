<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 05/05/2015
 * Time: 09:45 SA
 */
?>
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Quản lý Game
            <small>Thêm mới</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Quản lý Game</a></li>
            <li class="active">Thêm mới</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Thêm mới Game</h3>
                        <?php echo $this->flash->message();?>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form id="myForm" data-toggle="validator" role="form" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" name="title" class="form-control" placeholder="Tiêu đề" required >
                                <span class="help-block with-errors"></span>
                            </div>
                            <div class="form-group">
                                <label>Tiêu đề SEO</label>
                                <input type="text" name="title_page" class="form-control" data-maxlength="110" placeholder="Tiêu đề SEO" required >
                                <span class="help-block with-errors"></span>
                            </div>
                            <div class="form-group">
                                <label>Mô tả SEO</label>
                                <textarea name="intro" class="form-control" rows="3" required></textarea>
                                <span class="help-block with-errors"></span>
                            </div>
                            <div class="form-group">
                                <label>Từ khóa SEO</label>
                                <input type="text" name="keywords" class="form-control" placeholder="Keyword 1, keyword 2,keyword 3"  required>
                                <span class="help-block with-errors"></span>
                            </div>
                            <div class="form-group">
                                <label>HOT</label>
                                <input class="switch-check" type="checkbox" name="is_hot">
                            </div>
                            <div class="form-group">
                                <label>Ảnh</label>
                                <input id="fileUpload" name="image" class="file" type="file">
                                <div id="image-holder"></div>
                            </div>
                            <div class="form-group">
                                <label>Chuyên mục</label>
                                <select name="category_id" class="form-control">
                                    <?php echo $this->selectCategory(); ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea name="content" class="form-control tinymce" rows="8"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Trạng thái</label>
                                <input class="switch-check" type="checkbox" name="status" checked>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-default">Thêm mới</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        </div>
                    </form>
                </div><!-- /.box -->
            </div><!--/.col (left) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div>