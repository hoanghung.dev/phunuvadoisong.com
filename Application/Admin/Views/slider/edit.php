<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 05/05/2015
 * Time: 09:45 SA
 */
$item = $this->data;
?>
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Quản lý slider
            <small>Cập nhật</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Quản lý slider</a></li>
            <li class="active">Cập nhật</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cập nhật slider</h3>
                        <?php echo $this->flash->message();?>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form id="myForm" data-toggle="validator" role="form" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" name="title" class="form-control" value="<?php echo $item->title; ?>" placeholder="Tiêu đề" required>
                                <span class="help-block with-errors"></span>
                            </div>
                            <div class="form-group">
                                <label>Nội dung</label>
                                <input type="text" name="intro" class="form-control" value="<?php echo $item->intro; ?>" placeholder="Nội dung" required>
                                <span class="help-block with-errors"></span>
                            </div>
                            <div class="form-group">
                                <label>Link</label>
                                <input type="text" name="link" class="form-control" value="<?php echo $item->link; ?>" placeholder="link" required>
                                <span class="help-block with-errors"></span>
                            </div>
                            <div class="form-group">
                                <label>Ảnh</label>
                                <input id="fileUpload" name="image" class="file" type="file">
                                <div id="image-holder"><img src="<?php echo _ROOT_UPLOAD.'/'.$item->image; ?>"></div>
                            </div>
                            <div class="form-group">
                                <label>Trạng thái</label>
                                <input class="switch-check" type="checkbox" name="status" <?php if($item->status == true) echo "checked"; ?>>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-default">Cập nhật</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        </div>
                    </form>
                </div><!-- /.box -->
            </div><!--/.col (left) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div>