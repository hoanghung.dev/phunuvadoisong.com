<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 05/05/2015
 * Time: 09:45 SA
 */
?>
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Quản lý slider
            <small>Danh sách slider</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Quản lý slider</a></li>
            <li class="active">Danh sách slider</li>
        </ol>
    </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Danh sách slider</h3>

                            <div class="box-tools">
                                <div class="input-group" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                                    <div class="input-group-btn">
                                        <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                            <?php echo $this->flash->message(); ?>
                            <?php if (!empty($this->data)): ?>
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Ảnh slider</th>
                                    <th>Title</th>
                                    <th>Link</th>
                                    <th class="text-center">ACTION</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($this->data as $item): ?>
                                    <tr data-id="<?php echo $item->page_id; ?>">
                                        <td><?php echo $item->page_id; ?></td>
                                        <td><img src="<?php echo _ROOT_UPLOAD.'/'.$item->image; ?>" width="300" height="100"></td>
                                        <td><?php echo $item->title; ?></td>
                                        <td><?php echo $item->link ?></td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-success" onclick="javascript: window.open('/slider/edit/<?php echo $item->page_id; ?>','_blank');"><i class="fa fa-edit"></i></button>
                                            <button type="button" class="btn btn-danger btnDelete"><i class="fa fa-times"></i></button>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <?php endif; ?>
                        </div><!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <ul class="pagination pagination-sm no-margin center-block">
                                <?php echo isset($this->page)?$this->page:''; ?>
                            </ul>
                        </div>
                    </div><!-- /.box -->
                </div>
            </div>   <!-- /.row -->
        </section><!-- /.content -->

</div>