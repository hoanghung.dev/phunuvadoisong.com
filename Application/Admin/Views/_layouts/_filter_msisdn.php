<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 03/09/2015
 * Time: 10:01 SA
 */
?>
<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            Tìm kiếm
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <form role="form" method="GET" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
                        <div class="form-group">
                            <input type="text" name="msisdn" class="form-control" value="<?php echo isset($_GET['msisdn'])?$_GET['msisdn']:''; ?>" placeholder="Nhập số điện thoại..">
                        </div>
                        <button type="submit" class="btn btn-primary">Tìm kiếm</button>
                        <button type="reset" class="btn btn-danger">Reset</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>