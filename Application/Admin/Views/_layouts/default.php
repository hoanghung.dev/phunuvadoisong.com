<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CMS | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="/plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="/plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="/plugins/datepicker/datepicker3.css">
    <link href="/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="/plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- jQuery 2.1.4 -->
    <script src="/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="/plugins/tinymce/plugins/moxiemanager/js/moxman.loader.min.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php echo $this->action('boxHeader','block','admin',$this->params); ?>
    <?php echo $this->layoutContent; ?>
</div>

<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="/plugins/datepicker/bootstrap-datepicker.js"></script>

<script src="/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>

<script src="/plugins/bootstrap-validator-master/dist/validator.min.js"></script>

<!-- Slimscroll -->
<script src="/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/plugins/fastclick/fastclick.min.js"></script>

<!--<script src="/plugins/tinymce/jquery.tinymce.js"></script>-->
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<!-- AdminLTE App -->
<script src="/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--<script src="/dist/js/pages/dashboard.js"></script>-->
<!-- AdminLTE for demo purposes -->
<script src="/dist/js/demo.js"></script>
<?php
$moduleName = $this->params['router']['controller'];
?>
<script type="text/javascript">
    $(document).ready(function(){
        jQuery('ul.sidebar-menu li.<?php echo $this->params['router']['controller'];?>').addClass('active');
        jQuery('ul.sidebar-menu li.<?php echo $this->params['router']['controller'].'_'.$this->params['router']['action'];?>').addClass('active');
        $('#myForm').validator();
        $("input[name='status'],input[name='is_hot']").bootstrapSwitch();

        $("#fileUpload").on('change', function () {

            //Get count of selected files
            var countFiles = $(this)[0].files.length;

            var imgPath = $(this)[0].value;
            var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
            var image_holder = $("#image-holder");
            image_holder.empty();

            if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
                if (typeof (FileReader) != "undefined") {

                    //loop for each file selected for uploaded.
                    for (var i = 0; i < countFiles; i++) {

                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $("<img />", {
                                "src": e.target.result,
                                "class": "thumb-image",
                                "style" : "width: 200px; padding: 10px 0;"
                            }).appendTo(image_holder);
                        };

                        image_holder.show();
                        reader.readAsDataURL($(this)[0].files[i]);
                    }

                } else {
                    alert("This browser does not support FileReader.");
                }
            } else {
                alert("Vui lòng chọn file ảnh");
            }
        });

        $('.btnDelete').click(function(){
            var id = $(this).parents('tr').attr('data-id');
            console.log(id);
            if(confirm("Bạn có chắc chắn xóa <?php echo $moduleName; ?> này khỏi hệ thống không ?") == true){
                $.ajax({
                    type: 'POST',
                    url: '/<?php echo $moduleName; ?>/actDelete',
                    data: {id:id},
                    success: function(response){
                        console.log(response);
                        if(response == true) alert('Xóa <?php echo $moduleName; ?> thành công !');
                        else alert('Xóa <?php echo $moduleName; ?> không thành công !');
                        location.reload();
                    }
                })
            }
        });
        $('.showInsert').click(function(){
            $('.add-slider').toggleClass('hidden');
        });
        $('.btnUpdate').click(function(){
            var id = $(this).parents('tr').attr('data-id');
            var position = $(this).parents('tr').find('input[name="position"]').val();
            var image = $(this).parents('tr').find('img.previewing').attr('src');
            var title = $(this).parents('tr').find('input[name="title"]').val();
            var link = $(this).parents('tr').find('input[name="link"]').val();


            if(confirm("Bạn có chắc chắn sửa Slider này không ?") == true){
                $.ajax({
                    type: "POST",
                    url: "/setting/actUpdateSlider",
                    data: {id:id,position:position,image:image,title:title,link:link},
                    beforeSend: function(){
                        console.log('Đang update');
                        $('.ajax-loader').removeClass('hidden');
                    },
                    success: function(response){
                        console.log(response);
                        $('.ajax-loader').addClass('hidden');
                        if(response){
                            alert(response);
                            location.reload();
                        }
                    }
                })
            }

        });
        $('.btnInsert').click(function(){
            var position = $(this).parents('tr').find('input[name="position"]').val();
            var image = $(this).parents('tr').find('img.previewing').attr('src');
            var title = $(this).parents('tr').find('input[name="title"]').val();
            var link = $(this).parents('tr').find('input[name="link"]').val();


            if(confirm("Bạn có chắc chắn thêm Slider này không ?") == true){
                //alert('Thêm mới thành công !');
                //location.reload();
                $.ajax({
                    type: "POST",
                    url: "/setting/actInsertSlider",
                    data: {position:position,image:image,title:title,link:link},
                    beforeSend: function(){
                        console.log('Đang insert');
                        $('.ajax-loader').removeClass('hidden');
                    },
                    success: function(response){
                        console.log(response);
                        $('.ajax-loader').addClass('hidden');
                        if(response){
                            alert(response);
                            location.reload();
                        }
                    }
                })
            }
        });
        $('table tr th.group').each(function(){
            var role = $(this).attr('data-perm');
            var allPermission=role.split('|');
            var col = $(this).attr('data-col');
            //console.log(allPermission);
            $('table tr td:nth-child('+col+')').each(function(){
                var permId = $(this).parents('tr').attr('data-id');
                if ($.inArray(permId,allPermission) > -1){
                    //console.log(permId+' Checked');
                    $(this).find('input[type="checkbox"]').attr("checked","checked");
                }
            });
        });
        $('input[type="checkbox"].btnCheckAll').click(function(){
            var col = $(this).parents('th').attr('data-col');
            if($(this).is(":checked")) {
                $('table tr td:nth-child('+col+') input').each(function(){
                    this.checked = true;
                });
            } else {
                $('table tr td:nth-child('+col+') input').each(function(){
                    this.checked = false;
                });
            }

        });


        $('.btnUpdatePerm').click(function(){
            console.clear();
            $('table tr th.group').each(function(){
                var groupId = $(this).attr('data-id');
                var col = $(this).attr('data-col');
                var allPermission = '';
                $('table tr td:nth-child('+col+')').each(function(){

                    var permId = $(this).parents('tr').attr('data-id');
                    var checked = $(this).val();
                    if ($(this).find('input[type="checkbox"]').is(":checked")){
                        if(allPermission != ''){
                            allPermission += '|'+permId;
                        }else{
                            allPermission += permId;
                        }
                    }
                    $(this).find('i').addClass('s25 fa fa-smile-o pull-right');
                });
                $.ajax({
                    type:'POST',
                    url: '/permission/actUpdate',
                    data: {id: groupId,permission:allPermission},
                    beforeSend: function(){
                        $('.ajax-loader').removeClass('hidden');
                    },
                    success: function(response) {
                        /*alert(response);
                         location.reload();
                         }*/
                        console.log(response);
                        $('.ajax-loader').html('<div class="alert alert-success alert-dismissable col-lg-7 pull-right" style="margin: 0;padding: 5px;">'+response+'</div>');
                    }
                });

                console.log(allPermission);
                ///$('table tr td:nth-child('+col+') i').addClass('s25 fa fa-smile-o pull-right');
            });
            /*var id = $(this).parents('tr').attr('data-id');
             if(confirm("Bạn có chắc chắn xóa tạm bản ghi này không ?") == true){
             $.ajax({
             url: '/permission/actTrash',
             data: {id:id},
             success: function(response){
             alert(response);
             location.reload();
             }
             })
             }*/

        });
        setTimeout(loadTinyMce(),2000);
    });
    function loadTinyMce(){
        tinymce.PluginManager.load('moxiemanager', '/plugins/tinymce/plugins/moxiemanager/plugin.min.js');
        tinymce.init({
            selector: "textarea.tinymce",
            plugins: [
                "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons template textcolor paste textcolor colorpicker textpattern moxiemanager link image",
            ],

            toolbar1: "newdocument | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft insertfile link image",

            menubar: false,
            toolbar_items_size: 'small',

            relative_urls : false,
            remove_script_host : false,
            convert_urls : true,
            document_base_url: '<?php echo _ROOT_UPLOAD; ?>',
            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],

            templates: [
                {title: 'Test template 1', content: 'Test 1'},
                {title: 'Test template 2', content: 'Test 2'}
            ]
        })
    };
</script>
</body>

</html>
