
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Thông báo</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="alert alert-warning fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Bạn không có quyền truy cập vào trang này. Vui lòng liên hệ Ban Quản trị để được giúp đỡ !</strong>
        </div>

    </div>
    <!-- /#page-wrapper -->
