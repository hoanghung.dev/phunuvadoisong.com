<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 05/05/2015
 * Time: 09:45 SA
 */
$oneItem = $this->data;
?>
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Quản lý phân quyền
            <small>Cập nhật</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Quản lý phân quyền</a></li>
            <li class="active">Cập nhật</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cập nhật phân quyền</h3>
                        <?php echo $this->flash->message();?>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form id="myForm" data-toggle="validator" role="form" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Tên module</label>
                                <input name="title" class="form-control" placeholder="Title" value="<?php echo $oneItem->title; ?>" required>
                                <span class="help-block with-errors"></span>
                            </div>
                            <div class="form-group">
                                <label>Mã module</label>
                                <input type="text" name="module_key" class="form-control" placeholder="Mã module"  value="<?php echo $oneItem->module; ?>"  required>
                                <span class="help-block with-errors"></span>
                            </div>
                            <div class="form-group">
                                <label>Module cha</label>
                                <select name="parent_id" class="form-control">
                                    <?php $listParent = $this->selectPermission($oneItem->parent_id); ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Trạng thái</label>
                                <input class="switch-check" type="checkbox" name="status" <?php if($oneItem->status == 1) echo "checked"; ?>>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-default">Cập nhật</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        </div>
                    </form>
                </div><!-- /.box -->
            </div><!--/.col (left) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div>