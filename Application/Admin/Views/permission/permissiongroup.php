<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 05/05/2015
 * Time: 09:45 SA
 */
?>
<style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;border-color:#999;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#26ADE4;}
    .tg .tg-5y5n{background-color:#ecf4ff}
    .tg .tg-uhkr{background-color:#efefef}
    .tg tr:hover td{background-color: rgb(147, 192, 139) !important;}
    .color-red{
        background-color: #FF0000 !important;}
    i.s25
    {
        font-size:25px;
    }
</style>
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Quản lý phân quyền
            <small>Danh sách phân quyền</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Quản lý phân quyền</a></li>
            <li class="active">Danh sách phân quyền</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Bảng phân quyền theo nhóm thành viên</h3>
                        <button type="button" class="btn btn-primary btnUpdatePerm">Cập nhật quyền</button>
                        <span class="ajax-loader hidden"><img src="../images/ajax-loader-3.gif"> </span>
                        <div class="box-tools">
                            <div class="input-group" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                                <div class="input-group-btn">
                                    <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <?php echo $this->flash->message(); ?>
                        <?php if($this->listPermission && $this->listGroup): ?>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover tg">
                                        <tr>
                                            <th class="tg-5y5n"></th>
                                            <?php $i = 2; $countGroup = count($this->listGroup); foreach($this->listGroup as $item):
                                                //$arrPerm = explode('|',$item->role_permission);
                                                //$listPerm = implode(',',$arrPerm);
                                                ?>
                                                <th class="tg-031e text-center group" data-perm="<?php echo $item->role_permission; ?>" data-col="<?php echo $i++; ?>" data-id="<?php echo $item->group_id; ?>">
                                                    <strong><?php echo $item->title; ?></strong>
                                                    <input type="checkbox" class="btnCheckAll"> Check All
                                                </th>
                                            <?php endforeach; ?>
                                        </tr>
                                        <?php foreach($this->listPermission as $items):
                                            $child = $this->getPermission($items->permission_id);
                                            ?>
                                            <tr data-id="<?php echo $items->permission_id; ?>" >
                                                <td class="tg-uhkr col-lg-3"><strong><?php echo $items->title; ?></strong></td>
                                                <?php if($countGroup) for($j = 1;$j <= $countGroup;$j++): ?>
                                                    <td class="tg-031e text-center"><input type="checkbox" class="btn btn-default btn-sm"><i class=""></i></td>
                                                <?php endfor; ?>
                                                <!--<td class="tg-031e text-center"><input type="checkbox" class="btn btn-default btn-sm"><i class=""></i></td>
                                                <td class="tg-031e text-center"><input type="checkbox" class="btn btn-default btn-sm"><i class=""></i></td>
                                                <td class="tg-031e text-center"><input type="checkbox" class="btn btn-default btn-sm"><i class=""></i></td>
                                                <td class="tg-031e text-center"><input type="checkbox" class="btn btn-default btn-sm"><i class=""></i></td>-->
                                            </tr>
                                            <?php if(!empty($child)) foreach($child as $item): ?>
                                            <tr data-id="<?php echo $item->permission_id; ?>" >
                                                <td class="tg-uhkr col-lg-3"><i class="fa fa-long-arrow-right fa-w">&nbsp;</i><?php echo $item->title; ?></td>
                                                <?php if($countGroup) for($j = 1;$j <= $countGroup;$j++): ?>
                                                    <td class="tg-031e text-center"><input type="checkbox" class="btn btn-default btn-sm"><i class=""></i></td>
                                                <?php endfor; ?>
                                                <!--<td class="tg-031e text-center"><input type="checkbox" class="btn btn-default btn-sm"><i class=""></i></td>
                                                <td class="tg-031e text-center"><input type="checkbox" class="btn btn-default btn-sm"><i class=""></i></td>
                                                <td class="tg-031e text-center"><input type="checkbox" class="btn btn-default btn-sm"><i class=""></i></td>
                                                <td class="tg-031e text-center"><input type="checkbox" class="btn btn-default btn-sm"><i class=""></i></td>-->
                                            </tr>
                                        <?php endforeach; ?>
                                        <?php endforeach; ?>

                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        <?php endif; ?>
                    </div><!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <ul class="pagination pagination-sm no-margin center-block">
                            <?php echo isset($this->page)?$this->page:''; ?>
                        </ul>
                    </div>
                </div><!-- /.box -->
            </div>
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div>