<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 05/05/2015
 * Time: 09:45 SA
 */
?>
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Quản lý phân quyền
            <small>Danh sách phân quyền</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Quản lý phân quyền</a></li>
            <li class="active">Danh sách phân quyền</li>
        </ol>
    </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Danh sách phân quyền</h3>

                            <div class="box-tools">
                                <div class="input-group" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                                    <div class="input-group-btn">
                                        <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                            <?php echo $this->flash->message(); ?>
                            <?php if (!empty($this->data)): ?>
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Tên module</th>
                                    <th>Mã module</th>
                                    <th>Trạng thái</th>
                                    <th>ACTION</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($this->data as $item):
                                    $child = $this->getPermission($item->permission_id);
                                    ?>
                                    <tr data-id="<?php echo $item->permission_id; ?>">
                                        <td><?php echo $item->permission_id; ?></td>
                                        <td><strong><?php echo $item->title; ?></strong></td>
                                        <td><?php echo $item->module; ?></td>
                                        <td><?php if($item->status == 1): ?><span class="label label-success">Hiển thị</span><?php else: ?><span class="label label-danger">Không hiển thị</span><?php endif; ?></td>
                                        <td>
                                            <button type="button" class="btn btn-success btn-circle" onclick="javascript: window.open('/permission/edit/<?php echo $item->permission_id; ?>','_blank');"><i class="fa fa-pencil"></i></button>
                                            <button type="button" class="btn btn-danger btn-circle btnDelete"><i class="fa fa-times"></i></button>
                                        </td>
                                    </tr>
                                    <?php if(!empty($child)) foreach($child as $items): ?>
                                    <tr data-id="<?php echo $items->permission_id; ?>" >
                                        <td><?php echo $items->permission_id; ?></td>
                                        <td><i class="fa fa-long-arrow-right fa-w">&nbsp;</i><?php echo $items->title; ?></td>
                                        <td><?php echo $items->module; ?></td>
                                        <td><?php if($items->status == 1): ?><span class="label label-success">Hiển thị</span><?php else: ?><span class="label label-danger">Không hiển thị</span><?php endif; ?></td>
                                        <td>
                                            <button type="button" class="btn btn-success btn-circle" onclick="javascript: window.open('/permission/edit/<?php echo $items->permission_id; ?>','_blank');"><i class="fa fa-pencil"></i></button>
                                            <button type="button" class="btn btn-danger btn-circle btnDelete"><i class="fa fa-times"></i></button>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <?php endif; ?>
                        </div><!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <ul class="pagination pagination-sm no-margin center-block">
                                <?php echo isset($this->page)?$this->page:''; ?>
                            </ul>
                        </div>
                    </div><!-- /.box -->
                </div>
            </div>   <!-- /.row -->
        </section><!-- /.content -->
</div>