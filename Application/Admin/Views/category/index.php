<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 05/05/2015
 * Time: 09:45 SA
 */
?>
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Quản lý chuyên mục
            <small>Danh sách chuyên mục</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Quản lý chuyên mục</a></li>
            <li class="active">Danh sách chuyên mục</li>
        </ol>
    </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Danh sách chuyên mục</h3>

                            <div class="box-tools">
                                <div class="input-group" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                                    <div class="input-group-btn">
                                        <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                            <?php echo $this->flash->message(); ?>
                            <?php if (!empty($this->data)): ?>
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Tên danh mục</th>
                                    <th>Title SEO</th>
                                    <th>Danh mục cha</th>
                                    <th>Số danh mục con</th>
                                    <th class="text-center">ACTION</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($this->data as $item): ?>
                                    <tr data-id="<?php echo $item->category_id; ?>">
                                        <td><?php echo $item->category_id; ?></td>
                                        <td><a href="/category?parent_id=<?php echo $item->category_id; ?>" title="Xem danh mục con"> <?php echo $item->title; ?></a></td>
                                        <td><?php echo $item->title_page; ?></td>
                                        <td><?php echo $this->getCategory($item->parent_id,'title')?$this->getCategory($item->parent_id,'title')->title:'Trang chủ'; ?></td>
                                        <td><?php echo $this->countCategory(array('parent_id'=>$item->category_id)); ?></td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-primary" onclick="javascript: window.open('<?php echo $this->getUrlCate($item->category_id); ?>','_blank');"><i class="fa fa-link"></i></button>
                                            <button type="button" class="btn btn-success" onclick="javascript: window.open('/category/edit/<?php echo $item->category_id; ?>','_blank');"><i class="fa fa-edit"></i></button>
                                            <button type="button" class="btn btn-danger btnDelete"><i class="fa fa-times"></i></button>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <?php endif; ?>
                        </div><!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <ul class="pagination pagination-sm no-margin center-block">
                                <?php echo isset($this->page)?$this->page:''; ?>
                            </ul>
                        </div>
                    </div><!-- /.box -->
                </div>
            </div>   <!-- /.row -->
        </section><!-- /.content -->

</div>