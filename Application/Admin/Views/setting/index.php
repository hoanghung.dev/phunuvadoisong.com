<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 05/05/2015
 * Time: 09:45 SA
 */
$item = $this->data;
?>
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Cấu hình Website
            <small>Setting</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Setting</a></li>
            <li class="active">Cấu hình chung</li>
        </ol>
    </section>
<?php if (!empty($item)): ?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cài đặt hệ thống</h3>
                        <?php echo $this->flash->message();?>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form id="myForm" data-toggle="validator" role="form" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Tên Website</label>
                                <input type="text" name="title" class="form-control" placeholder="Tên website"
                                       required value="<?php echo $item->title; ?>">
                                <span class="help-block with-errors"></span>
                            </div>
                            <div class="form-group">
                                <label>Tiêu đề SEO</label>
                                <input type="text" name="title_page" class="form-control" data-maxlength="110"
                                       placeholder="Tiêu đề SEO" required value="<?php echo $item->title_page; ?>">
                                <span class="help-block with-errors"></span>
                            </div>
                            <div class="form-group">
                                <label>Mô tả SEO</label>
                                <textarea name="description" class="form-control" rows="3" required><?php echo $item->description; ?></textarea>
                                <span class="help-block with-errors"></span>
                            </div>

                            <div class="form-group">
                                <label>Từ khóa SEO</label>
                                <input type="text" name="keywords" class="form-control"
                                       placeholder="Keyword 1, keyword 2,keyword 3" value="<?php echo $item->keywords; ?>" required>
                                <span class="help-block with-errors"></span>
                            </div>
                            <div class="form-group">
                                <label>Tên công ty</label>
                                <input type="text" name="company" class="form-control" placeholder="Tên công ty" value="<?php echo $item->company; ?>" required>
                                <span class="help-block with-errors"></span>
                            </div>
                            <div class="form-group">
                                <label>Địa chỉ liên hệ</label>
                                <input type="text" name="address" class="form-control"
                                       placeholder="Địa chỉ liên hệ" value="<?php echo $item->address; ?>" required>
                                <span class="help-block with-errors"></span>
                            </div>

                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" name="email" class="form-control"
                                       placeholder="Email" value="<?php echo $item->email; ?>" required>
                                <span class="help-block with-errors"></span>
                            </div>
                            <div class="form-group">
                                <label>Điện thoại</label>
                                <input type="tel" name="tel" class="form-control"
                                       placeholder="Điện thoại liên hệ" value="<?php echo $item->tel; ?>" required>
                                <span class="help-block with-errors"></span>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input id="header" type="text" name="banner_header" value="<?php echo $item->banner_header; ?>" class="form-control" placeholder="Banner Top (728x90)">
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-primary" onclick="moxman.browse({fields: 'header', no_host: true});">Chọn ảnh</button>
                                        </div><!-- /btn-group -->
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input id="sidebar" type="text" name="banner_sidebar" value="<?php echo $item->banner_sidebar; ?>" class="form-control" placeholder="Banner Right (300x600)">
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-primary" onclick="moxman.browse({fields: 'sidebar', no_host: true});">Chọn ảnh</button>
                                        </div><!-- /btn-group -->
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Javascript Head</label>
                                <textarea name="script" class="form-control" rows="5"><?php echo $item->script; ?></textarea>
                                <span class="help-block with-errors"></span>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-default">Update</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        </div>
                    </form>
                </div><!-- /.box -->

            </div><!--/.col (left) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
<?php endif; ?>
</div>
