<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 05/05/2015
 * Time: 09:45 SA
 */
?>
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Quản lý thành viên
            <small>Danh sách thành viên</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Quản lý thành viên</a></li>
            <li class="active">Danh sách thành viên</li>
        </ol>
    </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Danh sách thành viên (<?php echo isset($this->total)?$this->total:0; ?>)</h3>

                            <div class="box-tools">
                                <div class="input-group" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                                    <div class="input-group-btn">
                                        <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                            <?php echo $this->flash->message(); ?>
                            <?php if (!empty($this->data)): ?>
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th class="text-center">User Name</th>
                                    <th class="text-center">Full Name</th>
                                    <th class="text-center">Nhóm</th>
                                    <th class="text-center">Email</th>
                                    <th class="text-center">Mobile</th>
                                    <th>Thời gian tạo</th>
                                    <th>Thời gian sửa</th>
                                    <th class="text-center">ACTION</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($this->data as $k=>$item): ?>
                                    <tr data-id="<?php echo $item->user_id; ?>">
                                        <td><?php echo $item->user_id; ?></td>
                                        <td><?php echo $item->username; ?></td>
                                        <td><?php echo $item->full_name; ?></td>
                                        <td class="text-center"><?php echo $item->group_id; ?></td>
                                        <td><?php echo $item->email; ?></td>
                                        <td><?php echo $item->tel; ?></td>
                                        <td>
                                            <?php echo $this->timeAgo($item->created_time,'d/m/Y H:i:s'); ?>
                                        </td>
                                        <td>
                                            <?php echo $this->timeAgo($item->updated_time,'d/m/Y H:i:s'); ?>
                                        </td>
                                        <td class="text-center">
                                            <?php if($item->user_id != 1): ?>
                                                <button type="button" class="btn btn-success" onclick="javascript: window.open('<?php echo $this->params['router']['controller'].'/edit/'.$item->user_id; ?>','_blank');"><i class="fa fa-edit"></i></button>
                                                <button type="button" class="btn btn-danger btnDelete"><i class="fa fa-times"></i></button>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <?php endif; ?>
                        </div><!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <ul class="pagination pagination-sm no-margin center-block">
                                <?php echo isset($this->page)?$this->page:''; ?>
                            </ul>
                        </div>
                    </div><!-- /.box -->
                </div>
            </div>   <!-- /.row -->
        </section><!-- /.content -->
</div>