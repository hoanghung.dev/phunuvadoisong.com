<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 05/05/2015
 * Time: 09:45 SA
 */
?>
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Quản lý thành viên
            <small>Thêm mới</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Quản lý thành viên</a></li>
            <li class="active">Thêm mới</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Thêm mới thành viên</h3>
                        <?php echo $this->flash->message();?>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form id="myForm" data-toggle="validator" role="form" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label>User Name</label>
                                <input name="username"  pattern="^([_A-z0-9]){3,}$" maxlength="20" data-error="Tối đa 20 ký tự bao gồm chữ số và chữ cái" class="form-control" placeholder="User Name" required>
                                <span class="help-block with-errors"></span>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label">Password</label>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <input type="password" name="password" data-toggle="validator" data-minlength="6" class="form-control" id="inputPassword" placeholder="Password" required="">
                                        <div class="help-block"></div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <input type="password" class="form-control" id="inputPasswordConfirm" data-match="#inputPassword" data-match-error="Mật khẩu không khớp" placeholder="Confirm" required="">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Full Name</label>
                                <input name="full_name" type="text" class="form-control" placeholder="Full Name" required>
                                <span class="help-block with-errors"></span>
                            </div>
                            <div class="form-group">
                                <label>Ngày sinh</label>
                                <input name="birthday" type="date" class="form-control" placeholder="Ngày sinh">
                                <span class="help-block with-errors"></span>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail" class="control-label">Email</label>
                                <input name="email" type="email" class="form-control" id="inputEmail" placeholder="Email" data-error="Định dạng email không đúng !" required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label>Số điện thoại</label>
                                <input name="tel" type="tel" class="form-control" placeholder="Số điện thoại">
                                <span class="help-block with-errors"></span>
                            </div>
                            <div class="form-group">
                                <label>Địa chỉ</label>
                                <input name="address" type="text" class="form-control" placeholder="Địa chỉ">
                                <span class="help-block with-errors"></span>
                            </div>
                            <div class="form-group">
                                <select name="group" class="form-control">
                                    <?php echo $this->GetUserGroup(); ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Trạng thái</label>
                                <input class="switch-check" type="checkbox" name="status" checked>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-default">Thêm mới</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        </div>
                    </form>
                </div><!-- /.box -->
            </div><!--/.col (left) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div>