<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 26/04/2015
 * Time: 10:36 CH
 */
namespace Application\Admin\Helpers;


class GetImage{
    public function getImage($url = null){
        if($url == null) return _ROOT_UPLOAD.'/noimage.gif';
        return _ROOT_UPLOAD.$url;
    }
}