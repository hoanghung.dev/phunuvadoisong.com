<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 26/04/2015
 * Time: 10:36 CH
 */
namespace Application\Admin\Helpers;
use Application\Admin\Models\News;

class GetImageNews{
    public function getImageNews($id,$width = 0, $height = 0){
        if(is_numeric($id)){
            $newsModel = new News();
            $data = $newsModel->getOne('news_id = ?',array($id),'image');
            $image = !empty($data->image)?$data->image:'no-image.png';
        }else $image = $id;
        if($width != 0 && $height != 0){
            $size = sprintf('-%dx%d', $width, $height);
            $part = explode('.', $image);
            $ext = '.'.end($part);
            $newlink = str_replace($ext,$size.$ext, $image);
            //$newlink = str_replace('/Frontend/upload/','/',$newlink);
            //$newlink = str_replace(_ROOT_HOME,'/',$newlink);
            $newlink = str_replace('/upload/','/',$newlink);
            return _ROOT_UPLOAD.'/cache/'.$newlink;
        }else return _ROOT_UPLOAD.'/'.$image;
    }
}