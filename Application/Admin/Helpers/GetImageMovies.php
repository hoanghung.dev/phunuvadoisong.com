<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Admin\Helpers;
use Application\Admin\Controllers\Base;
use Application\Admin\Models\Movies;

class GetImageMovies extends Base{
    public function getImageMovies($id,$width = 100, $height = 100, $position = 'in'){
        $moviesModel = new Movies();
        $data = $moviesModel->getOne('movies_id = ?',array($id),'image_in,image_out');
        $image = 'image_in';
        $image = $data->$image;

        if($width != 0 && $width != 0){
            $imageArr = explode('.',$image);
            $nameImage = $imageArr[0];
            $typeImage = end($imageArr);
            $imageOutput = $nameImage.'-'.$width.'x'.$height.'.'.$typeImage;
            if(is_file(DIR_FOLDER_IMAGES.$imageOutput)) return _ROOT_IMAGES.'/'.$imageOutput;
            else{
                $imageOutput = $this->cropImage($image,$width,$height);
                return _ROOT_IMAGES.'/'.$imageOutput;
            }
        }else return _ROOT_IMAGES.'/'.$image;
    }
}