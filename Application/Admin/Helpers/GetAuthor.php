<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 04/05/2015
 * Time: 02:50 CH
 */

namespace Application\Admin\Helpers;
use Application\Admin\Models\Users;

class GetAuthor{
    public function getAuthor($id){
        if($id == 0) print 'Crawler';
        else{
            $userModel = new Users();
            $data = $userModel->getOne('user_id = ?',array($id),'user_name,full_name');
            if(!empty($data)) print $data->user_name;
        }
    }
}