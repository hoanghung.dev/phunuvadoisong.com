<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Admin\Helpers;
use Application\Admin\Controllers\Base;
use Application\Admin\Models\Movies;

class GetUrlVideo extends Base{
    public function getUrlVideo($id){
        $file = DIR_FOLDER.'/tmp/' . $id . '.json';
        $timestr = time();
        $str = "mobi_portal" . $id . $timestr . 'Mobi@Portal!@#';
        $hash = md5($str);
        $link = "http://vapi.kenh1.vn/video/export/video.json?username=mobi_portal&id=$id&timestamp=$timestr&hash=$hash";
        if (!file_exists($file) || filemtime($file) < time() - 180) {
            $data = $this->cUrl($link);
            file_put_contents($file, $data);
        } else {
            $data = file_get_contents($file);
        }
        return json_decode($data);
    }
}