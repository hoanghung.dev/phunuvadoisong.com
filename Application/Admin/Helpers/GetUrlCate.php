<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Admin\Helpers;
use Application\Admin\Controllers\Base;
use Application\Admin\Models\Categories;

class GetUrlCate extends Base{
    public function getUrlCate($id){
        $cateModel = new Categories();
        $data = $cateModel->getOne('category_id = :id',array(':id'=>$id),'slug');
        $slug = $data->slug;
        if($slug == null){
            $slug = $this->toSlug($data->title);
            $cateModel->update(array('slug'=>$slug),'category_id = :id',array(':id'=>$id));
        }
        return _ROOT_HOME.'/the-loai/'.$slug.'-'.$id;
    }
}