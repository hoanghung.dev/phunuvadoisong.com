<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 06/05/2015
 * Time: 09:31 SA
 */
namespace Application\Admin\Helpers;
use Application\Admin\Models\News;
use Application\Mobile\Controllers\Base;

class GetNews extends Base{
    public function getNews($id){
        $newsModel = new News();
        $data = $newsModel->getOne('news_id = :id',array(':id' => $id));
        return $data;
    }

}
