<?php
/**
 * Created by PhpStorm.
 * User: Steven
 * Date: 15/03/2014
 * Time: 10:53
 */
namespace Application\Admin\Models;

use Soul\Mvc\Model;
use Soul\Registry;

class UsersGroup extends Model
{
    protected  $_tbl ='cms_group';

    public function init()
    {
        $this->_mysql = Registry::get('Mysql');
    }

    public function getAll()
    {
        return $this->_mysql->select($this->_tbl());
    }

    public function getOne($where, $bind,$select='*')
    {
        $sql = sprintf('SELECT %s FROM %s WHERE %s LIMIT 1',$select,$this->_tbl, $where);
        $st = $this->_mysql->prepare($sql);
        $st->execute($bind);
        return $st->fetch(\PDO::FETCH_OBJ);
    }
    public function getCount($args = null)
    {
        $status = '';
        $not_in = '';
        $in = '';
        $search = '';
        $bind = array();


        $default = array('status'=> null,'not_in' => 0, 'in' => 0,'search'=> null);
        $args = $this->parseArgs($args, $default);
        extract($args);

        $where = 'WHERE 1=1';

        if ($status != null){
            $where .= ' AND status = :status';
            $bind[] = array(
                'element'=>':status',
                'value'=>$status,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if ($in != 0){
            $where .= ' AND group_id IN (:group_id)';
            $bind[] = array(
                'element'=>':group_id',
                'value'=>$in,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if ($not_in != 0){
            $where .= ' AND group_id NOT IN (:group_id)';
            $bind[] = array(
                'element'=>':group_id',
                'value'=>$not_in,
                //'type'=>'\PDO::PARAM_INT'
            );
        }


        if($search != null){
            //$search = rawurldecode($search);
            $where .= ' AND (MATCH(title) AGAINST (:search))';
            $bind[] = array(
                'element'=>':search',
                'value'=>$search,
                //'type'=>'\PDO::PARAM_STR'
            );
        }

        $sql = sprintf('SELECT COUNT(1) FROM %s %s', $this->_tbl, $where);
        $st = $this->_mysql->prepare($sql);
        if(is_array($bind)) foreach($bind as $item){
            $st->bindParam($item['element'],$item['value'],isset($item['type'])?$item['type']:null,10);
        }
        $st->execute();
        $data = $st->fetchColumn();
        return $data;
    }
    public function getDataArr($args = null)
    {
        $select = '';
        $status = '';
        $not_in = '';
        $in = '';
        $order_by = '';
        $order = '';
        $limit = '';
        $page = '';
        $search = '';
        $bind = array();

        $default = array('select'=>'*','status'=>null,'not_in' => 0, 'in' => 0,'search'=> null, 'order_by' => null,'limit' => 0, 'page'=>1);
        $args = $this->parseArgs($args, $default);
        extract($args);

        $where = 'WHERE 1=1';

        if ($status != null){
            $where .= ' AND status = :status';
            $bind[] = array(
                'element'=>':status',
                'value'=>$status,
                //'type'=>'\PDO::PARAM_INT'
            );
        }


        if ($in != 0){
            $where .= ' AND group_id IN (:group_id)';
            $bind[] = array(
                'element'=>':group_id',
                'value'=>$in,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if ($not_in != 0){
            $where .= ' AND group_id NOT IN (:group_id)';
            $bind[] = array(
                'element'=>':group_id',
                'value'=>$not_in,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if($search != null){
            //$search = rawurldecode($search);
            $where .= ' AND (MATCH(title) AGAINST (:search))';
            $bind[] = array(
                'element'=>':search',
                'value'=>$search,
                //'type'=>'\PDO::PARAM_STR'
            );
        }

        if($order_by != null)
            $order = sprintf('ORDER BY %s', $order_by);

        if($limit != 0){
            $page = intval($page);
            $offset = ($page-1)*$limit;
            $limit = sprintf('LIMIT %d,%d',$offset,$limit);
        }else $limit = 'LIMIT 0,10';
        $sql = sprintf('SELECT %s FROM %s %s %s %s',$select, $this->_tbl, $where, $order, $limit);
        //echo $sql;
        $st = $this->_mysql->prepare($sql);
        if(is_array($bind)) foreach($bind as $item){
            $st->bindParam($item['element'],$item['value'],isset($item['type'])?$item['type']:null,10);
        }
        $st->execute();
        $data = $st->fetchAll(\PDO::FETCH_OBJ);
        return $data;

    }
    public function insert($arr){
        return $this->_mysql->insert($this->_tbl, $arr);
    }
    public function update($arr, $where, $bind){
        return $this->_mysql->update($this->_tbl, $arr, $where, $bind);
    }
    public function delete( $where, $bind){
        return $this->_mysql->delete($this->_tbl, $where, $bind);
    }
}