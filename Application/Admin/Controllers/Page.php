<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 06:54 CH
 */
namespace Application\Admin\Controllers;


use Application\Admin\Models\Pages;

class Page extends Base
{

    public function index(){
        $pageModel = new Pages();
        $param['select'] = '*';
        $param['type'] = 3;
        $this->view->data = $data = $pageModel->getDataArr($param);
        if(empty($data)) $this->_flash->warning('Không có phản hồi nào !');
        $this->view->countAll = $pageModel->getCount($param);
        $this->displayLayout();
    }
    public function add(){
        $pageModel = new Pages();
        if($this->_request->isPost()){
            $data = array();
            foreach($this->_request->getPosts() as $field=>$value){
                $data[$field] = $value;
            }
            $data['type'] = 3;
            $slug = $this->toSlug($data['title']);
            if(!empty($_FILES['image']['name'])){
                $data['image'] = $this->uploadImage($_FILES['image'],$slug,'setting');
            }
            $data['created_time'] = date(DATE_TIME_FORMAT);
            $data['updated_time'] = date(DATE_TIME_FORMAT);
            if($this->_request->getPost('status')) $data['status'] = true; else $data['status'] = false;
            if($pageModel->insert($data) == true) $this->_flash->success("Thêm mới phản hồi thành công !");
            else $this->_flash->danger("Thêm mới phản hồi không thành công !");
            unset($data);

        }
        $this->displayLayout('default',$this->render());
    }
    public function edit(){
        $pageModel = new Pages();
        $id = $this->_request->getParam('id');
        if($this->_request->isPost()){
            $data = array();
            foreach($this->_request->getPosts() as $field=>$value){
                $data[$field] = $value;
            }
            $data['type'] = 3;
            $slug = $this->toSlug($data['title']);
            if(!empty($_FILES['image']['name'])){
                $data['image'] = $this->uploadImage($_FILES['image'],$slug,'setting');
            }
            $data['updated_time'] = date(DATE_TIME_FORMAT);
            if($this->_request->getPost('status')) $data['status'] = true; else $data['status'] = false;
            if(is_array($data)){
                if($pageModel->update($data,'page_id = :cateId',array(':cateId'=>$id)) == true) $this->_flash->success("Cập nhật phản hồi thành công !");
                else $this->_flash->danger("Cập nhật phản hồi không thành công !");
                unset($data);
            }
        }
        $this->view->data = $pageModel->getOne('page_id = :id',array(':id'=>$id));
        $this->displayLayout('default',$this->render());

    }

    public function actDelete(){
        $pageModel = new Pages();
        $id = $this->_request->getPost('id');
        $data = $pageModel->delete('page_id = :id',array(':id'=>$id));
        print $data;
        exit;
    }
}