<?php
namespace Application\Admin\Controllers;
use Application\Admin\Models\Permissions;
use Soul\Application;
use Soul\Mvc\Controller;
use Soul\Registry;
use Soul\Session;
use Soul\Translate;
use Application\Admin\Helpers;

class Base extends Controller
{

    //public $DOM;
    /**
     * @var Soul/Helpers/Flash $_flash
     */
    protected $_flash;

    protected $_params;

    protected $_session;

    public function init()
    {
        parent::init();
        $this->_translate = $this->_getTranslation();
        $this->_session = Session::getInstance();
        $this->initView()->setTranslate($this->_translate)->setSession($this->_session);
        $this->view->_session = $this->_session;
        $this->view->flash = $this->_flash = $this->_helper->flash();
        $this->view->params = $this->_params = $this->_request->getParams();
        if ($this->checkLogin() == false && $this->_params['router']['action'] != 'login' && $this->_params['router']['controller'] != 'user') {
            $this->redirect('/login'); //ok
        }
    }

    public function redirect($url)
    {
        header('Location:' . $url);
        exit;
    }

    public function checkLogin()
    {
        if ($this->_session->auth) return true;
        else return false;
    }

    public function accessDenied($action=null){
        if(!empty($action)){
            $groupId = $this->_session->auth->group_id;
            if($this->_helper->accessPermission($groupId,$action) == false){
                $this->displayLayout('accessdenied');
                exit;
            }
        }
    }
    /**
     * @return Translate
     */
    protected function _getTranslation()
    {
        $translate = new Translate(array(
            'locale' => 'vi_VN',
            'file' => 'messages',
            'directory' => __DIR__ . '/../Language'
        ));

        return $translate;
    }

    /**
     * @param string $layout
     * @param null $layoutContent
     */
    public function displayLayout($layout = 'default', $layoutContent = null)
    {
        $content = $this->renderLayout($layout, $layoutContent);
        $this->display($content);
    }

    public function renderLayout($layout = 'default', $layoutContent = null)
    {
        if (null === $layoutContent) {
            $layoutContent = $this->render();
        }
        $this->view->layoutContent = $layoutContent;
        $view = $this->getLayoutScript($layout);
        return $this->view->render($view);
    }

    /**
     * @param null $layout
     * @return string
     */
    public function getLayoutScript($layout = null)
    {
        $script = '_layouts' . DIRECTORY_SEPARATOR . $layout . '.php';
        return $script;
    }

    public function forward($url)
    {
        $u = explode('/', $url);
        // $this->_helper->action($url[0], );
    }

    public function varDump($array)
    {
        echo "<pre>";
        var_dump($array);
        echo "</pre>";
    }

    function getPaging($totalCount,$activePage = 1,$limit = 10,$maxPage = 5,$url = null){
        $page = $this->_helper->getHelper('Paging');
        $page->setNumItem($totalCount);
        $page->setCurrentPage($activePage);
        $page->setMaxPage($maxPage);
        $page->setNumItemOnPage($limit);
        $page->setUrl($url);
        $page->setUrlType('?page=');
        return $page->toString();
    }
    public function cUrl($url, array $post_data = array(), $delete = false, $verbose = false, $ref_url = false, $cookie_location = false, $return_transfer = true)
    {
        $return_val = false;
        $pointer = curl_init();

        curl_setopt($pointer, CURLOPT_URL, $url);
        curl_setopt($pointer, CURLOPT_TIMEOUT, 40);
        curl_setopt($pointer, CURLOPT_RETURNTRANSFER, $return_transfer);
        curl_setopt($pointer, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.28 Safari/534.10");
        curl_setopt($pointer, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($pointer, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($pointer, CURLOPT_HEADER, false);
        curl_setopt($pointer, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($pointer, CURLOPT_AUTOREFERER, true);

        if ($cookie_location !== false) {
            curl_setopt($pointer, CURLOPT_COOKIEJAR, $cookie_location);
            curl_setopt($pointer, CURLOPT_COOKIEFILE, $cookie_location);
            curl_setopt($pointer, CURLOPT_COOKIE, session_name() . '=' . session_id());
        }

        if ($verbose !== false) {
            $verbose_pointer = fopen($verbose, 'w');
            curl_setopt($pointer, CURLOPT_VERBOSE, true);
            curl_setopt($pointer, CURLOPT_STDERR, $verbose_pointer);
        }

        if ($ref_url !== false) {
            curl_setopt($pointer, CURLOPT_REFERER, $ref_url);
        }

        if (count($post_data) > 0) {
            curl_setopt($pointer, CURLOPT_POST, true);
            curl_setopt($pointer, CURLOPT_POSTFIELDS, $post_data);
        }
        if ($delete !== false) {
            curl_setopt($pointer, CURLOPT_CUSTOMREQUEST, "DELETE");
        }

        $return_val = curl_exec($pointer);

        $http_code = curl_getinfo($pointer, CURLINFO_HTTP_CODE);

        if ($http_code == 404) {
            return false;
        }

        curl_close($pointer);

        unset($pointer);

        return $return_val;
    }

    public function getResult($keyword, $page = '1')
    {
        if (substr_count($keyword, ' ') > 0)
            $keywords = str_replace(' ', '+', $keyword);
        else
            $keywords = $keyword;

        if ($page == 0)
            $url = "https://www.google.com.vn/search?q=" . $keywords;
        else {
            $index = ($page * 10);
            $url = "https://www.google.com.vn/search?q=" . $keywords . "&start=" . $index;
        }
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_REFERER, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $str = curl_exec($curl);
        curl_close($curl);

        $html = $this->DOM->load($str);

        if ($html->find('#ires', 0)) {
            $i = 0;
            foreach ($html->find('#ires li') as $article) {
                if (isset($article->find('h3 a', 0)->href)) {
                    $link = explode("?q=", $article->find('h3 a', 0)->href);
                    $link1 = explode("&", $link[1]);
                    $data[$i]['link'] = $link1[0];
                }
                if (isset($article->find('h3 a', 0)->plaintext)) $data[$i]['title'] = utf8_encode($article->find('h3 a', 0)->plaintext);
                if (isset($article->find('span.st', 0)->innertext)) $data[$i]['description'] = utf8_encode($article->find('span.st', 0)->innertext);
                $i++;
            }
            return $data;
        }
    }

    public function getImage($keyword)
    {
        if (substr_count($keyword, ' ') > 0)
            $keywords = str_replace(' ', '+', $keyword);
        else $keywords = $keyword;

        $url = "https://www.google.com.vn/search?q=" . $keywords . "&tbm=isch";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_REFERER, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $str = curl_exec($curl);
        curl_close($curl);

        $html = $this->DOM->load($str);
        if ($html->find('#search', 0)) {
            foreach ($html->find('#search td') as $article) {
                return $article->find('img', 0)->src;
            }
        }
    }
    function uploadImage($file,$filename= null,$folder=null){
        $rootDir = DIR_UPLOAD;
        if ((($file["type"] == "image/gif") || ($file["type"] == "image/jpeg") || ($file["type"] == "image/png") || ($file["type"] == "image/jpeg")) && ($file["size"] < 1000000)) {
            if ($file["error"] > 0) return false;
            else {
                if($filename==null) {
                    $filename=$file["name"]; $filename=preg_replace("/(\=|\+|\&)/", '-', $filename);
                } else {
                    $filename = $filename.'.'.pathinfo($file["name"], PATHINFO_EXTENSION);;
                }
                $path = '';
                if($folder != null) $path .= $folder.'/';
                $path .= date('Ym').'/';
                if (!is_dir($rootDir.'/'.$path)) {
                    mkdir($rootDir.'/'.$path, 0777, true);
                }
                $link = $path.$filename;
                move_uploaded_file($file["tmp_name"], $rootDir.'/'.$link);
                return $link;
            }
        }
        else return false;
    }
    function uploadVideo($file){
        $target_dir = DIR_FOLDER_VIDEO;
        $path = DIR_FOLDER_VIDEO_ARRAY.'/images/'.date('Ymd').'/';
        $target_file = $target_dir.$path.md5(basename($file["name"]));
        $uploadOk = 1;

        if (file_exists($target_file)) {
            print "Sorry, file already exists.";
            $uploadOk = 0;
        }
        if ($uploadOk == 0) {
            print "Sorry, your file was not uploaded.";
        } else {
            if (move_uploaded_file($file["tmp_name"], $target_file.$path)) {
                print "The file ". basename($file["name"]). " has been uploaded.";
                return $path.md5(basename($file["name"]));
            } else {
                print "Sorry, there was an error uploading your file.";
            }
        }
    }
    function uploadImageURL($url, $filename = '')
    {
        $url = trim($url);
        //print_r($url);die('ok');
        if ($url) {
            $file = fopen($url, "rb");
            if ($file) {
                $directory = DIR_FOLDER_IMAGES; // Directory to upload files to.
                $valid_exts = array("jpg", "jpeg", "gif", "png"); // default image only extensions
                $ext = end(explode(".", strtolower(basename($url))));
                if (in_array($ext, $valid_exts)) {
                    $rand = rand(1000, 9999);
                    if($filename=='') {
                        $filename=$file["name"]; $filename=preg_replace("/(\=|\+|\&)/", '-', $filename);
                    } else {
                        $filename = $filename.'-'.rand(1000,9999).'.jpg';
                    }
                    $newfile = fopen($directory . $filename, "wb"); // creating new file on local server
                    if ($newfile) {
                        while (!feof($file)) {
                            // Write the url file to the directory.
                            fwrite($newfile, fread($file, 1024 * 8), 1024 * 8); // write the file to the new directory at a rate of 8kb/sec. until we reach the end.
                        }
                        return $filename;
                    } else {
                        return 'Error Upload Image: Could not establish new file (' . $directory . $filename . ') on local server. Be sure to CHMOD your directory to 777.';
                    }
                } else {
                    return 'Error Upload Image: Invalid file type. Please try another file.';
                }
            } else {
                return 'Error Upload Image: Could not locate the file: ' . $url . '';
            }
        } else {
            return 'Error Upload Image: Invalid URL entered. Please try again.';
        }
    }
    function wgetUploadVideoURL($url) {
        $directory = DIR_FOLDER_VIDEO; // Directory to upload files to.
        $path = DIR_FOLDER_VIDEO_ARRAY.'/videos/'.date('Ymd').'/';
        $code = 'wget --output-document='.$directory.$path.md5(date('YmdHis')).'.mp4 '."'$url'";
        shell_exec($code);
        return $path.md5(date('YmdHis')).'.mp4';
    }
    function uploadVideoURL($url)
    {
        $url = trim($url);
        //print_r($url);die('ok');
        if ($url) {
            $file = fopen($url, "rb");
            if ($file) {
                $directory = DIR_FOLDER_VIDEO; // Directory to upload files to.
                $path = DIR_FOLDER_VIDEO_ARRAY.'/images/'.date('Ymd').'/';
                $newfile = fopen($directory . $path, "wb"); // creating new file on local server
                if ($newfile) {
                    while (!feof($file)) {
                        // Write the url file to the directory.
                        fwrite($newfile, fread($file, 1024 * 8), 1024 * 8); // write the file to the new directory at a rate of 8kb/sec. until we reach the end.
                    }
                    return $path.md5($file["name"]);
                } else {
                    return 'Error Upload Image: Could not establish new file (' . $directory . $filename . ') on local server. Be sure to CHMOD your directory to 777.';
                }
            } else {
                return 'Error Upload Image: Could not locate the file: ' . $url . '';
            }
        } else {
            return 'Error Upload Image: Invalid URL entered. Please try again.';
        }
    }
    function toSlug($doc)
    {
        $str = addslashes(html_entity_decode($doc));
        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
        $str = preg_replace("/(đ)/", 'd', $str);
        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
        $str = preg_replace("/(Đ)/", 'D', $str);
        $str = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $str);
        $str = preg_replace("/( )/", '-', $str);
        $str = preg_replace("/(--)/", '-', $str);
        $str = preg_replace("/(--)/", '-', $str);
        $str = str_replace("/", "-", $str);
        $str = str_replace("\\", "-", $str);
        $str = str_replace("+", "", $str);
        $str = str_replace("%", "", $str);
        $str = strtolower($str);
        $str = stripslashes($str);
        return $str;
    }
    function getKeyYoutube($url)
    {
        $pattern = '#^(?:https?://)?';    # Optional URL scheme. Either http or https.
        $pattern .= '(?:www\.)?';         #  Optional www subdomain.
        $pattern .= '(?:';                #  Group host alternatives:
        $pattern .=   'youtu\.be/';       #    Either youtu.be,
        $pattern .=   '|youtube\.com';    #    or youtube.com
        $pattern .=   '(?:';              #    Group path alternatives:
        $pattern .=     '/embed/';        #      Either /embed/,
        $pattern .=     '|/v/';           #      or /v/,
        $pattern .=     '|/watch\?v=';    #      or /watch?v=,
        $pattern .=     '|/watch\?.+&v='; #      or /watch?other_param&v=
        $pattern .=   ')';                #    End path alternatives.
        $pattern .= ')';                  #  End host alternatives.
        $pattern .= '([\w-]{11})';        # 11 characters (Length of Youtube video ids).
        $pattern .= '(?:.+)?$#x';         # Optional other ending URL parameters.
        preg_match($pattern, $url, $matches);
        return (isset($matches[1])) ? $matches[1] : false;
    }
    function getDataYoutube($video_id)
    {
        define('YT_API_URL', 'http://gdata.youtube.com/feeds/api/videos?q=');

        //Using cURL php extension to make the request to youtube API
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, YT_API_URL . $video_id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //$feed holds a rss feed xml returned by youtube API
        $feed = curl_exec($ch);
        curl_close($ch);


        echo $feed;
        //Using SimpleXML to parse youtube's feed
        $xml = simplexml_load_string($feed);

        $entry = $xml->entry[0];

        //If no entry whas found, then youtube didn't find any video with specified id
        if (!$entry) echo ''; else {
            //exit('Error: no video with id "' . $video_id . '" whas found. Please specify the id of a existing video.');
            $media = $entry->children('media', true);
            $group = $media->group;


            $title = $group->title;//$title: The video title
            $desc = $group->description;//$desc: The video description
            $vid_keywords = $group->keywords;//$vid_keywords: The video keywords
            $thumb = $group->thumbnail[0];//There are 4 thumbnails, the first one (index 0) is the largest.
            //$thumb_url: the url of the thumbnail. $thumb_width: thumbnail width in pixels.
            //$thumb_height: thumbnail height in pixels. $thumb_time: thumbnail time in the video
            list($thumb_url, $thumb_width, $thumb_height, $thumb_time) = $thumb->attributes();
            $content_attributes = $group->content->attributes();
            //$vid_duration: the duration of the video in seconds. Ex.: 192.
            $vid_duration = $content_attributes['duration'];
            //$duration_formatted: the duration of the video formatted in "mm:ss". Ex.:01:54
            $duration_formatted = str_pad(floor($vid_duration / 60), 2, '0', STR_PAD_LEFT) . ':' . str_pad($vid_duration % 60, 2, '0', STR_PAD_LEFT);
            $author = $entry->author->name;

            //echoing the variables for testing purposes:
            $video['id'] = $video_id;
            $video['title'] = $title;
            $video['desc'] = $desc;
            $video['keywords'] = $vid_keywords;
            $video['thumb_url'] = $thumb_url;
            $video['thumb_width'] = $thumb_width;
            $video['thumb_height'] = $thumb_height;
            $video['thumb_time'] = $thumb_time;
            $video['duration'] = $vid_duration;
            //$video['duration_formatted']=$duration_formatted;
            $video['channel'] = $author;

            return $video;
        }
    }

    function downloadYoutube($ytlink){
        if (isset($ytlink)) {
            if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $ytlink, $match)) {
                $id = $match[1];
            }
            $videoid = $id;
            $ydata = getUrls(urldecode(urldecode(get_content('http://www.youtube.com/get_video_info?video_id=' . $videoid . '&asv=3&el=detailpage&hl=en_US'))));
            /*echo "<pre>";
            print_r($ydata);
            echo "</pre>";*/
            /*gets 720p video. pass the relevent itag instead of '22' in the gettag() method. below are several itags
            5:  Result := 'Low Quality, 240p, FLV, 400x240';
            17: Result := 'Low Quality, 144p, 3GP, 0x0';
            18: Result := 'Medium Quality, 360p, MP4, 480x360';
            22: Result := 'High Quality, 720p, MP4, 1280x720';
            34: Result := 'Medium Quality, 360p, FLV, 640x360';
            35: Result := 'Standard Definition, 480p, FLV, 854x480';
            36: Result := 'Low Quality, 240p, 3GP, 0x0';
            37: Result := 'Full High Quality, 1080p, MP4, 1920x1080';
            38: Result := 'Original Definition, MP4, 4096x3072';
            43: Result := 'Medium Quality, 360p, WebM, 640x360';
            44: Result := 'Standard Definition, 480p, WebM, 854x480';
            45: Result := 'High Quality, 720p, WebM, 1280x720';
            46: Result := 'Full High Quality, 1080p, WebM, 1280x720';
            82: Result := 'Medium Quality 3D, 360p, MP4, 640x360';
            84: Result := 'High Quality 3D, 720p, MP4, 1280x720';
            100: Result := 'Medium Quality 3D, 360p, WebM, 640x360';
            102: Result := 'High Quality 3D, 720p, WebM, 1280x720';
            */
            $urlvideo = gettag($ydata, '18', $videoid);

            return download($urlvideo, $videoid);
        }
    }


    function download($videoId){
        $root = DIR_FOLDER_VIDEO;
        $path = DIR_FOLDER_VIDEO_ARRAY.'/videos/'.date('Ymd').'/';
        if (!is_dir($root.$path)) {
            mkdir($root.$path, 0777, true);
        }
        $youtubeDownload = new \YouTubeVideoDownloader($videoId,"18",$root,$path);
        //$data = $youtubeDownload->curlDownload();
        $data = $youtubeDownload->wgetDownload();
        return $data;

    }

    function get_content($url){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        ob_start();

        curl_exec($ch);
        curl_close($ch);
        $string = ob_get_contents();

        ob_end_clean();

        return $string;

    }

    function getUrls($string){
        $regex = '/http?\:\/\/[^\" ]+/i';
        preg_match_all($regex, $string, $matches);
        //return (array_reverse($matches[0]));
        return ($matches[0]);
    }


    function getvid($arr, $itag){
        $size = count($arr);
        $url = "";
        for ($i = 0; $i < $size; $i++) {
            $string = $arr[$i];
            if (strpos($string, $itag) !== false) {
                $url = $string;
            }
        }
        return $url;
    }

    function gettag($ydata, $itag, $videoid){
        $tag = "itag=" . $itag;
        $url = getvid($ydata, $tag);
        $vidlink = (explode(",", $url));
        $url2 = $vidlink[0];
        return getTitled($url2, $videoid);
    }

    function getTitled($urlvid, $videoid){
        $url = 'http://gdata.youtube.com/feeds/api/videos/' . $videoid;
        $content = file_get_contents($url);
        $first_step = explode("<title type='text'>", $content);
        $second_step = explode("</title>", $first_step[1]);
        $tit = str_replace(" ", "+", $second_step[0]);
        return $urlvid . "&title=" . $tit;
    }
    public function cropImage($image, $width, $height) {
        $path = DIR_FOLDER_IMAGES;
        if(is_file($path.$image)){
            $im = new \Imagick($path.$image);
            $im->cropThumbnailImage( $width, $height);

            $imageArr = explode('.',$image);
            $nameImage = $imageArr[0];
            $typeImage = end($imageArr);

            $im->writeImage($path.$nameImage.'-'.$width.'x'.$height.'.'.$typeImage);
            return $nameImage.'-'.$width.'x'.$height.'.'.$typeImage;
        }else return false;

    }



}