<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 06:54 CH
 */
namespace Application\Admin\Controllers;


use Application\Admin\Models\Categories;

class Category extends Base
{

    public function index(){
        $categoryModel = new Categories();
        $param['select'] = '*';
        $param['parent_id'] = $this->_request->getParam('parent_id');
        $param['limit'] = 30;
        $this->view->data = $data = $categoryModel->getDataArr($param);
        if(empty($data)) $this->_flash->warning('Không có chuyên mục nào !');
        $this->view->countAll = $categoryModel->getCount($param);
        $this->displayLayout('default', $this->render());
    }
    public function add(){
        $categoryModel = new Categories();
        if($this->_request->isPost()){
            $data = array();
            foreach($this->_request->getPosts() as $field=>$value){
                $data[$field] = $value;
            }
            $data['slug'] = $this->toSlug($data['title']);
            $data['created_time'] = date(DATE_TIME_FORMAT);
            $data['updated_time'] = date(DATE_TIME_FORMAT);
            if($categoryModel->getOne('slug = :slug',array(':slug'=>$data['slug']))){
                $this->_flash->danger("Danh mục đã tồn tại !");
            }else{
                if($categoryModel->insert($data) == 1) $this->_flash->success("Thêm mới danh mục thành công !");
                else $this->_flash->danger("Thêm mới danh mục không thành công !");
                unset($data);
            }

        }
        $this->displayLayout('default',$this->render());
    }
    public function edit(){
        $categoryModel = new Categories();
        $id = $this->_request->getParam('id');
        if($this->_request->isPost()){
            $data = array();
            foreach($this->_request->getPosts() as $field=>$value){
                $data[$field] = $value;
            }
            $data['updated_time'] = date(DATE_TIME_FORMAT);
            if($this->_request->getPost('status')) $data['status'] = 1; else $data['status'] = 0;
            if(is_array($data)){
                if($categoryModel->update($data,'category_id = :cateId',array(':cateId'=>$id)) == 1) $this->_flash->success("Cập nhật danh mục thành công !");
                else $this->_flash->danger("Cập nhật danh mục không thành công !");
                unset($data);
            }
        }
        $this->view->data = $categoryModel->getOne('category_id = :id',array(':id'=>$id));
        $this->displayLayout('default',$this->render());

    }

    public function actDelete(){
        $categoryModel = new Categories();
        $id = $this->_request->getPost('id');
        $data = $categoryModel->delete('category_id = :id',array(':id'=>$id));
        print $data;
        exit;
    }
}