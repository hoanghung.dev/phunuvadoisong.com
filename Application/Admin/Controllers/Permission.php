<?php
namespace Application\Admin\Controllers;


use Application\Admin\Models\Permissions;
use Application\Admin\Models\UsersGroup;

class Permission extends Base
{
    public function index(){
        $this->accessDenied('permission');
        $permissionModel = new Permissions();
        $params['parent_id'] = 0;
        $params['order_by'] = 'order_number ASC';
        $data = $permissionModel->getDataArr($params);
        if(empty($data)) $this->_flash->warning('Không có dữ liệu !');
        $this->view->data = $data;
        $this->displayLayout();
    }
    public function permissionGroup(){
        $this->accessDenied('permission_group');
        $groupUserModel = new UsersGroup();
        $permissionModel = new Permissions();
        $params['parent_id'] = '0';
        $params['order_by'] = 'order_number ASC';
        $this->view->listPermission = $permissionModel->getDataArr($params);
        $this->view->listGroup = $groupUserModel->getDataArr();
        $this->displayLayout('default',$this->render());
    }
    public function add(){
        $this->accessDenied('permission_add');
        $permissionModel = new Permissions();
        if($this->_request->isPost()){
            $data['title'] = $this->_request->getPost('title');
            $data['module'] = $this->_request->getPost('module_key');
            $data['parent_id'] = $this->_request->getPost('parent_id');
            if($this->_request->getPost('status') == 'on')
                $data['status'] = 1;//Trạng thái mở
            else  $data['status'] = 0; // Trạng thái khóa
            if($permissionModel->getOne('module = :module',array(':module'=>$data['module']))){
                $this->_flash->danger("Module đã tồn tại !");
            }else{
                if(is_array($data)){
                    if($permissionModel->insert($data) == true) $this->_flash->success("Thêm mới module thành công !");
                    else $this->_flash->danger("Thêm mới module không thành công !");
                    unset($data);

                }
            }

        }
        $this->displayLayout('default',$this->render());
    }
    public function edit(){
        $this->accessDenied('permission_edit');
        $permissionModel = new Permissions();
        $id = $this->_request->getParam('id');
        if($this->_request->isPost()){
            $data['title'] = $this->_request->getPost('title');
            $data['module'] = $this->_request->getPost('module_key');
            $data['parent_id'] = $this->_request->getPost('parent_id');
            if($this->_request->getPost('status') == 'on')
                $data['status'] = 1;//Trạng thái mở
            else  $data['status'] = 0; // Trạng thái khóa
            if(is_array($data)){
                if($permissionModel->update($data,'permission_id = :id',array(':id'=>$id)) == true) $this->_flash->success("Cập nhật module thành công !");
                else $this->_flash->danger("Cập nhật module không thành công !");
                unset($data);

            }

        }
        $this->view->data = $permissionModel->getOne('permission_id = :id',array(':id'=>$id));
        $this->displayLayout('default',$this->render());
    }
    public function actDelete(){
        $permissionModel = new Permissions();
        $id = $this->_request->getParam('id');
        if($permissionModel->delete('permission_id = :id',array(':id'=>$id))) print 'Xóa bản ghi thành công !';
        else print 'Xóa bản ghi không thành công !';
        exit;
    }
    public function actUpdate(){
        if($this->_request->isPost()){
            $dataGroupModel = new UsersGroup();
            $id = $this->_request->getPost('id');
            $permission = $this->_request->getPost('permission');
            if($dataGroupModel->update(array('role_permission'=>$permission),'group_id = :id',array(':id'=>$id))){
                print 'Cập nhật phân quyền thành công !';
            }else{
                print 'Cập nhật phân quyền không thành công !';
            }
            exit;
        }
    }


}