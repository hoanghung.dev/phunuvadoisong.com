<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 06:54 CH
 */
namespace Application\Admin\Controllers;


use Application\Admin\Models\News;

class Article extends Base
{

    public function index(){
        $newsModel = new News();
        $page = $this->_request->getParam('page',1);
        $limit = 30;
        $params['select'] = '*';
        $params['order_by'] = 'news_id DESC';
        $params['page'] = $page;
        $params['limit'] = $limit;
        $data = $newsModel->getDataArr($params);
        $total = $newsModel->getCount($params);
        if(empty($data)) $this->_flash->warning('Chưa có dữ liệu !');
        $this->view->data = $data;
        $this->view->total = $total;
        $this->view->paging = $this->getPaging($total,$page,$limit,5);
        $this->displayLayout('default', $this->render());
    }
    public function add(){
        $newsModel = new News();
        if($this->_request->isPost()){
            $data = array();
            foreach($this->_request->getPosts() as $field=>$value){
                $data[$field] = $value;
            }
            $data['slug'] = $this->toSlug($data['title']);

            if(!empty($_FILES['image']['name'])){
                $data['image'] = $this->uploadImage($_FILES['image'],$data['slug']);
            }
            $data['created_time'] = date(DATE_TIME_FORMAT);
            $data['updated_time'] = date(DATE_TIME_FORMAT);
            $data['user_id'] = $this->_session->auth->user_id;
            if($this->_request->getPost('is_hot')) $data['is_hot'] = 1; else $data['is_hot'] = 0;
            if($this->_request->getPost('status')) $data['status'] = 1; else $data['status'] = 0;
            if($newsModel->getOne('slug = :slug',array(':slug'=>$data['slug']))){
                $this->_flash->warning("Bài viết đã tồn tại !");
            }else{
                if(is_array($data)){
                    if($newsModel->insert($data) == true) $this->_flash->success("Thêm mới bài viết thành công !");
                    else $this->_flash->danger("Thêm mới bài viết không thành công !");
                    unset($data);

                }
            }
        }
        $this->displayLayout('default',$this->render());
    }

    public function edit(){
        $newsModel = new News();
        $id = $this->_request->getParam('id');
        $this->view->data = $newsModel->getOne('news_id = :id',array(':id'=>$id));
        if($this->_request->isPost()){
            $data = array();
            foreach($this->_request->getPosts() as $field=>$value){
                $data[$field] = $value;
            }
            if(!empty($_FILES['image']['name'])){
                $data['image'] = $this->uploadImage($_FILES['image'],$this->toSlug($data['title']));
            }
            $data['updated_time'] = date(DATE_TIME_FORMAT);
            $data['user_id'] = $this->_session->auth->user_id;
            if($this->_request->getPost('is_hot')) $data['is_hot'] = 1; else $data['is_hot'] = 0;
            if($this->_request->getPost('status')) $data['status'] = 1; else $data['status'] = 0;
            if(is_array($data)){
                if($newsModel->update($data,'news_id = :id',array(':id'=>$id)) == true) $this->_flash->success("Cập nhật bài viết thành công !");
                else $this->_flash->danger("Cập nhật bài viết không thành công !");
                unset($data);
            }
        }
        $this->displayLayout('default',$this->render());
    }
    public function actDelete(){
        $newsModel = new News();
        $id = $this->_request->getParam('id');
        $data = $newsModel->delete('news_id = :id',array(':id'=>$id));
        print $data;
        exit;
    }
}