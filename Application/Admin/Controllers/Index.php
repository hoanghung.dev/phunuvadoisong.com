<?php
namespace Application\Admin\Controllers;

use Soul\Registry;


class Index extends Base
{

    public function index(){
        $this->view->flash = $this->_flash;
        $this->displayLayout('default',$this->render());
    }

    public function notFound()
    {
        $this->displayLayout('default',$this->render());
    }
}