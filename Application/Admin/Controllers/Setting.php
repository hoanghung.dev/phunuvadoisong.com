<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 02:30 CH
 */
namespace Application\Admin\Controllers;

use Application\Admin\Models\Settings;

//Type: 1:page, 2:logo, 3:Slider
class Setting extends Base{
    public function index(){
        $settingModel = new Settings();
        if($this->_request->isPost()){
            $data = array();
            foreach($this->_request->getPosts() as $field=>$value){
                $data[$field] = $value;
            }
            $data['updated_time'] = date(DATE_TIME_FORMAT);
            if(is_array($data)){
                if($settingModel->update($data,'setting_id = :id',array(':id'=>1)) == true) $this->_flash->success("Cập nhật thành công !");
                else $this->_flash->danger("Cập nhật không thành công !");
                unset($data);

            }
        }
        $this->view->data = $settingModel->getOne('setting_id = :id',array('id'=>1));
        $this->displayLayout('default',$this->render());
    }
    public function slider(){
        $settingModel = new Settings();
        $param['select'] = '*';
        $param['type'] = 3;
        $this->view->data = $data = $settingModel->getDataArr($param);
        if(empty($data)) $this->_flash->warning('Không có slider nào !');
        $this->displayLayout('default',$this->render());
    }
    public function boxHome(){
        $settingModel = new Settings();
        $param['select'] = '*';
        $param['type'] = 4;//type =4 set box
        $this->view->data = $settingModel->getDataArr($param);
        $this->displayLayout('default',$this->render());
    }
    public function page(){
        $settingModel = new Settings();
        $id = $this->_request->getParam('id');
        $this->view->item = $settingModel->getOne('setting_id = :id',array(':id'=>$id));
        $param['select'] = '*';
        $param['type'] = 2;
        $this->view->data = $settingModel->getDataArr($param);
        $this->displayLayout('default',$this->render());
    }


    public function add(){
        $settingModel = new Settings();
        if($this->_request->isPost()){
            $data['title'] = $this->_request->getPost('title');
            $data['title_page'] = $this->_request->getPost('title_page');
            $data['slug'] = $this->toSlug($this->_request->getPost('title'));
            $data['intro'] = $this->_request->getPost('intro');
            $data['keywords'] = $this->_request->getPost('keywords');
            $data['content'] = $this->_request->getPost('content');
            $data['position'] = $this->_request->getPost('position');
            $data['created_time'] = date(DATE_TIME_FORMAT);
            $data['updated_time'] = date(DATE_TIME_FORMAT);
            $data['type'] = 2;
            if($settingModel->getOne('slug = :slug',array(':slug'=>$data['slug']))){
                echo "Trang đơn đã tồn tại !";
            }else{
                if(is_array($data)){
                    if($settingModel->insert($data) == true) echo "Thêm mới trang đơn thành công !";
                    else echo "Thêm mới trang đơn không thành công !";
                    unset($data);
                    $this->redirect('/page');
                }
            }

        }
        exit;
    }
    public function edit(){
        $settingModel = new Settings();
        if($this->_request->isPost()){
            $id = $this->_request->getPost('setting_id');
            $data['title'] = $this->_request->getPost('title');
            $data['title_page'] = $this->_request->getPost('title_page');
            $data['slug'] = $this->toSlug($this->_request->getPost('title'));
            $data['intro'] = $this->_request->getPost('intro');
            $data['keywords'] = $this->_request->getPost('keywords');
            $data['content'] = $this->_request->getPost('content');
            $data['position'] = $this->_request->getPost('position');
            $data['updated_time'] = date(DATE_TIME_FORMAT);
            if(is_array($data)){
                if($settingModel->update($data,'setting_id = :id',array(':id'=>$id)) == true) echo "Cập nhật trang đơn thành công !";
                else echo "Cập nhật trang đơn không thành công !";
                unset($data);
                $this->redirect('/page');
            }
        }

    }
    function uploadImageBase64($data,$fileName){
        $rootDir = DIR_FOLDER_UPLOAD;
        $path = '/setting/';
        if (!is_dir($rootDir.$path)) {
            mkdir($rootDir.$path, 0777, true);
        }
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
        $data = base64_decode($data);
        if(file_put_contents($rootDir.$path.$fileName.'.png', $data)) return $path.$fileName.'.png';
    }
    public function actInsert(){
        $settingModel = new Settings();
        if(isset($_POST)){
            foreach($_POST as $key=>$item){
                $data[$key] = $item;
            }
        }
        $data['created_time'] = date(DATE_TIME_FORMAT);
        $data['updated_time'] = date(DATE_TIME_FORMAT);
        if($settingModel->insert($data)) print 'Thêm mới cấu hình thành công !';
        else print 'Thêm mới cấu hình không thành công !';
        exit;
    }
    public function actUpdate(){
        $settingModel = new Settings();
        $id= $this->_request->getPost('id');
        if(isset($_POST)){
            foreach($_POST as $key=>$item){
                $data[$key] = $item;
            }
        }
        $data['updated_time'] = date(DATE_TIME_FORMAT);
        if($settingModel->update($data,'setting_id = :id',array(':id'=>$id))) print 'Cập nhật cấu hình thành công !';
        else print 'Cập nhật cấu hình không thành công !';
        exit;
    }
    public function actUpdateLogo(){
        $settingModel = new Settings();
        $id= $this->_request->getPost('id');
        $data['position'] = $this->_request->getParam('position');
        $this->uploadImageBase64($this->_request->getParam('image'),md5('banner_'.time().$data['position']));
        $data['title'] = $this->_request->getParam('title');
        $data['updated_time'] = date(DATE_TIME_FORMAT);
        if($settingModel->update($data,'setting_id = :id',array(':id'=>$id))) print 'Cập nhật logo thành công !';
        else print 'Cập nhật logo không thành công !';
        exit;
    }
    public function actInsertSlider(){
        $settingModel = new Settings();
        $data['type'] = 3;
        $data['position'] = $this->_request->getParam('position');
        $data['image'] = $this->uploadImageBase64($this->_request->getParam('image'),md5('banner_'.time().$data['position']));
        $data['title'] = $this->_request->getParam('title');
        $data['link'] = $this->_request->getParam('link');
        $data['created_time'] = date(DATE_TIME_FORMAT);
        $data['updated_time'] = date(DATE_TIME_FORMAT);
        if($settingModel->insert($data)) print 'Thêm mới slider thành công !';
        else print 'Thêm mới slider không thành công !';
        exit;
    }
    public function actUpdateSlider(){
        $settingModel = new Settings();
        $id= $this->_request->getPost('id');
        $data['position'] = $this->_request->getParam('position');
        $data['image'] = $this->uploadImageBase64($this->_request->getParam('image'),md5('banner_'.time().$data['position']));
        $data['title'] = $this->_request->getParam('title');
        $data['link'] = $this->_request->getParam('link');
        $data['updated_time'] = date(DATE_TIME_FORMAT);
        if($settingModel->update($data,'setting_id = :id',array(':id'=>$id))) print 'Cập nhật slider thành công !';
        else print 'Cập nhật slider không thành công !';
        exit;
    }
    public function actTrash(){
        $settingModel = new Settings();
        $id = $this->_request->getParam('id');
        if($settingModel->update(array('is_trash' => 1),'setting_id = :id',array(':id'=>$id))) print 'Xóa tạm bản ghi thành công !';
        else print 'Xóa tạm bản ghi không thành công !';
        exit;
    }
    public function actUnTrash(){
        $settingModel = new Settings();
        $id = $this->_request->getParam('id');
        if($settingModel->update(array('is_trash' => 0),'setting_id = :id',array(':id'=>$id))) print 'Khôi phục bản ghi thành công !';
        else print 'Khôi phục bản ghi không thành công !';
        exit;
    }

    public function actDelete(){
        $settingModel = new Settings();
        $id = $this->_request->getParam('id');
        if($settingModel->delete('setting_id = :id',array(':id'=>$id))) print 'Xóa bản ghi thành công !';
        else print 'Xóa bản ghi không thành công !';
        exit;
    }
}