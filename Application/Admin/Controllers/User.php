<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 02:30 CH
 */
namespace Application\Admin\Controllers;


use Application\Admin\Models\Users;

class User extends Base{
    private function _registerSession($user){
        $this->_session->auth = $user;
        $_SESSION['isLoggedIn'] = true; // True/false if user is logged in or not, should be same as above
        $_SESSION["UserName"] = "Tony";
        $_SESSION['moxiemanager.filesystem.rootpath'] = DIR_UPLOAD; // Set a root path for this use
    }
    public function index(){
        $userModel = new Users();
        $page = $this->_request->getParam('page',1);
        $limit = 30;
        $params['select'] = '*';
        $params['order_by'] = 'user_id DESC';
        $params['page'] = $page;
        $params['limit'] = $limit;
        $data = $userModel->getDataArr($params);
        $total = $userModel->getCount($params);
        if(empty($data)) $this->_flash->warning('Chưa có dữ liệu !');
        $this->view->data = $data;
        $this->view->total = $total;
        $this->view->paging = $this->getPaging($total,$page,$limit,5);
        $this->view->flash = $this->_flash;
        $this->displayLayout('default', $this->render());
    }
    public function login(){
        if($this->_request->getPost('username') != null && $this->_request->getPost('password') != null){
            $userModel = new Users();
            $userName = $this->_request->getPost('username');
            $userPass = $this->_request->getPost('password');
            $data = $userModel->getOne('username = :name AND password = :pass',array(':name'=>$userName,':pass'=>md5($userPass.'_steven')));
            if(!empty($data)){
                $this->_registerSession($data);
                $this->redirect('/');
            }else $this->_flash->danger('Sai Username hoặc Password ! Vui lòng liên hệ BQT (Skype: steven.mucian) !');
        }
        $this->view->flash = $this->_flash;
        $this->displayLayout('login',$this->render());
    }
    function setSession($username,$password,$cookie=null){
        // Other code for login ($_POST[]....)
        // $row is result of your sql query
        $values = array($username,$this->obscure($password),$row['id']);
        $session = implode(",",$values);

        // check if cookie is enable for login
        if($cookie=='on'){
            setcookie("your_cookie_name", $session, time()+60*60*24*100,'/');
        } else {
            $_SESSION["your_session_name"] = $session;
        }
    }
    public function logout(){
        unset($this->_session->auth);
        $this->redirect('/');
    }
    public function profile(){
        $this->displayLayout('default',$this->render());
    }
    public function setting(){
        $this->displayLayout('default',$this->render());
    }
    public function add(){
        $userModel = new Users();
        if($this->_request->isPost()){
            $data = array();
            foreach($this->_request->getPosts() as $field=>$value){
                $data[$field] = $value;
            }
            $data['password'] = md5($this->_request->getPost('password')."_steven");
            $data['updated_time'] = date('Y-m-d H:i:s');
            //print_r($data);exit;
            if($this->_request->getPost('status')) $data['status'] = true; else $data['status'] = false;
            if($userModel->getOne('username = :user',array(':user'=>$data['username']))){
                $this->_flash->warning('Username đã tồn tại !');
            }else{
                if($userModel->insert($data)) $this->_flash->success("Thêm mới thành viên thành công !");
                else $this->_flash->danger("Thêm mới thành viên không thành công !");
                unset($data);
            }

        }
        $this->view->flash = $this->_flash;
        $this->displayLayout('default',$this->render());
    }
    public function edit(){
        $userModel = new Users();
        $id = $this->_request->getParam('id');
        $oneItem = $userModel->getOne('user_id = :id',array(':id'=>$id));
        if($this->_request->isPost()){
            $data = array();
            foreach($this->_request->getPosts() as $field=>$value){
                $data[$field] = $value;
            }
            if($oneItem->password != $this->_request->getPost('password')) $data['password'] = md5($this->_request->getPost('password').'_steven');
            else $data['password'] = $oneItem->password;
            $data['updated_time'] = date('Y-m-d H:i:s');
            if($this->_request->getPost('status')) $data['status'] = true; else $data['status'] = false;
            if(is_array($data)){
                if($userModel->update($data,'user_id = :id',array(':id'=>$id))) $this->_flash->success("Cập nhật thành viên thành công !");
                else $this->_flash->danger("Cập nhật thành viên không thành công !");
                unset($user);
            }
        }
        $this->view->data = $oneItem;
        $this->view->flash = $this->_flash;
        $this->displayLayout('default',$this->render());
    }

    public function actDelete(){
        $userModel = new Users();
        $id = $this->_request->getParam('id');
        $res = $userModel->delete('user_id = :id',array(':id'=>$id));
        print_r($res);
        exit;
    }
}