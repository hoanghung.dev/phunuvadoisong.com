<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 06:54 CH
 */
namespace Application\Admin\Controllers;


use Application\Admin\Models\Pages;

class Slider extends Base
{

    public function index(){
        $pageModel = new Pages();
        $param['select'] = '*';
        $param['type'] = 2;
        $this->view->data = $data = $pageModel->getDataArr($param);
        if(empty($data)) $this->_flash->warning('Không có slider nào !');
        $this->view->countAll = $pageModel->getCount($param);
        $this->displayLayout('default', $this->render());
    }
    public function add(){
        $pageModel = new Pages();
        if($this->_request->isPost()){
            $data = array();
            foreach($this->_request->getPosts() as $field=>$value){
                $data[$field] = $value;
            }
            $data['type'] = 2;
            if(!empty($_FILES['image']['name'])){
                $data['image'] = $this->uploadImage($_FILES['image'],$this->toSlug($data['title']),'slider');
            }
            $data['created_time'] = date(DATE_TIME_FORMAT);
            $data['updated_time'] = date(DATE_TIME_FORMAT);
            if($this->_request->getPost('status')) $data['status'] = true; else $data['status'] = false;

            if($pageModel->insert($data) == true) $this->_flash->success("Thêm mới slider thành công !");
            else $this->_flash->danger("Thêm mới slider không thành công !");
            unset($data);


        }
        $this->displayLayout('default',$this->render());
    }
    public function edit(){
        $pageModel = new Pages();
        $id = $this->_request->getParam('id');
        if($this->_request->isPost()){
            $data = array();
            foreach($this->_request->getPosts() as $field=>$value){
                $data[$field] = $value;
            }
            $data['type'] = 2;
            if(!empty($_FILES['image']['name'])){
                $data['image'] = $this->uploadImage($_FILES['image'],$this->toSlug($data['title']),'slider');
            }
            $data['updated_time'] = date(DATE_TIME_FORMAT);
            if($this->_request->getPost('status')) $data['status'] = true; else $data['status'] = false;
            if(is_array($data)){
                if($pageModel->update($data,'page_id = :id',array(':id'=>$id)) == true) $this->_flash->success("Cập nhật slider thành công !");
                else $this->_flash->danger("Cập nhật slider không thành công !");
                unset($data);
            }
        }
        $this->view->data = $pageModel->getOne('page_id = :id',array(':id'=>$id));
        $this->displayLayout('default',$this->render());

    }

    public function actDelete(){
        $pageModel = new Pages();
        $id = $this->_request->getPost('id');
        $data = $pageModel->delete('page_id = :id',array(':id'=>$id));
        print $data;
        exit;
    }
}