<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 06:54 CH
 */
namespace Application\Admin\Controllers;


use Application\Admin\Models\Report;

class Reports extends Base
{

    public function index(){
        $this->accessDenied('report');
        $this->displayLayout('default', $this->render());
    }
    public function statisticsScore(){
        $dataModel = new Report();
        $startDate = $this->_request->getParam('startDate');
        $endDate = $this->_request->getParam('endDate');
        $page = $this->_request->getParam('page',1);
        if($startDate != '' && $endDate != '') {
            $startDate = date('Y-m-d 00:00:00', strtotime($this->_request->getParam('startDate')));
            $endDate = date('Y-m-d 23:59:59', strtotime($this->_request->getParam('endDate')));
            $limit = 30;
            $page = intval($page);
            $offset = ($page - 1) * $limit;
            $limitSql = sprintf(' LIMIT %d,%d', $offset, $limit);

            $sql = "SELECT MSISDN,ACTIVE_TIME,DEACTIVE_TIME,SCORE,STATUS,PACKAGE_NAME FROM SUBSCRIBER";
            $sqlCount = "SELECT COUNT(1) FROM SUBSCRIBER";
            $sqlCount .= ' WHERE DATE(ACTIVE_TIME) BETWEEN :start_date AND :end_date';
            $sql .= ' WHERE DATE(ACTIVE_TIME) BETWEEN :start_date AND :end_date';
            $sql .= ' ORDER BY SCORE DESC, ACTIVE_TIME ASC';
            $bind[] = array(
                'element'=>':start_date',
                'value'=>$startDate,
            );
            $bind[] = array(
                'element'=>':end_date',
                'value'=>$endDate,
            );

            $data = $dataModel->queryAll($sql . $limitSql,$bind);
            $totalRecord = $dataModel->countQuery($sqlCount,$bind);
            // echo $totalRecord;
            $this->view->data = $data;
            $this->view->countAll = $totalRecord;
            $Pagination = new \Pagination(array('page' => $page, 'total' => $totalRecord, 'limit' => $limit, 'param'=>'&page=','link' => isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:''));
            $this->view->paging = $Pagination->createLinks();
        }
        $this->displayLayout('default', $this->render());
    }
    public function statisticsMovies(){
        $this->displayLayout('default', $this->render());
    }
    public function statisticsRevenue(){
        $this->displayLayout('default', $this->render());
    }
    public function statisticsCategoryMovies(){
        $this->displayLayout('default', $this->render());
    }
    public function statisticsChangePackage(){
        $this->displayLayout('default', $this->render());
    }
    public function statisticsCancel(){
        $this->displayLayout('default', $this->render());
    }
    public function statisticsViewedNotRegistered(){
        $this->displayLayout('default', $this->render());
    }
    public function statisticsDownloadedMovies(){
        $this->displayLayout('default', $this->render());
    }
    public function statisticsRegisteredPackage(){
        $this->displayLayout('default', $this->render());
    }
    public function statisticsCP(){
        $this->displayLayout('default', $this->render());
    }
    public function statisticsRevenueTotal(){
        $this->displayLayout('default', $this->render());
    }
}