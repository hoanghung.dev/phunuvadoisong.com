<?php
namespace Application\Frontend\Controllers;

use App\Models\Category;
use Application\Frontend\Models\Categories;
use Application\Frontend\Models\Messages;
use Application\Frontend\Models\News;

class Index extends Base
{
    public function x() {
        $newModel = new News();
        $news = $newModel->getDataArr(['news_id' => 2878]);
        var_dump($news[0]->image); die;
    }

    public function index(){
        $categoryModel = new Categories();
        $params['parent_id'] = '0';
        $params1['order_by'] = 'viewed DESC';
        $this->view->listCategory = $categoryModel->getDataArr($params);
        $this->displayLayout('default',$this->render());
    }
    public function boxHot(){
        $newsModel = new News();
        $params['select'] = 'news_id, title, image, created_time';
        $params['is_hot'] = 0;
        $params['limit'] = 8;
        $params['order_by'] = 'created_time DESC';
        $this->view->listHot = $newsModel->getDataArr($params);
        $params1['select'] = 'news_id, title, category_id, image, created_time';
        $params1['order_by'] = 'viewed DESC';
        $params1['limit'] = 0;
        $this->view->listMostViewed = $newsModel->getDataArr($params1);
        echo $this->render();
    }
    
    public function hotCategory(){
        $newsModel = new News();
        $categoryId = $this->_request->getParamInt('category_id');
        $params['is_hot'] = 0;
        $params['category_id'] = $categoryId;
        $params['order_by'] = 'created_time DESC';
        $params['limit'] = 1;
        $this->view->oneHot = $newsModel->getDataArr($params);
        echo $this->render();
    }
    public function hotUrlCate(){
        $newsModel = new News();
        $params['select'] = 'news_id, title, image, created_time';
        $params['is_hot'] = 0;
        $params['limit'] = 3;
        $params['order_by'] = 'created_time DESC';
        $this->view->listHoted = $newsModel->getDataArr($params);
        echo $this->render();
    }

    public function notFound(){
        $this->displayLayout('default',$this->render());
    }
    public function sitemap() {
        $categoryModel = new Categories();
        $this->view->listCategory = $categoryModel->getDataArr();
        echo $this->render();
    }

}