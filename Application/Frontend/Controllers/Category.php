<?php
namespace Application\Frontend\Controllers;

use Application\Frontend\Models\Categories;
use Application\Frontend\Models\News;
use Application\Library\Pagination;

class Category extends Base
{

    public function index() {
        $activePage = $this->_request->getParam('page',1);
        $categoryModel = new Categories();
        $newsModel = new News();
        $categoryId = $this->_request->getParam('id');
        $data = $categoryModel->getOne('category_id = :id',array(':id'=>$categoryId),'category_id, title, title_page, intro, keywords');
        $params['select'] = 'news_id, category_id, title,title_page, intro, user_id, created_time,viewed';
        $params['category_id'] = $categoryId;
        $params['order_by'] = 'created_time DESC';
        $params['limit'] = 30;
        $params['page'] = $activePage;
        $total = $newsModel->getCount($params);
        $this->view->oneCate = $data;
        $this->view->listNews = $newsModel->getDataArr($params);

        $this->view->page = $this->getPaging($total,$activePage,$params['limit'],5,$this->_helper->getUrlCate($data->category_id));

        $SEO['title'] = $data->title;
        $SEO['link'] = $this->_helper->getUrlCate($data->category_id);
        $SEO['description'] = $data->intro;
        $SEO['keywords'] = $data->keywords;
        //$SEO['image'] = $this->_helper->getImageCate($data->news_id);
        $this->view->seo = $SEO;
        $this->displayLayout('default',$this->render());
    }
    function listCateChild($id){
        $categoryModel = new Categories();
        $params['select'] = 'category_id';
        $params['parent_id'] = $id;
        $data = $categoryModel->getDataArr($params);
        $return = '';
        if(!empty($data)) foreach ($data as $k=>$item){
            if($k != 0) $return .= ',';
            $return .= $item->category_id;
        }
        return $return;
    }

}