<?php
namespace Application\Frontend\Controllers;

use Application\Frontend\Models\Settings;
use Soul\Application;
use Soul\Mvc\Controller;
use Soul\Registry;
use Soul\Session;
use Soul\Translate;
use Application\Frontend\Helpers;


class Base extends Controller
{


    //public $DOM;
    /**
     * @var Soul/Helpers/Flash $_flash
     */
    protected $_flash;
    
    protected $_session;

    protected $_params;

    public $_baseUrl;

    public function init()
    {
        parent::init();
        $this->_translate = $this->_getTranslation();
        $this->_session = Session::getInstance();
        $this->initView()->setTranslate($this->_translate)->setSession($this->_session);
        $this->view->params = $this->_params = $this->_request->getParams();
        $this->view->info = $this->getSiteInfo();
        $this->_flash = $this->_helper->flash();
        $this->view->_session = $this->_session;
    }
    public function writeLog($fileName,$content){
        $dir = DIR_FOLDER.'/log/';
        $file = $dir.$fileName.'.log';
        if($fp = @fopen($file,"a")){
            fwrite($fp,$content ."\r\n");
            fclose($fp);
            return true;
        }else{
            return false;
        }
        /*if(!empty($fileName) && !empty($content)){
            $file = $dir.$fileName.'.log';

            // Write the contents to the file,
            // using the FILE_APPEND flag to append the content to the end of the file
            // and the LOCK_EX flag to prevent anyone else writing to the file at the same time
            file_put_contents($file, $content."\r\n", FILE_APPEND | LOCK_EX);
        }*/
    }
    public function getSiteInfo(){
        $settingModel = new Settings();
        return $settingModel->getOne('setting_id = ?',array(1));
    }
    public function redirect($url)
    {
        header('Location:' . $url);
        exit;
    }

    /**
     * @return Translate
     */
    protected function _getTranslation()
    {
        $translate = new Translate(array(
            'locale' => 'vi_VN',
            'file' => 'messages',
            'directory' => __DIR__ . '/../Language'
        ));

        return $translate;
    }

    /**
     * @param string $layout
     * @param null $layoutContent
     */
    public function displayLayout($layout = 'default', $layoutContent = null)
    {
        $content = $this->renderLayout($layout, $layoutContent);
        $this->display($content);
    }

    public function renderLayout($layout = 'default', $layoutContent = null)
    {
        if (null === $layoutContent) {
            $layoutContent = $this->render();
        }
        $this->view->layoutContent = $layoutContent;
        $view = $this->getLayoutScript($layout);
        return $this->view->render($view);
    }

    /**
     * @param null $layout
     * @return string
     */
    public function getLayoutScript($layout = null)
    {
        $script = '_layouts' . DIRECTORY_SEPARATOR . $layout . '.php';
        return $script;
    }

    public function forward($url) {
        $u = explode('/', $url);
       // $this->_helper->action($url[0], );
    }

    public function varDump($array){
        echo "<pre>";
        var_dump($array);
        echo "</pre>";
    }

    function getPaging($totalCount,$activePage = 1,$limit = 10,$maxPage = 5,$url){
        $page = $this->_helper->getHelper('Paging');
        $page->setNumItem($totalCount);
        $page->setCurrentPage($activePage);
        $page->setMaxPage($maxPage);
        $page->setNumItemOnPage($limit);
        $page->setUrl($url);
        $page->setUrlType('/');
        return $page->toString();
    }

    public function cUrl($url,array $post_data=array(),$delete = false,$verbose=false,$ref_url=false,$cookie_location=false,$return_transfer=true)
    {
        $return_val = false;
        $pointer = curl_init();

        curl_setopt($pointer, CURLOPT_URL, $url);
        curl_setopt($pointer, CURLOPT_TIMEOUT, 40);
        curl_setopt($pointer, CURLOPT_RETURNTRANSFER, $return_transfer);
        curl_setopt($pointer, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.28 Safari/534.10");
        curl_setopt($pointer, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($pointer, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($pointer, CURLOPT_HEADER, false);
        curl_setopt($pointer, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($pointer, CURLOPT_AUTOREFERER, true);

        if($cookie_location !== false)
        {
            curl_setopt($pointer, CURLOPT_COOKIEJAR, $cookie_location);
            curl_setopt($pointer, CURLOPT_COOKIEFILE, $cookie_location);
            curl_setopt($pointer, CURLOPT_COOKIE, session_name() . '=' . session_id());
        }

        if($verbose !== false)
        {
            $verbose_pointer = fopen($verbose,'w');
            curl_setopt($pointer, CURLOPT_VERBOSE, true);
            curl_setopt($pointer, CURLOPT_STDERR, $verbose_pointer);
        }

        if($ref_url !== false)
        {
            curl_setopt($pointer, CURLOPT_REFERER, $ref_url);
        }

        if(count($post_data) > 0)
        {
            curl_setopt($pointer, CURLOPT_POST, true);
            curl_setopt($pointer, CURLOPT_POSTFIELDS, $post_data);
        }
        if($delete !== false){
            curl_setopt($pointer, CURLOPT_CUSTOMREQUEST, "DELETE");
        }

        $return_val = curl_exec($pointer);

        $http_code = curl_getinfo($pointer, CURLINFO_HTTP_CODE);

        if($http_code == 404)
        {
            return false;
        }

        curl_close($pointer);

        unset($pointer);

        return $return_val;
    }

    public function getResult($keyword,$page = '1') {
        if(substr_count($keyword,' ')>0)
            $keywords=str_replace(' ','+',$keyword);
        else
            $keywords=$keyword;

        if($page==0)
            $url = "https://www.google.com.vn/search?q=".$keywords;
        else
        {
            $index=($page*10);
            $url = "https://www.google.com.vn/search?q=".$keywords."&start=".$index;
        }
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_REFERER, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $str = curl_exec($curl);
        curl_close($curl);

        $html = $this->DOM->load($str);

        if($html->find('#ires',0)) {
            $i = 0;
            foreach($html->find('#ires li') as $article) {
                if(isset($article->find('h3 a',0)->href)){
                    $link = explode("?q=",$article->find('h3 a',0)->href);
                    $link1 = explode("&",$link[1]);
                    $data[$i]['link'] = $link1[0];
                }
                if(isset($article->find('h3 a',0)->plaintext)) $data[$i]['title'] = utf8_encode($article->find('h3 a',0)->plaintext);
                if(isset($article->find('span.st',0)->innertext)) $data[$i]['description'] = utf8_encode($article->find('span.st',0)->innertext);
                $i++;
            }
            return $data;
        }
    }
    public function getImage($keyword) {
        if(substr_count($keyword,' ')>0)
            $keywords=str_replace(' ','+',$keyword);
        else $keywords=$keyword;

        $url = "https://www.google.com.vn/search?q=".$keywords."&tbm=isch";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_REFERER, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $str = curl_exec($curl);
        curl_close($curl);

        $html = $this->DOM->load($str);
        if($html->find('#search',0)) {
            foreach($html->find('#search td') as $article) {
                return $article->find('img',0)->src;
            }
        }
    }

    function uploadImageURL($url,$dir='',$filename='') {
        $url = trim($url);
        //print_r($url);die('ok');
        if($url){
            $file = fopen($url,"rb");
            if($file){
                $directory = "../Public/uploads/".$dir; // Directory to upload files to.
                $valid_exts = array("jpg","jpeg","gif","png"); // default image only extensions
                $ext = end(explode(".",strtolower(basename($url))));
                if(in_array($ext,$valid_exts)){
                    $rand = rand(1000,9999);
                    if($filename=='') {
                        $filename = 'dwn-'.$rand . basename($url);
                    } else {
                        $filename = $filename.".".$ext;
                    }
                    $newfile = fopen($directory . $filename, "wb"); // creating new file on local server
                    if($newfile){
                        while(!feof($file)){
                            // Write the url file to the directory.
                            fwrite($newfile,fread($file,1024 * 8),1024 * 8); // write the file to the new directory at a rate of 8kb/sec. until we reach the end.
                        }
                        return $directory.$filename;
                    } else { return 'Error Upload Image: Could not establish new file ('.$directory.$filename.') on local server. Be sure to CHMOD your directory to 777.'; }
                } else { return 'Error Upload Image: Invalid file type. Please try another file.'; }
            } else { return 'Error Upload Image: Could not locate the file: '.$url.''; }
        } else { return 'Error Upload Image: Invalid URL entered. Please try again.'; }
    }

    function toSlug($doc) {
        $str = addslashes(html_entity_decode($doc));
        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
        $str = preg_replace("/(đ)/", 'd', $str);
        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
        $str = preg_replace("/(Đ)/", 'D', $str);
        $str = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $str);
        $str = preg_replace("/( )/", '-', $str);
        $str = preg_replace("/(--)/", '-', $str);
        $str = preg_replace("/(--)/", '-', $str);
        $str = str_replace("/", "-", $str);
        $str = str_replace("\\", "-", $str);
        $str = str_replace("+", "", $str);
        $str = str_replace("%", "", $str);
        $str=strtolower($str);
        $str = stripslashes($str);
        return $str;
    }

    public function slugToCateId($slug){
        $category = new Categories();
        $where[':slug'] = $slug;
        $data = $category->getOne('slug = :slug',$where,'category_id');
        return $data['category_id'];
    }

    public function slugToNewId($slug){
        $new = new News();
        $slugArr = explode('/',$slug);
        $num =  count($slugArr);
        if($num > 1) $slug = end($slugArr);
        else $slug;
        $where[':slug'] = $slug;
        $data = $new->getOne('slug = :slug',$where,'news_id');
        return $data['news_id'];
    }

    public function getDateTime(){
        $arr=array('Thứ hai','Thứ ba','Thứ tư','Thứ năm','Thứ sáu','Thứ bảy','Chủ nhật');
        echo $arr[date('N',strtotime($one_channel['time_hienthi']))-1].date(', d/m/Y, H:i',max(strtotime($one_channel['time_hienthi']), strtotime($one_channel['time_hienthi']))).' GMT+7';
    }

    function getDataYoutube($video_id) {
        define('YT_API_URL', 'http://gdata.youtube.com/feeds/api/videos?q=');

        //Using cURL php extension to make the request to youtube API
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, YT_API_URL . $video_id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //$feed holds a rss feed xml returned by youtube API
        $feed = curl_exec($ch);
        curl_close($ch);

        //Using SimpleXML to parse youtube's feed
        $xml = simplexml_load_string($feed);

        $entry = $xml->entry[0];

        //If no entry whas found, then youtube didn't find any video with specified id
        if(!$entry) echo ''; else {
            //exit('Error: no video with id "' . $video_id . '" whas found. Please specify the id of a existing video.');
            $media = $entry->children('media', true);
            $group = $media->group;


            $title = $group->title;//$title: The video title
            $desc = $group->description;//$desc: The video description
            $vid_keywords = $group->keywords;//$vid_keywords: The video keywords
            $thumb = $group->thumbnail[0];//There are 4 thumbnails, the first one (index 0) is the largest.
            //$thumb_url: the url of the thumbnail. $thumb_width: thumbnail width in pixels.
            //$thumb_height: thumbnail height in pixels. $thumb_time: thumbnail time in the video
            list($thumb_url, $thumb_width, $thumb_height, $thumb_time) = $thumb->attributes();
            $content_attributes = $group->content->attributes();
            //$vid_duration: the duration of the video in seconds. Ex.: 192.
            $vid_duration = $content_attributes['duration'];
            //$duration_formatted: the duration of the video formatted in "mm:ss". Ex.:01:54
            $duration_formatted = str_pad(floor($vid_duration/60), 2, '0', STR_PAD_LEFT) . ':' . str_pad($vid_duration%60, 2, '0', STR_PAD_LEFT);
            $author = $entry->author->name;

            //echoing the variables for testing purposes:
            $video['id']=$video_id;
            $video['title']=$title;
            $video['desc']=$desc;
            $video['keywords']=$vid_keywords;
            $video['thumb_url']=$thumb_url;
            $video['thumb_width']=$thumb_width;
            $video['thumb_height']=$thumb_height;
            $video['thumb_time']=$thumb_time;
            $video['duration']=$vid_duration;
            //$video['duration_formatted']=$duration_formatted;
            $video['channel']=$author;

            return $video;
        }
    }

    function uploadPicture($uri){
        $validextensions = array("jpeg", "jpg", "png");
        $temporary = explode(".", $_FILES[$uri]["file"]["name"]);
        $file_extension = end($temporary);
        if ((($_FILES[$uri]["file"]["type"] == "image/png") || ($_FILES[$uri]["file"]["type"] == "image/jpg") || ($_FILES[$uri]["file"]["type"] == "image/jpeg")
            ) && ($_FILES[$uri]["file"]["size"] < 1000000)//Approx. 100kb files can be uploaded.
            && in_array($file_extension, $validextensions)) {
            if ($_FILES[$uri]["file"]["error"] > 0) {
                echo "Return Code: " . $_FILES[$uri]["file"]["error"] . "<br/><br/>";
            } else {
                echo "<span>Your File Uploaded Succesfully...!!</span><br/>";
                echo "<br/><b>File Name:</b> " . $_FILES[$uri]["file"]["name"] . "<br>";
                echo "<b>Type:</b> " . $_FILES[$uri]["file"]["type"] . "<br>";
                echo "<b>Size:</b> " . ($_FILES[$uri]["file"]["size"] / 1024) . " kB<br>";
                echo "<b>Temp file:</b> " . $_FILES[$uri]["file"]["tmp_name"] . "<br>";
                if (file_exists("upload/" . $_FILES[$uri]["file"]["name"])) {
                    echo $_FILES[$uri]["file"]["name"] . " <b>already exists.</b> ";
                } else {
                    move_uploaded_file($_FILES[$uri]["file"]["tmp_name"], "upload/" . $_FILES[$uri]["file"]["name"]);
                    echo "<b>Stored in:</b> " . "upload/" . $_FILES[$uri]["file"]["name"];
                }
            }
        } else {
            echo "<span>***Invalid file Size or Type***<span>";
        }
    }

    public function checkLogin(){
        if (isset($this->_session->auth['fb_id']) && $this->_session->auth['fb_id']>0 ) return true;
        return false;
    }

    function crop($target, $newcopy, $w, $h, $ext) {
        list($w_orig, $h_orig) = getimagesize($target);
        $src_x = ($w_orig / 2) - ($w / 2);
        $src_y = ($h_orig / 2) - ($h / 2);
        $ext = strtolower($ext);
        $img = "";
        if ($ext == "gif"){
            $img = imagecreatefromgif($target);
        } else if($ext =="png"){
            $img = imagecreatefrompng($target);
        } else {
            $img = imagecreatefromjpeg($target);
        }
        $tci = imagecreatetruecolor($w, $h);
        imagecopyresampled($tci, $img, 0, 0, $src_x, $src_y, $w, $h, $w, $h);
        if ($ext == "gif"){
            imagegif($tci, $newcopy);
        } else if($ext =="png"){
            imagepng($tci, $newcopy);
        } else {
            imagejpeg($tci, $newcopy, 84);
        }
    }
    function resize($target, $newcopy, $w, $h, $ext) {
        list($w_orig, $h_orig) = getimagesize($target);
        $scale_ratio = $w_orig / $h_orig;
        if (($w / $h) > $scale_ratio) {
            $w = $h * $scale_ratio;
        } else {
            $h = $w / $scale_ratio;
        }
        $img = "";
        $ext = strtolower($ext);
        if ($ext == "gif"){
            $img = imagecreatefromgif($target);
        } else if($ext =="png"){
            $img = imagecreatefrompng($target);
        } else {
            $img = imagecreatefromjpeg($target);
        }
        $tci = imagecreatetruecolor($w, $h);
        // imagecopyresampled(dst_img, src_img, dst_x, dst_y, src_x, src_y, dst_w, dst_h, src_w, src_h)
        imagecopyresampled($tci, $img, 0, 0, 0, 0, $w, $h, $w_orig, $h_orig);
        if ($ext == "gif"){
            imagegif($tci, $newcopy);
        } else if($ext =="png"){
            imagepng($tci, $newcopy);
        } else {
            imagejpeg($tci, $newcopy, 84);
        }
    }
   
}