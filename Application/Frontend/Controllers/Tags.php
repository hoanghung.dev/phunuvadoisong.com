<?php
namespace Application\Frontend\Controllers;

use Application\Frontend\Models\News;

class Tags extends Base {

    public function index() {
        $newsModel = new News();
        $tag = $this->_request->getParam('slug');
        $params['select'] = 'news_id, category_id, title,title_page, intro, user_id, created_time';
        $params['search'] = $tag;
        $params['order_by'] = 'news_id DESC';
        $params['limit'] = 20;
        
        $total = $newsModel->getCount($params);
        $this->view->url = $url = '/tags/'.$tag;
        $this->view->tag = $title = str_replace('-',' ',$tag);
        $this->view->listNews = $newsModel->getDataArr($params);
        $activePage = $this->_request->getParam('page',1);
        $this->view->page = $this->getPaging($total,$activePage,$params['limit'],5,$url);

        $SEO['title'] = $title;
        $SEO['link'] = $url;
        $SEO['description'] = $title .': This info '.$title.' video clip, image photo about '.$title;
        $SEO['keywords'] = $title;
        //$SEO['image'] = $this->_helper->getImageCate($data->news_id);
        $this->view->seo = $SEO;
        $this->displayLayout('default',$this->render());
    }
}