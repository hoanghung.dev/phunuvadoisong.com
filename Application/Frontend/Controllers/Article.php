<?php
namespace Application\Frontend\Controllers;

use Application\Frontend\Models\News;
use Application\Frontend\Models\Users;

class Article extends Base {

    public function search() {
        $newsModel = new News();
        $tag = $this->_request->getParam('q');
        $this->view->url = $url = '/search/'.$tag;
        $this->view->tag = $title = urldecode($tag);
        $params['select'] = 'news_id, category_id, title,title_page, intro, user_id, created_time';
        $params['search'] = $title;
        $params['order_by'] = 'news_id DESC';
        $params['limit'] = 20;

        $total = $newsModel->getCount($params);

        $this->view->listNews = $newsModel->getDataArr($params);
        $activePage = $this->_request->getParam('page',1);
        $this->view->page = $this->getPaging($total,$activePage,$params['limit'],5,$url);
        $SEO['title'] = "Search: ".$title;
        $SEO['link'] = $url;
        $SEO['description'] = "Search for ".$title .': This info '.$title.' video clip, image photo about '.$title;
        $SEO['keywords'] = $title;
        //$SEO['image'] = $this->_helper->getImageCate($data->news_id);
        $this->view->seo = $SEO;
        $this->displayLayout('default',$this->render());
    }

    public function detail() {
        $newsId = $this->_request->getParamInt('id');
        $new_model = new News();
        $params['limit'] = 3;
        $this->view->data = $oneItem = $new_model->getOne('news_id = :id',array(':id'=>$newsId));
        $this->flushView($newsId,$oneItem->viewed);
        $SEO['title'] = $oneItem->title;
        $SEO['link'] = $this->_helper->getUrlNews($newsId);
        $SEO['description'] = $oneItem->intro;
        $SEO['keywords'] = $oneItem->keywords;
        $SEO['image'] = $oneItem->image;
        $this->view->seo = $SEO;
        $this->displayLayout('default',$this->render());
    }

    public function flushView($id,$view){
        $newsModel = new News();
        $newsModel->update(array('viewed'=>$view),'news_id = :id',array(':id'=> $id));
    }
    public function related(){
        $newsModel = new News();
        $params['select']= 'news_id,title,title_page,intro,created_time,viewed';
        $params['search'] = $this->_request->getParam('title');
        $this->view->data = $newsModel->getDataArr($params);
        echo $this->render();
    }

}