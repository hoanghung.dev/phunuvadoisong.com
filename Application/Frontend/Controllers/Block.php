<?php
/**
 * Created by PhpStorm.
 * User: Steven
 * Date: 13/05/2014
 * Time: 15:13
 */
namespace Application\Frontend\Controllers;

use Application\Frontend\Models\Image;
use Application\Frontend\Models\News;
use Application\Frontend\Models\Categories;
use Application\Frontend\Models\Pages;
use Application\Frontend\Models\Tags as Tag;

class Block extends Base
{
    public $arrBcrumb = array();
    public function boxMostViewed(){
        $newsModel = new News();
        $params['select'] = 'news_id, title, title_page, created_time, category_id, image';
        $params['order_by'] = 'created_time DESC';
        $params['limit'] = 5;
        $this->view->mostview = $newsModel->getDataArr($params);
        echo $this->render();
    }
    public function boxSideBar() {
        $newsModel = new News();
        $params['select'] = 'news_id, title, title_page, created_time, category_id, image';
        $params['order_by'] = 'created_time DESC';
        $params['limit'] = 10;
        $this->view->listsidebar = $newsModel->getDataArr($params);
        echo $this->render();
    }
    public function boxHeader() {
        $categoryModel = new Categories();
        $params['parent_id'] = '0';
        $params['order_by'] = 'order_no ASC';
        $params['limit'] = 30;
        //print_r($this->params());
        $this->view->category_id = 1;
        $this->view->listMenu = $categoryModel->getDataArr($params);
        echo $this->render();
    }
    
    public function boxImage(){
        $newsModel = new News();
        $params['select'] = 'news_id, title, title_page, created_time, category_id, image';
        $params['order_by'] = 'created_time DESC';
        $params['limit'] = 3;
        $this->view->listImage = $newsModel->getDataArr($params);
        echo $this->render();
    }
    function getCategoryId(){
        $params = $this->params();
        if($params['controller'] == 'category'){
            $id = $params['controller']['id'];
        }
    }

    public function breadcrumb(){
        $newsModel = new News();
        $params = $this->_request->getParams();
        $controller = $params['router']['controller'];
        $id = isset($params['id']) ? $params['id'] : '';
        $slug = isset($params['slug']) ? $params['slug'] : '';
        $mapArr = array();
        switch($controller){
            case 'article':
                $oneNews = $newsModel->getOne('news_id = ?',array($id),'news_id,category_id,title,slug');
                $this->recursionCategory($oneNews->category_id, 0);
                $mapArr[10]['title'] = $oneNews->title;
                $mapArr[10]['slug'] = $oneNews->slug . '-' . $oneNews->news_id . '.html';
                if(!empty($this->arrBcrumb)) foreach($this->arrBcrumb as $key => $value){
                    $mapArr[$key] = $value;
                }
                break;
           case 'category':
                $category_model = new Categories();
                $category = $category_model->getOne('slug = :slug', array(':slug'=>$slug),'category_id');
                $this->recursionCategory($category->category_id, 0);
                if(!empty($this->arrBcrumb)) foreach($this->arrBcrumb as $key => $value){
                   $mapArr[$key] = $value;
                }
                break;
        }
        $this->view->breadcrumb = array_reverse($mapArr);
        echo $this->render();
    }

    private function recursionCategory($cateId, $i){
        $categoryModel = new Categories();
        $dataCate = $categoryModel->getOne('category_id = :id',array(':id'=>$cateId),'parent_id,title,slug,category_id');
        if(isset($dataCate) && $dataCate != false){
            $this->arrBcrumb[$i]['title'] = $dataCate->title;
            $this->arrBcrumb[$i]['slug'] = _ROOT_HOME . $dataCate->slug;
            if($dataCate->parent_id != 0){
                $i++;
                $this->recursionCategory($dataCate->parent_id,$i);
            }
        }
        return true;
    }

    public function slide() {
        $pageModel = new Pages();
        $this->view->listSlider = $slider = $pageModel->getDataArr(array('type'=>2));
        echo $this->render();
    }

    public function search() {
        echo $this->render();
    }

    public function pageface() {
        echo $this->render();
    }




    public function boxFooter(){
        $categoryModel = new Categories();
        $params['parent_id'] = '0';
        $this->view->listMenu = $categoryModel->getDataArr($params);
        echo $this->render();
    }


}