<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 23/04/2015
 * Time: 09:15 CH
 */
namespace Application\Frontend\Models;

use Soul\Mvc\Model;
use Soul\Registry;

class News extends Model
{
    protected  $_tbl ='default_news';

    public function init()
    {
        $this->_mysql = Registry::get('Mysql');
        $this->_memcache = Registry::get('Memcache');
    }

    public function getAll()
    {
        return $this->_mysql->select($this->_tbl());
    }

    public function getOne($where, $bind, $select="*")
    {
        $sql = sprintf('SELECT %s FROM %s WHERE %s',$select,$this->_tbl, $where);
        $key = md5(MEMCACHE_KEY.$sql.serialize($bind));

        if(ENABLE_MEMCACHED == true) $data = $this->_memcache->get($key);
        else $data = '';
        if(!empty($data)){
            return $data;
        }else {
            $st = $this->_mysql->prepare($sql);
            $st->execute($bind);
            $data = $st->fetch(\PDO::FETCH_OBJ);
            if(ENABLE_MEMCACHED == true) $this->_memcache->set($key,$data,false,MEMCACHED_TIME);
            return $data;
        }
    }
    public function getNavigator($where, $bind, $select="title,intro,created_time,content,keywords,user_id")
    {
        $sql = sprintf('SELECT MIN(news_id),%s FROM %s WHERE news_id < :id UNION SELECT MIN(news_id),%s FROM %s WHERE news_id = :id UNION SELECT MIN(news_id),%s FROM %s WHERE news_id > :id',$select,$this->_tbl, $where,$select,$this->_tbl, $where,$select,$this->_tbl, $where);
        $bind = array($bind);
        $key = md5(MEMCACHE_KEY.$sql.serialize($bind));

        if(ENABLE_MEMCACHED == true) $data = $this->_memcache->get($key);
        else $data = '';
        if(!empty($data)){
            return $data;
        }else {
            $st = $this->_mysql->prepare($sql);
            $st->execute($bind);
            $data = $st->fetch(\PDO::FETCH_OBJ);
            if(ENABLE_MEMCACHED == true) $this->_memcache->set($key,$data,false,MEMCACHED_TIME);
            return $data;
        }
    }
    public function getCount($args = null)
    {
        $category_id = '';
        $is_hot = '';
        $not_in = '';
        $in = '';
        $search = '';
        $bind = array();


        $default = array('news_id' => 0,'category_id'=>NULL,'is_hot'=>NULL,'not_in' => 0, 'in' => 0,'search'=> null, 'order_by' => null,'limit' => 0, 'page'=>1,);
        $args = $this->parseArgs($args, $default);
        extract($args);

        $where = 'WHERE 1 = 1';

        if ($in != 0){
            $where .= ' AND news_id IN (:news_id)';
            $bind[] = array(
                'element'=>':news_id',
                'value'=>$in,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if ($not_in != 0){
            $where .= ' AND news_id NOT IN (:news_id)';
            $bind[] = array(
                'element'=>':news_id',
                'value'=>$not_in,
                //'type'=>'\PDO::PARAM_INT'
            );
        }
        if ($is_hot != NULL){
            $where .= ' AND is_hot = :is_hot';
            $bind[] = array(
                'element'=>':is_hot',
                'value'=>$is_hot,
                //'type'=>'\PDO::PARAM_INT'
            );
        }
        if ($category_id != NULL){
            if (strpos($category_id, ',') !== false){
                $where .= ' AND category_id IN (:category_id)';
                $bind[] = array(
                    'element'=>':category_id',
                    'value'=>$category_id,
                    //'type'=>'\PDO::PARAM_INT'
                );
            } else{
                $where .= ' AND category_id = :category_id';
                $bind[] = array(
                    'element'=>':category_id',
                    'value'=>$category_id,
                    //'type'=>'\PDO::PARAM_INT'
                );
            }
        }

        if($search != null){
            $where .= ' AND MATCH(title,keywords) AGAINST (:search)';
            $bind[] = array(
                'element'=>':search',
                'value'=>$search,
            );
        }

        $sql = sprintf('SELECT count(*) FROM %s %s', $this->_tbl, $where);
        $key = md5(MEMCACHE_KEY.$sql.serialize($bind));
        /*echo "<!--<pre>";
        echo $sql;
        print_r($bind);
        echo "<pre>-->";*/
        if(ENABLE_MEMCACHED == true) $data = $this->_memcache->get($key);
        else $data = '';
        if(!empty($data)){
            return $data;
        }else{
            $st = $this->_mysql->prepare($sql);
            if(is_array($bind)) foreach($bind as $item){
                $st->bindParam($item['element'],$item['value'],isset($item['type'])?$item['type']:null,10);
            }
            $st->execute();
            $data = $st->fetchColumn();
            if(ENABLE_MEMCACHED == true) $this->_memcache->set($key,$data,false,MEMCACHED_TIME);
            return $data;
        }
    }
    public function getDataArr($args = null)
    {
        $select = '';
        $prev_id = '';
        $next_id = '';
        $category_id = '';
        $status = '';
        $is_hot = '';
        $not_in = '';
        $in = '';
        $in_cat = '';
        $order_by = '';
        $order = '';
        $limit = '';
        $page = '';
        $search = '';
        $bind = array();


        $default = array('select'=>'*','news_id' => 0,'category_id'=>NULL,'prev_id'=>NULL,'next_id'=>NULL,'status'=>NULL,'is_trash'=>0,'is_hot'=>NULL,'not_in' => 0,'in_cat'=>0, 'in' => 0,'search'=> null,'order_by' => null,'limit' => 0, 'page'=>1);
        $args = $this->parseArgs($args, $default);
        extract($args);

        $where = 'WHERE status = 1';
//        $bind[] = array(
//            'element'=>':is_trash',
//            'value'=>$is_trash,
//            //'type'=>'\PDO::PARAM_INT'
//        );


        if ($in != 0){
            //$in = implode(',',$in);
            $where .= ' AND FIND_IN_SET(news_id ,(:news_id))';
            $bind[] = array(
                'element'=>':news_id',
                'value'=>$in,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if ($in_cat != 0){
            $where .= ' AND category_id IN ('.$in_cat.')';
        }

        if ($not_in != 0){
            $not_in = explode(',',$not_in);
            $where .= ' AND news_id NOT IN (:news_id)';
            $bind[] = array(
                'element'=>':news_id',
                'value'=>$not_in,
                //'type'=>'\PDO::PARAM_INT'
            );
        }
        if ($is_hot != NULL){
            $where .= ' AND is_hot = :is_hot';
            $bind[] = array(
                'element'=>':is_hot',
                'value'=>$is_hot,
                //'type'=>'\PDO::PARAM_INT'
            );
        }
        if ($prev_id != NULL){
            $where .= ' AND news_id < :prev_id';
            $bind[] = array(
                'element'=>':prev_id',
                'value'=>$prev_id,
                //'type'=>'\PDO::PARAM_INT'
            );
        }
        if ($next_id != NULL){
            $where .= ' AND news_id > :next_id';
            $bind[] = array(
                'element'=>':next_id',
                'value'=>$next_id,
                //'type'=>'\PDO::PARAM_INT'
            );
        }
        if ($category_id != NULL){
            if (strpos($category_id, ',') !== false){
                $where .= ' AND category_id IN (:category_id)';
                $bind[] = array(
                    'element'=>':category_id',
                    'value'=>$category_id,
                    //'type'=>'\PDO::PARAM_INT'
                );
            } else{
                $where .= ' AND category_id = :category_id';
                $bind[] = array(
                    'element'=>':category_id',
                    'value'=>$category_id,
                    //'type'=>'\PDO::PARAM_INT'
                );
            }
        }

        if ($status != NULL){
            $where .= ' AND status = :status';
            $bind[] = array(
                'element'=>':status',
                'value'=>$status,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if($search != null){
            $where .= ' AND MATCH(title,keywords) AGAINST (:search)';
            $bind[] = array(
                'element'=>':search',
                'value'=>$search,
            );
        }

        if($order_by != null)
            $order = sprintf('ORDER BY %s', $order_by);

        if($limit != 0){
            $page = intval($page);
            $offset = ($page-1)*$limit;
            $limit = sprintf('LIMIT %d,%d',$offset,$limit);
        }else $limit = 'LIMIT 0,10';

        $sql = sprintf('SELECT %s FROM %s %s %s %s',$select, $this->_tbl, $where, $order, $limit);
        //echo $sql; print_r($bind);
        $key = md5(MEMCACHE_KEY.$sql.serialize($bind));
        if(ENABLE_MEMCACHED == true) $data = $this->_memcache->get($key);
        else $data = '';
        if(!empty($data)){
            return $data;
        }else {
            $st = $this->_mysql->prepare($sql);
            if (is_array($bind)) foreach ($bind as $item) {
                $st->bindParam($item['element'], $item['value'], isset($item['type']) ? $item['type'] : null, 10);
            }
            $st->execute();
            $data = $st->fetchAll(\PDO::FETCH_OBJ);
            if(ENABLE_MEMCACHED == true) $this->_memcache->set($key,$data,false,MEMCACHED_TIME);
            return $data;
        }
    }

    public function insert($arr){
        return $this->_mysql->insert($this->_tbl, $arr);
    }
    public function update($arr, $where, $bind){
        return $this->_mysql->update($this->_tbl, $arr, $where, $bind);
    }
    public function delete($where,$bind){
        return $this->_mysql->delete($this->_tbl, $where, $bind);
    }
}