<div class="cs-container">
    <!-- Page title -->
    <h1 class="cs-page-title"><a href="<?php echo $this->url; ?>" title="<?php echo $this->tag; ?>"><?php echo $this->tag; ?></a></h1>

    <!-- Main content -->
    <div class="cs-main-content cs-sidebar-on-the-right">
        <!-- Block layout 4 -->
        <div class="cs-post-block-layout-4">
            <?php if(!empty($this->listNews)) foreach($this->listNews as $oneItem): ?>
                <!-- Post item -->
                <div class="cs-post-item hidden visible cs-animate-element">
                    <div class="cs-post-thumb">
                        <div class="cs-post-format-icon">
                            <i class="fa fa-play"></i>
                        </div>
                        <a href="<?php echo $this->getUrlNews($oneItem->news_id); ?>" title="<?php echo $oneItem->title; ?>"><img src="<?php echo $this->getImageNews($oneItem->news_id); ?>" alt="<?php echo $oneItem->title; ?>"></a>
                    </div>
                    <div class="cs-post-inner">
                        <h3><a href="<?php echo $this->getUrlNews($oneItem->news_id); ?>" title="<?php echo $oneItem->title; ?>"><?php echo $oneItem->title; ?></a></h3>
                        <div class="cs-post-meta cs-clearfix">
                            <span class="cs-post-meta-author"><?php echo $this->getAuthor($oneItem->user_id); ?></span>
                            <span class="cs-post-meta-date"><?php echo $this->timeAgo($oneItem->created_time,'d-m-Y'); ?></span>
                                    <span class="cs-post-meta-rating" title="Rated 2.50 out of 5">
                                        <span style="width: 50%">2.50 out of 5</span>
                                    </span>
                        </div>
                        <p class="cs-post-excerpt"><?php echo $oneItem->intro; ?></p>
                        <a class="cs-post-read-more" href="<?php echo $this->getUrlNews($oneItem->news_id); ?>">Read more <i
                                class="fa fa-angle-double-right"></i></a>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
        <!-- Pagination -->
        <div class="text-center" >
            <ul class="page-numbers">
                <?php echo isset($this->page)?$this->page:''; ?>
            </ul>
        </div>
    </div>

    <?php echo $this->action('boxSidebar', 'Block', 'Frontend');?>
</div>