<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if lt IE 7 ]> <html lang="en" class="ie6">    <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <?php if(isset($this->seo)): $SEO = $this->seo; ?>
        <title><?php echo isset($SEO['title'])?$SEO['title']:''; ?> | <?php echo isset($this->info->title)?$this->info->title:''; ?></title>
        <meta name="description" content="<?php echo isset($SEO['description'])?strip_tags($SEO['description']):''; ?>"/>
        <meta name="keywords" content="<?php echo isset($SEO['keywords'])?$SEO['keywords']:''; ?>"/>
        <meta property="og:title" content="<?php echo isset($SEO['title'])?$SEO['title']:''; ?> | <?php echo isset($this->info->title)?$this->info->title:''; ?>" />
        <meta property="og:description" content="<?php echo isset($SEO['description'])?strip_tags($SEO['description']):''; ?>" />
        <meta property="og:image" content="<?php echo $this->getImageNews($SEO['image'],600,300); ?>" />
        <meta property="og:url" content="<?php echo isset($SEO['link'])?$SEO['link']:''; ?>" />
        <link rel="canonical" href="<?php echo isset($SEO['link'])?$SEO['link']:''; ?>"/>
    <?php else: ?>
        <title><?php echo isset($this->info->title_page)?$this->info->title_page:''; ?> | <?php echo isset($this->info->title)?$this->info->title:''; ?></title>
        <meta name="description" content="<?php echo isset($this->info->description)?$this->info->description:''; ?>"/>
        <meta name="keywords" content="<?php echo isset($this->info->keywords)?$this->info->keywords:''; ?>"/>
        <meta property="og:title" content="<?php echo isset($this->info->title_page)?$this->info->title_page:''; ?> | <?php echo isset($this->info->title)?$this->info->title:''; ?>" />
        <meta property="og:description" content="<?php echo isset($this->info->description)?$this->info->description:''; ?>" />
        <meta property="og:image" content="<?php echo $this->getImageNews('logo.png',400,200); ?>" />
        <meta property="og:url" content="<?php echo _ROOT_HOME; ?>" />
        <link rel="canonical" href="<?php echo _ROOT_HOME; ?>"/>
    <?php endif; ?>
    <meta property="og:type" content="website" />
    <meta name="author" content="Sakura Trần Yến" />
    <meta property="og:site_name" content="<?php echo _ROOT_HOME; ?>" />
    <meta property="fb:admins" content="steven.mucian"/>
    <meta property="fb:app_id" content="624120221076245"/>

    <!-- ~~~=| Fonts files |==-->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,300,700,900,700italic,400italic,300italic,100' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,400italic,500,700,700italic,900' rel='stylesheet' type='text/css'>

    <!-- ~~~=| Font awesome |==-->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">

    <!-- ~~~=| Bootstrap css |==-->
    <link rel="stylesheet" href="/css/bootstrap.css">

    <!-- ~~~=| Theme files |==-->
    <link rel="stylesheet" href="/css/style.css" type="text/css">
    <link rel="stylesheet" href="/css/responsive.css">
    <link rel="stylesheet" href="/css/custom.css" type="text/css">

    <!-- Favicons -->
    <link rel="apple-touch-icon-precomposed" href="images/apple-touch-icon-precomposed.png">
    <link rel="shortcut icon" type="image/ico" href="images/favicon.ico"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!-- Done by Shakhawat H. from codingcouples.com // -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
   <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-82836821-1', 'auto');
  ga('send', 'pageview');

</script>
   <script>
        // kéo xuống khoảng cách 600px thì xuất hiện nút Top-up
        var offset = 600;

        // thời gian di trượt 0.95s ( 1000 = 1s )
        var duration = 950;
        $(function(){
        $(window).scroll(function () {
        if ($(this).scrollTop() > offset) $('#top-up').fadeIn(duration);
        else $('#top-up').fadeOut(duration);
        });
        $('#top-up').click(function () {
        $('body,html').animate({scrollTop: 0}, duration);
        });
        });
    </script>
</head>
<body>
<div id="fb-root"></div>

<?php echo $this->action('boxHeader','block','Frontend',$this->params); ?>
<?php echo $this->layoutContent; ?>
<?php echo $this->action('boxFooter','block','Frontend',$this->params); ?>

<!-- ~~~=| Latest jQuery |=~~~ -->
<script src="https://code.jquery.com/jquery.min.js"></script>

<!-- ~~~=| Bootstrap jQuery |=~~~ -->
<script src="/js/bootstrap.min.js"></script>

<!-- ~~~=| Opacity & Other IE fix for older browser |=~~~ -->
<!--[if lte IE 8]>
<script type="text/javascript" src="/js/ie-opacity-polyfill.js"></script>
<![endif]-->

<!-- ~~~=| Theme jQuery |=~~~ -->
<script src="/js/main.js"></script>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6&appId=624120221076245";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    
</body>
</html>
