<?php if(!empty($this->oneHot)) foreach ($this->oneHot as $item): ?>
<div class="fs_news_left">
    <div class="single_fs_news_left_text">
        <div class="fs_news_left_img"><a href="<?php echo $this->getUrlNews($item->news_id);?>"> <img src="<?php echo $this->getImageNews($item->news_id,408,189); ?>" alt="<?php echo $item->title; ?>" /></a>
            <div class="br_cam br_vid_big_s"> <a class="fa fa-camera" href=""></a> </div>
        </div>
        <h4><a href="<?php echo $this->getUrlNews($item->news_id); ?>" title="<?php echo $item->title; ?>"><?php echo $item->title; ?></a></h4>
        <p style="overflow: hidden;display: -webkit-box;text-overflow: ellipsis;-webkit-box-orient: vertical;"><?php echo $item->intro;?></p>
        <p> <i class="fa fa-clock-o"></i> <?php echo $this->timeAgo($item->created_time); ?> <i class="fa fa-comment"></i> 19 </p>
        <?php $listNews = $this->listNewsInCategories($item->category_id); if(!empty($listNews)): ?>
	        <?php foreach ($listNews as $k=>$oneItem): ?>
	        	 <?php $oneCate = $this->getCategory($oneItem->category_id,'title, title_page'); if(!empty($oneCate)): ?>    
	        		<p style="overflow: hidden;
                   text-overflow: ellipsis;
                   display: -webkit-box;
                   line-height: 14px;     /* fallback */
                   max-height: 32px;      /* fallback */
                   -webkit-line-clamp: 1; /* number of lines to show */
                   -webkit-box-orient: vertical;"> 
	        			<a href="<?php echo $this->getUrlNews($oneItem->news_id); ?>" title="<?php echo $oneItem->title; ?>"><?php echo $oneItem->title; ?><br/></a>
	        		</p>
	            <?php endif; ?>
	    	<?php endforeach; ?>
	    <?php endif;?>
    </div>
</div>
<?php endforeach; ?>