<div class="container singlefullwidth sitecontainer single-wrapper bgw">
    <div class="row">
        <div class="col-md-10 col-md-offset-1 m22 single-post">
            <div class="widget">
                <div class="large-widget m30">
                    <div class="post clearfix">

                        <div class="title-area text-center">
                            <h1>Giới thiệu trung tâm gia sư giỏi</h1>

                            <div class="large-post-meta">
                                <span class="hidden-xs"><a href="/gioi-thieu.html#comments"><i class="fa fa-comments-o"></i> Bình Luận</a></span>
                                <small class="hidden-xs">&#124;</small>
                                <span class="hidden-xs"><a href="/gioi-thieu.html"><i class="fa fa-eye"></i> 19223</a></span>
                            </div><!-- end meta -->
                            
                        </div><!-- /.pull-right -->

                    </div><!-- end post -->

                    <div class="post-desc">
                        <p><img src="//bizweb.dktcdn.net/thumb/2048x2048/100/036/836/files/12049161-768736549915710-69448.png?v=1449340225477"></p>

                        <p>&nbsp;Với phương pháp: Dạy sát chương trình – Dạy sâu kiến thức – Dạy đúng chuyên môn. Trung tâm gia sư giỏi Hà Nội đảm nhận cung cấp gia sư với chất lượng tốt nhất, nhận dạy tại nhà cho các em học sinh từ lớp 1 đến lớp 12, ôn thi vào lớp 10, thi TN, ĐH, CĐ, TH chuyên nghiệp với tất cả các môn: TOÁN, LÝ, HÓA, VĂN, NGOẠI NGỮ, LUYỆN CHỮ ĐẸP…</p>

                        <p>Cùng với đội ngũ gia sư là những giáo viên, sinh viên ưu tú được tuyển chọn kỹ lưỡng, có kiến thức chuyên môn vững chắc, có nghiệp vụ sư phạm và tâm huyết với nghề, chúng tôi tự tin sẽ giúp các học sinh :</p>

                        <p>▶&nbsp;Củng cố kiến thức cơ bản, tạo nền tảng vững chắc trong học tập cho các em học sinh yếu và trung bình.&nbsp;&nbsp;<br>
                            ▶&nbsp;Bồi dưỡng và từng bước nâng cao kiến thức cho các em có triển vọng khá, giỏi.<br>
                            ▶&nbsp;Tư vấn tâm lý, truyền đạt kinh nghiệm và nâng cao kiến thức cho các em thi vượt cấp, thi TN, ĐH, CĐ, thi HS giỏi.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
                            ▶&nbsp;Trung tâm sẽ dành 1 buổi&nbsp;để đánh giá trình độ của học sinh từ đó đưa ra phương pháp dạy phù hợp nhất! &nbsp;</p>

                        <p><img src="//bizweb.dktcdn.net/thumb/1024x1024/100/036/836/files/12049161-768736549915710-69448.png?v=1449340225477"></p>

                        <p>Hotline:&nbsp;<strong>096</strong><strong>8.54</strong><strong>9.222 / 0936</strong><strong>.126.239 / 04.62.819.777</strong>&nbsp;<strong>( Cô Trang )</strong>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</p>

                        <p><br>
                            VP1: Tầng 3, nhà B10, TT ĐH Sư Phạm, Cầu Giấy, Hà Nội.</p>

                        <p>VP2: P609 nhà E5 tập thể ĐHBK, Tạ Quang Bửu, Hà Nội. &nbsp;</p>

                        <div class="post-sharing">
                            <ul class="list-inline">
                                <li><a href="#" class="fb-button btn btn-primary"><i class="fa fa-facebook"></i> <span class="hidden-xs">Share on Facebook</span></a></li>
                                <li><a href="#" class="tw-button btn btn-primary"><i class="fa fa-twitter"></i> <span class="hidden-xs">Tweet on Twitter</span></a></li>
                                <li><a href="#" class="gp-button btn btn-primary"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div><!-- end post-sharing -->
                    </div><!-- end post-desc -->
                    

                    <div id="comments" class="row">
                        <div class="fb-comments" data-href="" data-width="100%" data-numposts="5"></div>
                    </div><!-- end row -->

                </div><!-- end large-widget -->
            </div><!-- end widget -->
        </div><!-- end col -->
    </div><!-- end row -->
</div><!-- end container -->