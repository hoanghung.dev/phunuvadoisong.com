<!-- ~~~=| Banner START |=~~~ -->
<?php echo $this->action('boxHot', 'index', 'Frontend'); ?>
<!-- ~~~=| Banner END |=~~~ -->
<section class="main_news_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12">
                <!-- ~~~=| Fashion area START |=~~~ -->
                <div class="fashion_area">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <?php if (!empty($this->listCategory)) foreach ($this->listCategory as $item): ?>
                                <div class="header_fasion">
                                    <div class="left_fashion main_nav_box">
                                        <ul>
                                            <li class="nav_fashion"><a
                                                    href="<?php echo $this->getUrlCate($item->category_id); ?>"
                                                    title="<?php echo $item->title_page; ?>"><?php echo $item->title; ?></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <?php
                                    $cateModel = new \Application\Frontend\Models\Categories();
                                    $childs = $cateModel->getDataArr(['parent_id' => $item->category_id]);
                                    if (!empty($childs)):
                                        ?>
                                        <div class="fasion_right">
                                            <ul>
                                                <?php foreach ($childs as $child): ?>
                                                    <li>
                                                        <a href="<?php echo $this->getUrlCate($child->category_id); ?>"
                                                           title="<?php echo $child->title_page; ?>"><?php echo $child->title; ?>
                                                        </a>
                                                    </li>
                                                <?php endforeach;; ?>
                                            </ul>
                                        </div>
                                        <?php
                                    endif;
                                    ?>
                                </div>
                                <?php $listNews = $this->ListNewsInCategories($item->category_id);
                                if (!empty($listNews)): ?>
                                    <div class="fashion_area_box"
                                         style="overflow: hidden;display: -webkit-box;text-overflow: ellipsis;-webkit-box-orient: vertical;">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="fs_news_left">
                                                    <div class="single_fs_news_left_text">
                                                        <div class="fs_news_left_img"><a
                                                                href="<?php echo $this->getUrlNews($listNews[0]->news_id); ?>">
                                                                <img
                                                                    src="<?php echo $this->getImageNews($listNews[0]->news_id, 408, 189); ?>"
                                                                    alt="<?php echo $listNews[0]->title; ?>"/></a>
                                                            <div class="br_cam br_vid_big_s"><a class="fa fa-camera"
                                                                                                href=""></a></div>
                                                        </div>
                                                        <h4><a href="<?php echo $this->getUrlNews($listNews[0]->news_id); ?>"
                                                               title="<?php echo $item->title; ?>"><?php echo $listNews[0]->title; ?></a>
                                                        </h4>
                                                        <p style="overflow: hidden;display: -webkit-box;text-overflow: ellipsis;-webkit-box-orient: vertical;"><?php echo $listNews[0]->intro; ?></p>
                                                        <p>
                                                            <i class="fa fa-clock-o"></i> <?php echo $this->timeAgo($listNews[0]->created_time); ?>
                                                            <i class="fa fa-comment"></i> 19 </p>
                                                        <?php
                                                        unset($listNews[0]);
                                                        $k = 11;
                                                        while (isset($listNews[$k]) && $k > 5):
                                                            $oneItem = $listNews[$k];
                                                            ?>
                                                            <?php $oneCate = $this->getCategory($oneItem->category_id, 'title, title_page');
                                                            if (!empty($oneCate)): ?>
                                                                <p style="overflow: hidden;
                   text-overflow: ellipsis;
                   display: -webkit-box;
                   line-height: 14px;     /* fallback */
                   max-height: 32px;      /* fallback */
                   -webkit-line-clamp: 1; /* number of lines to show */
                   -webkit-box-orient: vertical;">
                                                                    <a href="<?php echo $this->getUrlNews($oneItem->news_id); ?>"
                                                                       title="<?php echo $oneItem->title; ?>"><?php echo $oneItem->title; ?>
                                                                        <br/></a>
                                                                </p>
                                                            <?php endif; ?>
                                                            <?php
                                                            $k--;
                                                        endwhile; ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="all_news_right">
                                                    <?php unset($listNews[11]);unset($listNews[10]);unset($listNews[9]);unset($listNews[8]);unset($listNews[7]);unset($listNews[6]); ?>
                                                    <?php foreach ($listNews as $k => $oneItem): ?>
                                                        <div class="fs_news_right">
                                                            <div class="single_fs_news_img"><a
                                                                    href="<?php echo $this->getUrlNews($oneItem->news_id); ?>"><img
                                                                        src="<?php echo $this->getImageNews($oneItem->news_id, 399, 270); ?>"
                                                                        alt="<?php echo $oneItem->title; ?>"/></a></div>
                                                            <div class="single_fs_news_right_text">
                                                                <h4>
                                                                    <a href="<?php echo $this->getUrlNews($oneItem->news_id); ?>"
                                                                       title="<?php echo $oneItem->title; ?>"><?php echo $oneItem->title; ?></a>
                                                                </h4>
                                                                <?php $oneCate = $this->getCategory($oneItem->category_id, 'title, title_page');
                                                                if (!empty($oneCate)): ?>
                                                                    <p>
                                                                        <a href="<?php echo $this->getUrlCate($oneItem->category_id); ?>"
                                                                           title="<?php echo $oneCate->title_page; ?>"><?php echo $oneCate->title; ?> </a>
                                                                        <i class="fa fa-clock-o"></i> <?php echo $this->timeAgo($oneItem->created_time); ?>
                                                                    </p>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <!-- ~~~=| Fashion area END |=~~~ -->
            </div>
            <?php echo $this->action('boxSidebar', 'block', 'frontend'); ?>
        </div>
    </div>
</section>
