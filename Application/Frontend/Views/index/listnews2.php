<?php $oneCate = $this->oneCate; ?>
<section class="main_news_wrapper">
    <div class="container">
<div class="cs-row">
    
    <?php if(!empty($this->data)) foreach ($this->data as $key=>$oneItem): ?>

            <div class="cs-col cs-col-6-of-12">
                <div class="cs-post-block-layout-2">
                    <div class="cs-post-item">
                        
                        <div class="cs-post-inner">
                            
                            <div class="cs-post-category-empty cs-clearfix">
                                <a href="<?php echo $this->getUrlCate($oneCate->category_id); ?>" title="<?php echo $oneCate->title_page; ?>"><?php echo $oneCate->title; ?></a>
                            </div>
                            <div class="cs-post-meta cs-clearfix">
                                <span class="cs-post-meta-author"><?php echo $this->getAuthor($oneItem->user_id); ?></span>
                                <span class="cs-post-meta-date"><?php echo $this->timeAgo($oneItem->created_time,'d-m-Y'); ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    <?php endforeach; ?>
</div>
</div>
</section>

