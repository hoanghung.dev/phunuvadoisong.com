<section class="hp_banner_area section_padding" style="margin-bottom:10px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="hp_banner_box">
                    <div class="hp_banner_left">
                        <?php if(!empty($this->listHot)) foreach ($this->listHot as $item): ?>
                            <div class="bl_single_news" style="border: solid 1px #fff;"><a href="<?php echo $this->getUrlNews($item->news_id);?>"> <img src="<?php echo $this->getImageNews($item->image,291 ,232) ?>" alt="<?php echo $item->title; ?>" /></a>
                                <div class="bl_single_text">
                                    <a href="<?php echo $this->getUrlNews($item->news_id); ?>" title="<?php echo $item->title; ?>">
                                        <h4><?php echo $item->title; ?></h4>
                                    </a>
                                   </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <!-- <div class="hp_banner_right">
                        <?php if(!empty($this->listMostViewed)) foreach ($this->listMostViewed as $item): ?>
                        <div class="br_single_news"> <img src="<?php echo $this->getImageNews($item->image,332,223) ?>" alt="<?php echo $item->title; ?>" />
                            <div class="br_single_text">
                                <?php $oneCate = $this->getCategory($item->category_id,'title,title_page'); if(!empty($oneCate)): ?>
                                    <span class="green_hp_span"><a href="" title="<?php echo $oneCate->title_page; ?>"> <?php echo $oneCate->title; ?></a></span>
                                <?php endif; ?>

                                <a href="<?php echo $this->getUrlNews($item->news_id); ?>" title="<?php echo $item->title; ?>">
                                    <h4><?php echo $item->title; ?></h4>
                                </a>
                            </div>
                            <div class="br_cam"> <a href="" class="fa fa-camera"></a> </div>
                        </div>
                        <?php endforeach; ?>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</section>