<?php if($this->listMostViewed) foreach ($this->listMostViewed as $item): ?>
    <div class="cs-post-item cs-width-33">
        <div class="cs-post-thumb">
            <div class="cs-post-category-icon">
                <a href="<?php echo $this->getUrlNews($item->news_id);?>" title="<?php echo $item->title;?>"><i class="fa fa-gamepad"></i></a>
            </div>
            <a href="<?php echo $this->getUrlNews($item->news_id);?>"><img src="<?php echo $this->getImageNews($item->news_id); ?>" alt="<?php echo $item->title;?>"></a>
        </div>
        <div class="cs-post-inner">
            <h3><a href="<?php echo $this->getUrlNews($item->news_id);?>"><?php echo $item->title;?></a></h3>
            <div class="cs-post-meta cs-clearfix">
                <span class="cs-post-meta-date"><?php echo $this->timeAgo($item->created_time,'d m Y'); ?></span>
            </div>
        </div>
    </div>
<?php endforeach; ?>