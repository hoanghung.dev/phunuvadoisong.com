<section class="section bgg">
    <div class="container">
        <div class="title-area">
            <h2><a href="/lien-he.html" title="Liên hệ Trung tâm Nhật Ngữ Sakura Trần Yến">Liên hệ</a></h2>
        </div><!-- /.pull-right -->
    </div><!-- end container -->
</section>
<div class="container sitecontainer bgw">
    <div class="row">
        <div class="col-md-12 m22 single-post">
            <div class="widget">
                <div class="large-widget m30">
                    <div class="post-desc">

                        <div class="row">
                            <div class="col-md-3 col-sm-12">
                                <div class="widget">
                                    <div class="links-widget darkcolor m30">
                                        <ul class="sociallinks">
                                            <li><i class="fa fa-building-o"></i>: <?php echo $this->info->company; ?></li>
                                            <li><i class="fa fa-map-marker"></i>: <?php echo $this->info->address; ?></li>
                                            <li><i class="fa fa-envelope"></i>: <?php echo $this->info->email; ?></li>
                                            <li><i class="fa fa-mobile-phone"></i>: <?php echo $this->info->tel; ?></li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- end widget -->
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <div class="widget">
                                            <div class="widget-title">
                                                <h4>Contact Form</h4>
                                                <hr>
                                                <?php echo $this->flash->message();?>
                                            </div>
                                            <!-- end widget-title -->

                                            <div class="commentform">
                                                <form role="form" class="row" method="post">
                                                    <div class="col-md-4 col-sm-12">
                                                        <label>Tên <span class="required">*</span></label>
                                                        <input type="text" name="name" class="form-control" placeholder="Tên của bạn" required>
                                                    </div>
                                                    <div class="col-md-4 col-sm-12">
                                                        <label>Email <span class="required">*</span></label>
                                                        <input type="email" name="email" class="form-control" placeholder="Email của bạn" required>
                                                    </div>

                                                    <div class="col-md-4 col-sm-12">
                                                        <label>Tiêu đề</label>
                                                        <input type="text" name="subject" class="form-control" placeholder="Tiêu đề" required>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12">
                                                        <label>Nội dung <span class="required">*</span></label>
                                                        <textarea name="message" class="form-control" placeholder="Nội dung muốn gửi" required></textarea>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12">
                                                        <label>Nhập mã bên dưới</label>
                                                        <input type="text" name="captcha" class="form-control" placeholder="Nhập mã bên dưới" required>
                                                        <span class="help-block with-errors"></span>
                                                        <img src="<?php echo $this->_session->captcha['image_src']; ?>" alt="CAPTCHA code"><a href="javascript:void(0)" onclick="window.location.reload();">Đổi mã khác</a>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12">
                                                        <input type="submit" value="Send Message" class="btn btn-primary" />
                                                    </div>
                                                </form>
                                            </div>
                                            <!-- end newsletter -->
                                        </div>
                                        <!-- end form-container -->
                                    </div>
                                    <!-- end col -->
                                </div>
                                <!-- end row -->
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <div class="widget">
                                    <div class="fb-page" data-href="https://www.facebook.com/Trung-T%C3%A2m-Nh%E1%BA%ADt-Ng%E1%BB%AF-Sakura-Tr%E1%BA%A7n-Y%E1%BA%BFn-1830129597268422/?fref=ts" data-tabs="timeline"
                                         data-height="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false"
                                         data-show-facepile="false"></div>
                                </div>
                                <!-- end widget -->

                            </div>

                        </div>
                        <!-- end row -->

                    </div>
                    <!-- end post-desc -->
                </div>
                <!-- end large-widget -->
            </div>
            <!-- end widget -->
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->
</div>
<!-- end container -->