<?php header("Content-Type: text/xml;charset=iso-8859-1"); ?>
<urlset
    xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="
	  		http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">

    <url>
        <loc><?php echo _ROOT_HOME;?></loc>
        <priority>1.00</priority>
        <changefreq>Always</changefreq>
    </url>
    <?php
    $categoryModel = new Application\Frontend\Models\Categories();
    $listCategory = $categoryModel->getDataArr();?>
    <?php if(!empty($listCategory)) foreach ($listCategory as $item):?>
    <url>
        <loc><?php echo _ROOT_HOME . '/category/' . $item->slug .'-'. $item->category_id;?></loc>
        <priority>0.90</priority>
        <changefreq>Always</changefreq>
    </url>
    <?php endforeach;?>
</urlset>