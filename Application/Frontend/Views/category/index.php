<?php if(!empty($this->oneCate)): $oneCate = $this->oneCate; ?>
    <section class="main_news_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <!-- ~~~=| Fashion area START |=~~~ -->
                    <div class="fashion_area">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="header_fasion">
                                    <div class="left_fashion main_nav_box">
                                        <ul>
                                            <li class="nav_fashion" style="margin-top:10px;margin-bottom:10px;"><a href="<?php echo $this->getUrlCate($oneCate->category_id); ?>" title="<?php echo $oneCate->title_page; ?>"><?php echo $oneCate->title; ?></a></li>
                                        </ul>
                                    </div>
                                    <div class="fasion_right">                                    
                                                <ul>
                                                    <li><a href="<?php echo $this->getUrlCate($oneCate->category_id);?>" title="<?php echo $oneCate->title_page; ?>"><?php echo $oneCate->title; ?></a></li>
                                                </ul>
                                    </div>
                                </div>

                                <div class="fashion_area_box">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <?php if(!empty($this->listNews)) foreach($this->listNews as $oneItem): ?>
                                                <div class="fs_news_right">
                                                    <div class="single_fs_news_img"> <img src="<?php echo $this->getImageNews($oneItem->news_id,205,140); ?>" alt="<?php echo $oneItem->title; ?>" /> </div>
                                                    <div class="single_fs_news_right_text">
                                                        <h4><a href="<?php echo $this->getUrlNews($oneItem->news_id); ?>" title="<?php echo $oneItem->title; ?>"><?php echo $oneItem->title; ?></a></h4>
                                                        <p style="overflow: hidden;display: -webkit-box;text-overflow: ellipsis;-webkit-box-orient: vertical;"><?php echo $oneItem->intro; ?></p>
                                                        <?php $oneCate = $this->getCategory($oneItem->category_id,'title, title_page'); if(!empty($oneCate)): ?>
                                                            <p> <a href="<?php echo $this->getUrlCate($oneItem->category_id); ?>" title="<?php echo $oneCate->title_page; ?>"><?php echo $oneCate->title; ?> </a>| <i class="fa fa-clock-o"></i> <?php echo $this->timeAgo($oneItem->created_time); ?> </p>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="news_pagination">
                                    <ul class="pagination">
                                        <?php echo isset($this->page)?$this->page:''; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ~~~=| Fashion area END |=~~~ -->
                </div>
                <?php echo $this->action('boxSidebar', 'Block', 'Frontend');?>
            </div>
        </div>
    </section>
<?php endif; ?>