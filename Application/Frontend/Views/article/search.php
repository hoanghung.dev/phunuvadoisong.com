<section class="section bgg">
    <div class="container">
        <div class="title-area">
            <h2>Search result : Web Design</h2>
            <div class="bread">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Search Example</li>
                </ol>
            </div><!-- end bread -->
        </div><!-- /.pull-right -->
    </div><!-- end container -->
</section>

<div class="container sitecontainer single-wrapper bgw">
    <div class="row">
        <div class="col-md-9 col-sm-12 col-xs-12 m22">
            <div class="widget searchwidget">
                <div class="large-widget m30">
                    <div class="post row clearfix">
                        <div class="col-md-5">
                            <div class="post-media">
                                <a href="single.html">
                                    <img alt="" src="upload/big_news_01.jpg" class="img-responsive">
                                </a>
                            </div>
                        </div>

                        <div class="col-md-7">
                            <div class="title-area">
                                <div class="colorfulcats">
                                    <a href="#"><span class="label label-primary">Interview</span></a>
                                    <a href="#"><span class="label label-warning">Web Design</span></a>
                                </div>
                                <h3>How ThePhone thriller will change the way film directors & details about the Phone</h3>

                                <div class="large-post-meta">
                                    <span class="avatar"><a href="author.html"><img src="upload/avatar_01.png" alt="" class="img-circle"> Kubra Karahasan</a></span>
                                    <small>&#124;</small>
                                    <span><a href="category.html"><i class="fa fa-clock-o"></i> 21 March 2016</a></span>
                                    <small class="hidden-xs">&#124;</small>
                                    <span class="hidden-xs"><a href="single.html#comments"><i class="fa fa-comments-o"></i> 92</a></span>
                                    <small class="hidden-xs">&#124;</small>
                                    <span class="hidden-xs"><a href="single.html"><i class="fa fa-eye"></i> 1223</a></span>
                                </div><!-- end meta -->
                            </div><!-- /.pull-right -->
                        </div>
                    </div><!-- end post -->
                </div><!-- end large-widget -->

                <div class="large-widget m30">
                    <div class="post row clearfix">
                        <div class="col-md-5">
                            <div class="post-media">
                                <a href="single.html">
                                    <img alt="" src="upload/big_news_02.jpg" class="img-responsive">
                                </a>
                            </div>
                        </div>

                        <div class="col-md-7">
                            <div class="title-area">
                                <div class="colorfulcats">
                                    <a href="#"><span class="label label-primary">Interview</span></a>
                                </div>
                                <h3>Time to meet our new designer and developer who joined our team</h3>

                                <div class="large-post-meta">
                                    <span class="avatar"><a href="author.html"><img src="upload/avatar_01.png" alt="" class="img-circle"> Kubra Karahasan</a></span>
                                    <small>&#124;</small>
                                    <span><a href="category.html"><i class="fa fa-clock-o"></i> 21 March 2016</a></span>
                                    <small class="hidden-xs">&#124;</small>
                                    <span class="hidden-xs"><a href="single.html#comments"><i class="fa fa-comments-o"></i> 92</a></span>
                                    <small class="hidden-xs">&#124;</small>
                                    <span class="hidden-xs"><a href="single.html"><i class="fa fa-eye"></i> 1223</a></span>
                                </div><!-- end meta -->
                            </div><!-- /.pull-right -->
                        </div>
                    </div><!-- end post -->
                </div><!-- end large-widget -->

                <div class="large-widget m30">
                    <div class="post row clearfix">
                        <div class="col-md-5">
                            <div class="post-media">
                                <a href="single.html">
                                    <img alt="" src="upload/big_news_03.jpg" class="img-responsive">
                                </a>
                            </div>
                        </div>

                        <div class="col-md-7">
                            <div class="title-area">
                                <div class="colorfulcats">
                                    <a href="#"><span class="label label-warning">Web Design</span></a>
                                </div>
                                <h3>Best office design's and workspace examples</h3>

                                <div class="large-post-meta">
                                    <span class="avatar"><a href="author.html"><img src="upload/avatar_01.png" alt="" class="img-circle"> Kubra Karahasan</a></span>
                                    <small>&#124;</small>
                                    <span><a href="category.html"><i class="fa fa-clock-o"></i> 21 March 2016</a></span>
                                    <small class="hidden-xs">&#124;</small>
                                    <span class="hidden-xs"><a href="single.html#comments"><i class="fa fa-comments-o"></i> 92</a></span>
                                    <small class="hidden-xs">&#124;</small>
                                    <span class="hidden-xs"><a href="single.html"><i class="fa fa-eye"></i> 1223</a></span>
                                </div><!-- end meta -->
                            </div><!-- /.pull-right -->
                        </div>
                    </div><!-- end post -->
                </div><!-- end large-widget -->

                <div class="large-widget m30">
                    <div class="post row clearfix">
                        <div class="col-md-5">
                            <div class="post-media">
                                <a href="single.html">
                                    <img alt="" src="upload/big_news_04.jpg" class="img-responsive">
                                </a>
                            </div>
                        </div>

                        <div class="col-md-7">
                            <div class="title-area">
                                <div class="colorfulcats">
                                    <a href="#"><span class="label label-primary">Interview</span></a>
                                    <a href="#"><span class="label label-warning">Web Design</span></a>
                                </div>
                                <h3>What you think about our new laptop its build by Apple</h3>

                                <div class="large-post-meta">
                                    <span class="avatar"><a href="author.html"><img src="upload/avatar_01.png" alt="" class="img-circle"> Kubra Karahasan</a></span>
                                    <small>&#124;</small>
                                    <span><a href="category.html"><i class="fa fa-clock-o"></i> 21 March 2016</a></span>
                                    <small class="hidden-xs">&#124;</small>
                                    <span class="hidden-xs"><a href="single.html#comments"><i class="fa fa-comments-o"></i> 92</a></span>
                                    <small class="hidden-xs">&#124;</small>
                                    <span class="hidden-xs"><a href="single.html"><i class="fa fa-eye"></i> 1223</a></span>
                                </div><!-- end meta -->
                            </div><!-- /.pull-right -->
                        </div>
                    </div><!-- end post -->
                </div><!-- end large-widget -->

                <div class="large-widget m30">
                    <div class="post row clearfix">
                        <div class="col-md-5">
                            <div class="post-media">
                                <a href="single.html">
                                    <img alt="" src="upload/big_news_05.jpg" class="img-responsive">
                                </a>
                            </div>
                        </div>

                        <div class="col-md-7">
                            <div class="title-area">
                                <div class="colorfulcats">
                                    <a href="#"><span class="label label-success">WordPress</span></a>
                                </div>
                                <h3>WordPress App for showcase galleries and Instagram users</h3>

                                <div class="large-post-meta">
                                    <span class="avatar"><a href="author.html"><img src="upload/avatar_01.png" alt="" class="img-circle"> Kubra Karahasan</a></span>
                                    <small>&#124;</small>
                                    <span><a href="category.html"><i class="fa fa-clock-o"></i> 21 March 2016</a></span>
                                    <small class="hidden-xs">&#124;</small>
                                    <span class="hidden-xs"><a href="single.html#comments"><i class="fa fa-comments-o"></i> 92</a></span>
                                    <small class="hidden-xs">&#124;</small>
                                    <span class="hidden-xs"><a href="single.html"><i class="fa fa-eye"></i> 1223</a></span>
                                </div><!-- end meta -->
                            </div><!-- /.pull-right -->
                        </div>
                    </div><!-- end post -->
                </div><!-- end large-widget -->
            </div><!-- end widget -->

            <div class="pagination-wrapper">
                <nav>
                    <ul class="pagination">
                        <li>
                            <a href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li>
                            <a href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div><!-- end col -->

        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="widget">
                <div class="widget-title">
                    <h4>Social Media</h4>
                    <hr>
                </div><!-- end widget-title -->

                <div class="social-media-widget m30">
                    <ul class="list-social clearfix">
                        <li class="facebook"><a href="#"><i class="fa fa-facebook"></i> <small>12.042</small></a></li>
                        <li class="twitter"><a href="#"><i class="fa fa-twitter"></i> <small>67.221</small></a></li>
                        <li class="googleplus"><a href="#"><i class="fa fa-google-plus"></i> <small>44.213</small></a></li>
                        <li class="rss"><a href="#"><i class="fa fa-rss"></i> <small>22.551</small></a></li>
                        <li class="pinterest"><a href="#"><i class="fa fa-pinterest"></i> <small>33.122</small></a></li>
                        <li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i> <small>55.123</small></a></li>
                        <li class="youtube"><a href="#"><i class="fa fa-youtube"></i> <small>44.112</small></a></li>
                        <li class="instagram"><a href="#"><i class="fa fa-instagram"></i> <small>12.441</small></a></li>
                    </ul>
                </div><!-- end social -->
            </div>

            <div class="widget hidden-xs">
                <div class="widget-title">
                    <h4>Advertising</h4>
                    <hr>
                </div><!-- end widget-title -->

                <div class="ads-widget m30">
                    <a href="#"><img src="upload/banner_01.jpg" alt="" class="img-responsive"></a>
                </div><!-- end ads-widget -->
            </div><!-- end widget -->

            <div class="widget">
                <div class="widget-title">
                    <h4>Carrier</h4>
                    <hr>
                </div><!-- end widget-title -->

                <div class="mini-widget carrier-widget m30">
                    <div class="post clearfix">
                        <div class="mini-widget-thumb">
                            <a href="single.html">
                                <img alt="" src="upload/carrer_01.jpg" class="img-responsive">
                            </a>
                        </div>
                        <div class="mini-widget-title">
                            <a href="single.html"> We are looking a team member</a>
                            <span class="label label-primary">Full time</span>
                        </div>
                    </div>

                    <div class="post clearfix">
                        <div class="mini-widget-thumb">
                            <a href="single.html">
                                <img alt="" src="upload/carrer_02.jpg" class="img-responsive">
                            </a>
                        </div>
                        <div class="mini-widget-title">
                            <a href="single.html"> Looking for support members (15-25 mails)</a>
                            <span class="label label-danger">Part time</span>
                        </div>
                    </div>

                    <div class="post clearfix">
                        <div class="mini-widget-thumb">
                            <a href="single.html">
                                <img alt="" src="upload/carrer_03.jpg" class="img-responsive">
                            </a>
                        </div>
                        <div class="mini-widget-title">
                            <a href="single.html"> Who fix my PHP problem?</a>
                            <span class="label label-info">Freelancer</span>
                        </div>
                    </div>

                    <div class="post clearfix">
                        <div class="mini-widget-thumb">
                            <a href="single.html">
                                <img alt="" src="upload/carrer_04.jpg" class="img-responsive">
                            </a>
                        </div>
                        <div class="mini-widget-title">
                            <a href="single.html"> Looking Logo Designer ($15 Budget) </a>
                            <span class="label label-info">Freelancer</span>
                        </div>
                    </div>
                </div><!-- end mini-widget -->
            </div><!-- end widget -->
        </div><!-- end col -->
    </div><!-- end row -->
</div><!-- end container -->