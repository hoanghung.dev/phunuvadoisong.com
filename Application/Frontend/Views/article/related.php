<div class="row m22 related-posts">
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-title">
                <h4>Bài viết liên quan</h4>
                <hr>
            </div><!-- end widget-title -->
            <?php if(!empty($this->data)) foreach ($this->data as $item): ?>
            <div class="review-posts row m30">
                <div class="post-review col-md-4 col-sm-12 col-xs-12">
                    <div class="post-media entry">
                        <a href="<?php echo $this->getUrlNews($item->news_id); ?>" title="<?php echo $item->title; ?>">
                            <img alt="<?php echo $item->title; ?>" src="<?php echo $this->getImageNews($item->news_id,288,166); ?>" class="img-responsive">
                        </a>
                        <div class="magnifier">
                            <div class="hover-title-left">
                                <span><a href="#" title=""><i class="fa fa-eye"></i> <?php echo $item->viewed; ?></a></span>
                            </div><!-- end title -->
                            <div class="hover-title">
                                <span><a href="#" title=""><i class="fa fa-comments-o"></i> Bình Luận</a></span>
                            </div><!-- end title -->
                        </div><!-- end magnifier -->
                    </div><!-- end media -->
                    <div class="post-title">
                        <h3><a href="<?php echo $this->getUrlNews($item->news_id); ?>"><?php echo $item->title; ?></a></h3>
                    </div><!-- end post-title -->
                </div><!-- end post-review -->
            </div><!-- end review-post -->
            <?php endforeach; ?>
        </div><!-- end widget -->
    </div><!-- end col -->
</div><!-- end row -->