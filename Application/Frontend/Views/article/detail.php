<?php if(!empty($this->data)): $oneItem = $this->data; ?>
<section class="main_news_wrapper cc_single_post_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12">
                <!-- ~~~=| Fashion area START |=~~~ -->
                <div class="cc_single_post">
                    <div class="sp_details">
                        <?php $oneCate = $this->getCategory($oneItem->category_id,'title,title_page'); if(!empty($oneCate)): ?>
                            <a href="<?php echo $this->getUrlCate($oneItem->category_id); ?>" title="<?php echo $oneCate->title_page; ?>"><?php echo $oneCate->title; ?></a>
                        <?php endif; ?>
                        <h1><?php echo $oneItem->title; ?></h1>
                        <div class="post_meta">
                            <ul>
                                <?php $author = $this->getAuthor($oneItem->user_id); ?>
                                <li><a href="/" title="<?php echo $this->info->title_page; ?>"><i class="fa fa-user"></i><?php echo $author; ?></a></li>
                                <li><a rel="nofollow" href="javascript:void(0)" title="Lượt xem"><i class="fa fa-eye"></i><?php echo $oneItem->viewed; ?></a></li>
                                <li><a rel="nofollow" href="#comment" title="Bình luận"><i class="fa fa-comment-o"></i>45</a></li>
                                <div class="social_tags_right" style="float:right;">
                                <ul>
                                    <li class="facebook" ><a rel="nofollow" class="fa fa-facebook"
                                                            onClick="window.open('http://www.facebook.com/sharer.php?u=<?php echo $this->getUrlNews($oneItem->news_id); ?>','Facebook','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;"
                                                            href="javascript:void(0);" style="padding:7px;"></a>
                                    </li>
                                    <li class="twitter"><a rel="nofollow" class="fa fa-twitter"
                                                           onClick="window.open('http://twitter.com/share?url=<?php echo $this->getUrlNews($oneItem->news_id); ?>&amp;text=<?php echo $this->info->title; ?>','Twitter share','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;"
                                                           href="http://twitter.com/share?url=<?php echo $this->getUrlNews($oneItem->news_id); ?>&amp;text=<?php echo $this->info->title; ?>" style="padding:7px;"></a>
                                    </li>
                                    <li class="google-plus"><a rel="nofollow" class="fa fa-google-plus"
                                                               onClick="window.open('https://plus.google.com/share?url=<?php echo $this->getUrlNews($oneItem->news_id); ?>','Google plus','width=585,height=666,left='+(screen.availWidth/2-292)+',top='+(screen.availHeight/2-333)+''); return false;"
                                                               href="https://plus.google.com/share?url=<?php echo $this->getUrlNews($oneItem->news_id); ?>" style="padding:7px;"></a>
                                    </li>
                                </ul>
                            </div>
                            </ul>
                        </div>
                        <div class="post_text" style="max-width:100% !important;">
                            <?php echo $oneItem->content; ?>                              
                        </div>
                        <div class="bonus-text">
                                <?php $listNews2 = $this->listNewsInCate2($oneItem->category_id); if(!empty($listNews2)): ?>
                                    <?php foreach ($listNews2 as $k=>$oneItem): ?>
                                      <ul>
                                          <li>
                                              <a href="<?php echo $this->getUrlNews($oneItem->news_id) ;?>" title="<?php echo $oneItem->title;?>"><?php echo $oneItem->title;?></a>
                                          </li>
                                      </ul>
                                  <?php endforeach; ?>
                              <?php endif; ?>
                              </div>  
                        <div class="social_tags">
                            <!-- <div class="social_tags_left">
                                <p>Tags :</p>
                                <ul>
                                    <?php $listTag = $this->getTag($oneItem->keywords); ?>
                                </ul>
                            </div> -->
                            <div class="social_tags_right" style="float:right;">
                                <ul>
                                    <li class="facebook"><a rel="nofollow" class="fa fa-facebook"
                                                            onClick="window.open('http://www.facebook.com/sharer.php?u=<?php echo $this->getUrlNews($oneItem->news_id); ?>','Facebook','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;"
                                                            href="javascript:void(0);" style="padding:7px;"></a>
                                    </li>
                                    <li class="twitter"><a rel="nofollow" class="fa fa-twitter"
                                                           onClick="window.open('http://twitter.com/share?url=<?php echo $this->getUrlNews($oneItem->news_id); ?>&amp;text=<?php echo $this->info->title; ?>','Twitter share','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;"
                                                           href="http://twitter.com/share?url=<?php echo $this->getUrlNews($oneItem->news_id); ?>&amp;text=<?php echo $this->info->title; ?>" style="padding:7px;"></a>
                                    </li>
                                    <li class="google-plus"><a rel="nofollow" class="fa fa-google-plus"
                                                               onClick="window.open('https://plus.google.com/share?url=<?php echo $this->getUrlNews($oneItem->news_id); ?>','Google plus','width=585,height=666,left='+(screen.availWidth/2-292)+',top='+(screen.availHeight/2-333)+''); return false;"
                                                               href="https://plus.google.com/share?url=<?php echo $this->getUrlNews($oneItem->news_id); ?>" style="padding:7px;"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- <div class="sp-next-prev">
                                <div class="sp-prev">
                                    <a href="<?php echo $this->getUrlNews($oneItem->news_id);?>"><i class="fa fa-angle-double-left"></i>Bài trước</a>
                                </div>
                                <div class="sp-next">
                                    <a href="<?php echo $this->getUrlNews($oneItem->news_id);?>">Bài tiếp theo<i class="fa fa-angle-double-right"></i></a>                            
                                </div>
                        </div> -->
                        <div id="comment" class="sp-comments-box">
                            <h2>Comments</h2>
                            <div class="fb-comments" data-href="<?php echo $this->getUrlNews($oneItem->news_id); ?>" data-width="100%" data-numposts="5"></div>
                        </div>
                        
                        <h4 style="margin-left:10px;">Tin liên quan</h4>
                       
                            <?php $listNews = $this->listNewsInCate1($oneItem->category_id); if(!empty($listNews)): ?>
                                <?php foreach ($listNews as $k=>$oneItem): ?>
                                    <div class="br_single_news_1">
                                        <a href="<?php echo $this->getUrlNews($oneItem->news_id);?>">
                                            <img src="<?php echo $this->getImageNews($oneItem->news_id,247,178); ?>" alt="<?php echo $oneItem->title; ?>" />
                                        </a>
                                     <p style="width:89%;">
                                        <a href="<?php echo $this->getUrlNews($oneItem->news_id); ?>" title="<?php echo $oneItem->title; ?>">
                                       <?php echo $oneItem->title; ?>
                                        </a> 
                                        </p>
                                    </div>
                                <?php endforeach;?>
                            <?php endif; ?>
                    </div>
                </div>
                <!-- ~~~=| Fashion area END |=~~~ -->
            </div>
            <?php echo $this->action('boxsidebar','block','frontend',$this->params); ?>
        </div>
    </div>
</section>
<?php endif; ?>