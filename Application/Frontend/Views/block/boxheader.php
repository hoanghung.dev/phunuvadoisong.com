<!-- ~~~=| Header START |=~~~ -->
<header class="header_area">

    <!-- ~~~=| Logo Area START |=~~~ -->
    <div class="header_logo_area">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="logo"> <a href="/" title="<?php echo $this->info->title_page; ?>"><img src="/images/logo-bottom-1.png" alt="<?php echo $this->info->title_page; ?>"/></a> </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <div class="header_add"> <a href="/" title="<?php echo $this->info->title_page; ?>">
                        <?php if($this->checkMobile() == false){
                            echo  ' <iframe src="http://ylx-2.com/banner_show_safe.php?section=General&amp;pub=791475&amp;format=728x90&amp;ga=g" frameborder="0" scrolling="no" width="728" height="90" marginwidth="0" marginheight="0"></iframe>';
                        }
                        ;?>
                   </a> </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ~~~=| Logo Area END |=~~~ -->

    <!-- ~~~=| Main Navigation START |=~~~ -->
    <div class="mainnav">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <nav class="main_nav_box">
                        <ul id="nav">
                            <li class="nav_phunu active"><a href="/" title="<?php echo $this->info->title_page; ?>"><i class="fa fa-home"></i></a></li>
                            <?php
                                if(!empty($this->listMenu))
                                    $categoryModel = new Application\Frontend\Models\Categories();
                                    foreach($this->listMenu as $item):
                                        $active = '';
                                        if($item->category_id == $this->category_id) $active = 'active';
                            ?>
                                <li class="nav_phunu">
                                    <a href="<?php echo $this->getUrlCate($item->category_id);?>" title="<?php echo $item->title_page; ?>"><?php echo $item->title; ?>  </a>
                                    <?php
                                    $childs = $categoryModel->getDataArr(['parent_id'=>$item->category_id, 'order_by'=>'order_no ASC']);
                                    if(!empty($childs)) {
                                        echo '<ul class="child">';
                                        foreach ($childs as $child) {
                                            ?>
                                            <li class="nav_phunu">
                                            <a href="<?php echo $this->getUrlCate($child->category_id);?>" title="<?php echo $child->title_page; ?>"><?php echo $child->title; ?>  </a>
                                            <?php
                                        }
                                        echo '</ul>';
                                    }
                                    ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </nav>

                    <!-- ~~~=| Mobile Navigation END |=~~~ -->
                    <div class="only-for-mobile">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <ul class="ofm">
                             
                                <li class="m_nav"><i class="fa fa-bars">Menu</i> 

                                </li>
                            </ul>

                            <!-- MOBILE MENU -->
                            <div class="mobi-menu">
                                <div id='cssmenu'>
                                        <?php
                                        if(!empty($this->listMenu))
                                        foreach($this->listMenu as $item):
                                        $active = '';
                                        if($item->category_id == $this->category_id) $active = 'active';
                                        ?>
                                    <ul>
                                        <li class='active'> <a href="<?php echo $this->getUrlCate($item->category_id);?>"
                                         title="<?php echo $item->title_page; ?>"><?php echo $item->title; ?>  </a>
                                        </li>
                                    </ul>
                                        <?php endforeach; ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ~~~=| Mobile Navigation START |=~~~ -->

                </div>
            </div>
        </div>
    </div>
    <!-- ~~~=| Main Navigation END |=~~~ -->

</header>
<!-- ~~~=| Header END |=~~~ -->
<!-- ~~~=| Banner START |=~~~ -->

<!-- ~~~=| Banner END |=~~~ -->