<div id="property-slider" class="clearfix">
    <div class="flexslider">
        <ul class="slides">
            <?php if(!empty($this->listSlider)) foreach ($this->listSlider as $item): ?>
                <li>
                    <div class="psdesc hidden-xs">
                        <div class="ps-desc">
                            <h3><a href="<?php echo $item->link; ?>" title="<?php echo $item->title; ?>"><?php echo $item->title; ?></a></h3>
                            <p><?php echo $item->intro; ?></p>
                        </div>
                    </div>
                    <a href="<?php echo $item->link; ?>" title="<?php echo $item->title; ?>"><img alt="<?php echo $item->title; ?>" src="<?php echo $this->getImageNews($item->image,975,488); ?>" class="img-responsive"></a>
                </li>
            <?php endforeach; ?>
        </ul>
        <!-- end slides -->
    </div>
    <!-- end flexslider -->
</div>
<!-- end property-slider -->