<!-- ~~~=| Footer START |=~~~ -->
<footer class="footer_area">

    <div class="footer_top">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="footer_top_box">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="footer_top_left"> <a href="/" title="<?php echo $this->info->title_page; ?>"><img src="/images/logo-bottom-1.png" alt="Footer Logo"/></a>

                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <aside id="text-8" class="widget-2 widget-even widgetFooter col widget_text">
                                    <div class="textwidget">
                                        <p>
                                            <strong>Tổng biên tập:
                                            </strong>
                                        </p>
                                        <p>Trưởng ban phụ trách:
                                        </p>
                                        <p>Tổng thư ký tòa soạn:
                                        </p>
                                        <p>Kênh nhận tin miền Bắc:
                                        </p>
                                        <p>Kênh nhận tin miền Nam:
                                        </p>
                                    </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                </aside>
                                <aside id="text-9" class="widget-3 widget-last widget-odd widgetFooter col widget_text">
                                    <div class="textwidget">
                                        <p class="text-right">Góp ý nội dung:
                                        </p>
                                        <p class="text-right">Email: info@phunuvadoisong.net
                                        </p>
                                        <p class="text-right">Chuyên trang Phụ nữ và Đời sống Online
                                        </p>
                                        <p class="text-right">Phiên bản thử nghiệm trong khi chờ xin cấp phép
                                        </p>
                                        <p class="text-right">® Báo Phụ nữ và Đời sống Online
                                        </p>
                                    </div>
                                </aside>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="footer_bottom_box">
                        <p>Phát triển bởi HT. Copyright 2006. All rights reserved. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div id="top-up"><i class="fa fa-arrow-circle-o-up"></i></div>

</footer>
<!-- ~~~=| Footer END |=~~~ -->