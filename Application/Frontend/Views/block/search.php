<!-- Search icon show -->
<div id="cs-header-menu-search-button-show"><i class="fa fa-search"></i></div>
<!-- Search icon -->
<div id="cs-header-menu-search-form">
    <div id="cs-header-menu-search-button-hide"><i class="fa fa-close"></i></div>
    <div>
        <input id="keyword" type="text" placeholder="Type and press enter..." style="width: 100%;">
        <button class="btn" id="btn-search" type="submit" style="display: none;">Go</button>
    </div>
</div>
