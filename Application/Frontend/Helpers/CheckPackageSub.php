<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Frontend\Helpers;

class CheckPackageSub{
    function checkPackageSub($msisdn,$packageName){
        if(!empty($msisdn) && !empty($packageName)){
            $requestid = date('YmdHis');
            $application="cskh";
            $channel="cskh";
            $username="vcare";
            $userip=$_SERVER['REMOTE_ADDR'];
            //$userip="192.168.41.250";
            $url="http://192.168.44.2:8082/vcare/prov?wsdl";
            $client = new \SoapClient($url);
            $result = $client->getPackage($requestid,$msisdn,$packageName,$application,$channel,$username,$userip);
            $array = json_decode(json_encode($result), true);
            return $array['packageResult']['status'];
        }else{
            return false;
        }
    }
}