<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Frontend\Helpers;
use Application\Frontend\Models\News;

class oneCate{
    public function oneCate($cateId){
        $newsModel = new News();
        $params['select'] = 'news_id,title,created_time,intro,user_id ';
        $params['category_id'] = $cateId;
        $params['order_by'] = 'news_id DESC';
        $params['limit'] = 4;
        $data = $newsModel->getDataArr($params);
        return $data;
    }
}