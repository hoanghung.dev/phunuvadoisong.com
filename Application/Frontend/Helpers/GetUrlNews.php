<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Frontend\Helpers;
use Application\Frontend\Controllers\Base;
use Application\Frontend\Models\Categories;
use Application\Frontend\Models\News;

class GetUrlNews extends Base{
    public function getUrlNews($id){
        $newsModel = new News();
        $data = $newsModel->getOne('news_id = ?',array($id),'slug,title,category_id');
        $slug = $this->toSlug($data->title);
        if($slug == null){
            $slug = $this->toSlug($data->title);
            $newsModel->update(array('slug'=>$slug),'news_id = :id',array(':id'=>$id));
        }
        $slugCategory = $this->getTitleCate($data->category_id);
        return _ROOT_HOME.'/'.$slugCategory.'/'.$slug.'-'.$id . '.html';
    }
    private function getTitleCate($id){
        $categoryModel = new Categories();
        $data = $categoryModel->getOne('category_id = :id',array(':id'=>$id),'slug');
        return $data->slug;
    }
}