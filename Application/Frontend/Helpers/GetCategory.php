<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Frontend\Helpers;
use Application\Frontend\Models\Categories;

class GetCategory{
    public function getCategory($id,$select='*'){
        $categoryModel = new Categories();
        return $categoryModel->getOne('category_id = ?',array($id),$select);
    }
}