<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Frontend\Helpers;
use Application\Frontend\Controllers\Base;
use Application\Frontend\Models\Categories;

class GetUrlCate extends Base{
    public function getUrlCate($id){
        $cateModel = new Categories();
        $data = $cateModel->getOne('category_id = :id',array(':id'=>$id),'slug,title');
        if(!empty($data)){
            if($data->slug == null){
                $data->slug = $slug = $this->toSlug($data->title);
                $cateModel->update(array('slug'=>$slug),'category_id = :id',array(':id'=>$id));
                return _ROOT_HOME.'/category/'.$slug.'-'.$id;
            }
            else return _ROOT_HOME.'/'.$data->slug.'-'.$id;
        }else return _ROOT_HOME;
    }
}