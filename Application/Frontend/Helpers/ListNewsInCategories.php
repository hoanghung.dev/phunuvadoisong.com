<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Frontend\Helpers;
use Application\Frontend\Models\Categories;
use Application\Frontend\Models\News;

class ListNewsInCategories {
    public function ListNewsInCategories ($cateId){
        $arrCatId = $cateId;
        
        $cateModel = new Categories();
        $childs = $cateModel->getDataArr(['parent_id'=>$cateId]);
        if(!empty($childs)) {
            foreach ($childs as $key => $child) {
                $arrCatId .= ',' . $child->category_id ;
            }
        }

        $newsModel = new News();
        $params['select'] = 'news_id,title,category_id, created_time,intro,user_id ';
        $params['in_cat'] = $arrCatId;
        $params['order_by'] = 'news_id DESC';
        $params['limit'] = 12;
        $data = $newsModel->getDataArr($params);
        return $data;
    }
}