<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 26/04/2015
 * Time: 10:36 CH
 */
namespace Application\Frontend\Helpers;
use Application\Frontend\Controllers\Base;
use Application\Frontend\Models\News;
use Soul\Helpers\Thumbnail;

class GetImageNews extends Base{
    public function getImageNews($id,$width = 0, $height = 0){
        if(is_numeric($id)){
            $newsModel = new News();
            $data = $newsModel->getOne('news_id = ?',array($id),'image');
            $image = $data->image == NULL ? '/no-image.png' : $data->image;

            // Xoa bai viet neu ko co anh
            if($data->image == NULL || !file_exists(DIR_UPLOAD . '/' . $data->image)) $newsModel->delete('news_id=:id', [':id'=>$id]);
        }else $image = $id;
        if($width != 0 && $height != 0){
            $size = sprintf('-%dx%d', $width, $height);
            $part = explode('.', $image);
            $ext = '.'.end($part);
            $newlink = str_replace($ext,$size.$ext, $image);
            $newlink = str_replace('/Frontend/upload/','',$newlink);
            $newlink = str_replace(_ROOT_UPLOAD,'',$newlink);
            $newlink = str_replace('/upload/','',$newlink);
            return _ROOT_UPLOAD.'/cache'.$newlink;
        }else return _ROOT_UPLOAD.'/'.$image;
    }
}