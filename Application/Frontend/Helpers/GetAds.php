<?php
/**
 * Created by PhpStorm.
 * User: Steven
 * Date: 07/03/2014
 * Time: 22:57
 */
namespace Application\Frontend\Helpers;
class getAds
{
    public function getAds($size = null) {
        if($size != null){
            $sizeArr = explode('x',$size);
            $width = $sizeArr[0];
            $height = $sizeArr[1];
            $htmlAds = "
                <div style='border: 1px solid rgba(0, 0, 0, 0.85);width: 100%;height:".$height."px;margin: 0 auto;'>".$size."</div>
            ";
        }else $htmlAds = 'responsive';

        return $htmlAds;
    }
}