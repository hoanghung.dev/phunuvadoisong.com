<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 10:54 SA
 */

namespace Application\Frontend\Helpers;
use Application\Frontend\Controllers\Base;
use Application\Frontend\Models\News;

class GetTag extends Base{
    public function getTag($tag){
        $tag = explode(',',$tag);
        if(count($tag) > 1) {
            foreach($tag as $item){
                $item = trim($item);
                echo '<li><a href="/tags/'.$this->toSlug($item).'" title="'.$item.'">'.$item.'</a></li>';
            }
        }
    }
}