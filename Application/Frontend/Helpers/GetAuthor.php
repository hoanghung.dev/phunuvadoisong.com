<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Frontend\Helpers;

use Application\Frontend\Models\Users;

class GetAuthor{
    public function getAuthor($id){
        $model = new Users();
        $oneItem = $model->getOne('user_id = :id',array(':id'=>$id),'full_name');
        return isset($oneItem->full_name)?$oneItem->full_name:'Steven';
    }
}