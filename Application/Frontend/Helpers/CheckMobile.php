<?php
/**
 * Created by PhpStorm.
 * User: Steven
 * Date: 06/03/2014
 * Time: 11:24
 */
namespace Application\Frontend\Helpers;

class CheckMobile{
    public function checkMobile(){
        $Mobile = new \Mobile_Detect();
        if ( $Mobile->isMobile() ) {
            return true;
        }
        return false;

    }
}
