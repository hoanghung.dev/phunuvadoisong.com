<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Frontend\Helpers;
use Application\Frontend\Models\Categories;

class GetCateChild{
    public function getCateChild($id,$select='title,title_page,category_id'){
        $categoryModel = new Categories();
        $params['select'] = $select;
        $params['parent_id'] = $id;
        return $categoryModel->getDataArr($params);
    }
}