<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 08/07/2015
 * Time: 03:18 CH
 */
namespace Application\Frontend\Helpers;

class CutString{
    public function cutString($string,$number){
        $str = strip_tags(implode(' ',array_slice(explode(' ',$string),0,$number)).'...');
        return $str;
    }
}

