<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 26/04/2015
 * Time: 10:36 CH
 */
namespace Application\Frontend\Helpers;
use Application\Frontend\Controllers\Base;
use Application\Frontend\Models\News;

class GetVideoNews extends Base{
    public function getVideoNews($id){
        $newsModel = new News();
        $data = $newsModel->getOne('news_id = ?',array($id),'link');
        if($data->link) return _ROOT_HOME.'/videos/'.$data->link;
        else return _ROOT_HOME.'/images/noimage.gif';
    }
}