<?php
/**
 * Soul Framework
 * @category Soul
 * @package Soul_View_Paging
 * @author Đoàn Thanh Đức <hoangtubapcai@yahoo.com>
 * @copyright Copyright (c) 2006 - 2008 Cabbage Sofware VN Inc.
 * @version 1.9
 */
namespace Soul\Helpers;
class Paging
{
    /**
     * Ngôn ngữ
     * @var array
     */
    public $_queryString = null;
    public $_lang = array('back' => '<', 'next' => '>', 'firstPage' => '<<', 'lastPage' => '>>');
    /**
     * Số trang hiển thị trên 1 trang
     * vd: 5 => back 12345 next
     *
     * @var int
     */
    protected $_maxPage = 5;
    /**
     * Hiển thị phân trang hay ko
     *
     * @var bool
     */
    protected $_paging = null;
    /**
     * Enter description here...
     *
     * @var string
     */
    protected $_url = null;
    /**
     * Tổng số trang
     *
     * @var int
     */
    protected $_numPage;
    /**
     * Trang hiện tại
     *
     * @var int
     */
    protected $_currentPage;
    /**
     * Enter description here...
     *
     * @var int
     */
    protected $_offset;
    /**
     * So recode tren 1 trang
     *
     * @var int
     */
    protected $_numItemOnPage = 1;
    /**
     * Enter description here...
     *
     * @var int
     */
    protected $_numItem;
    /**
     * Enter description here...
     *
     * @var string
     */
    protected $_urlType = '&amp;page=';

    public function setQueryString($value)
    {
        $this->_queryString = $value;
    }

    public function getQueryString()
    {
        return $this->_queryString;
    }

    /**
     * Enter description here...
     *
     * @param string $url_type
     */
    public function setUrlType($url_type = '&amp;page=')
    {
        $this->_urlType = $url_type;
    }

    public function getUrlType()
    {
        return $this->_urlType;
    }

    /**
     * Enter description here...
     *
     * @param string $url
     * @return Dtd_Paging
     */
    public function setUrl($url)
    {
        $this->_url = trim($url);
        return $this;
    }

    /**
     * Enter description here...
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->_url;
    }

    /**
     * Enter description here...
     *
     * @param unknown_type $num_item
     * @return Dtd_Paging
     */
    public function setNumItem($num_item)
    {
        $this->_numItem = intval($num_item);
        return $this;
    }

    /**
     * Enter description here...
     *
     * @return int
     */
    public function getNumItem()
    {
        return $this->_numItem;
    }

    /**
     * Enter description here...
     *
     * @return int
     */
    public function getNumPage()
    {
        $this->_numPage = ceil($this->getNumItem() / $this->getNumItemOnPage());
        return $this->_numPage;
    }

    /**
     * Gán vị trí lấy dữ liệu từ dòng bao nhiêu trở đi
     *
     * @param int $offset
     * @return Dtd_Paging
     */
    public function setCurrentPage($current_page)
    {
        $this->_currentPage = abs($current_page);
        if ($this->_currentPage == 0) {
            $this->_currentPage = 1;
        }
        return $this;
    }

    /**
     * Trả về vị trí bắt đầu lấy dữ liệu
     *
     * @return int
     */
    public function getCurrentPage()
    {
        if ($this->_currentPage > $this->getNumPage()) {
            $this->_currentPage = 1;
        }
        return $this->_currentPage;
    }

    /**
     * Enter description here...
     *
     */
    public function getOffset()
    {
        $offset = 0;
        $current_page = $this->getCurrentPage();
        if ($current_page > 1) {
            $offset = ($current_page - 1) * $this->getNumItemOnPage();
        }
        return $offset;
    }

    /**
     * Enter description here...
     *
     * @param int $num_item
     * @return Dtd_paging
     */
    public function setNumItemOnPage($num_item)
    {
        $num_item = (intval($num_item) > 0) ? $num_item : 1;
        $this->_numItemOnPage = $num_item;
        return $this;
    }

    /**
     * Enter description here...
     *
     * @return int
     */
    public function getNumItemOnPage()
    {
        return $this->_numItemOnPage;
    }

    /**
     * Gán số trang hiển thị trên 1 trang
     *
     * @param int $num_page
     * @return int
     */
    public function setMaxPage($num_page)
    {
        $this->_maxPage = abs($num_page);
        return $this->_maxPage;
    }

    /**
     * Trả về số trang hiển thị trên 1 trang
     *
     * @return int
     */
    public function getMaxPage()
    {
        return $this->_maxPage;
    }

    /**
     * Thiết lập trạng thái phân trang
     *
     * @param bool $bool
     * @return Dtd_Paging
     */
    public function setPaging($bool)
    {
        $this->_paging = (bool)$bool;
        return $this;
    }

    /**
     * Kiểm tra dữ liệu có hợp lệ không
     *
     * @return bool
     */
    protected function isPaging()
    {
        if ((bool)$this->_paging) {
            return $this->_paging;
        }
        if ($this->getNumPage() <= 0) {
            $this->_paging = false;
        } else {
            $this->_paging = true;
        }
        return $this->_paging;
    }

    protected function getNumInteration()
    {
        $this->_numInteration = floor($this->_maxPage / 2);
        return $this->_numInteration;
    }

    /**
     * Trả về mảng các trang
     *
     * @return array
     */
    public function toArray()
    {
        if (!$this->isPaging()) {
            return array();
        }
        for ($i = $this->getFormPage(); $i <= $this->getToPage(); $i++) {
            $page[] = $i;
        }
        return $page;

    }

    public function getFormPage()
    {
        $page = $this->getCurrentPage();
        $inc = $this->getNumInteration();
        $fromPage = ($page > $inc) ? ($page - $inc) : 1;
        return $fromPage;
    }

    public function getToPage()
    {
        $totalPage = $this->getNumPage();
        $page = $this->getCurrentPage();
        $inc = $this->getNumInteration();
        $toPage = ($page <= $totalPage - $inc) ? ($page + $inc) : $totalPage;
        return $toPage;
    }

    public function getJumpPage()
    {
        $totalPage = $this->getNumPage();
        $page = $this->getCurrentPage();
        $maxPage = $this->getMaxPage();
        $jumpPage = ($page <= $totalPage - $maxPage) ? ($page + $maxPage) : 0;
        return $jumpPage;
    }

    /**
     * Trả về chuỗi phân trang
     *
     * @return string
     */
    public function toString()
    {
        if (!$this->isPaging()) {
            return null;
        }
        $output = '';
        if ($this->getCurrentPage() > 1) {
            $output .= sprintf('<li class="first"><a href="%s%s1%s">&lt;</a></li>', $this->getUrl(), $this->getUrlType(), $this->getQueryString());
        }
        foreach ($this->toArray() as $p) {
            if ($this->getCurrentPage() == $p) {
                $output .= sprintf('<li class="active"><a href="javascript:void(0);">%d</a></li>', $p);
            } else {
                $output .= sprintf('<li><a href="%s%s%d%s">%d</a></li>', $this->getUrl(), $this->getUrlType(), $p, $this->getQueryString(), $p);
            }

        }
        $jumPage = $this->getJumpPage();
        $totalPage = $this->getNumPage();
        if ($jumPage > 0) {
            $output .= sprintf('<li><a href="%s%s%d%s">...</a></li>', $this->getUrl(), $this->getUrlType(), $jumPage, $this->getQueryString());
        }
        if ($this->_currentPage < $totalPage) {
            $output .= sprintf('<li class="last"><a href="%s%s%d%s">&gt;</a></li>',

                $this->getUrl(), $this->getUrlType(), $totalPage, $this->getQueryString());
        }
        return $output;

    }

}

?>