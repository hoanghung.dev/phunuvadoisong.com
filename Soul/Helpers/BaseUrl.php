<?php
namespace Soul\Helpers;

use Soul\Application;

class BaseUrl
{
    public function baseUrl()
    {

        return sprintf(
            "%s://%s",
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
            $_SERVER['HTTP_HOST']
        );
        $reg = Application::getInstance()->getRequest();

        return $reg->getBaseUrl();
    }
}